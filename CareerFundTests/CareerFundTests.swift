//
//  CareerFundTests.swift
//  CareerFundTests
//
//  Created by Prawira Hadi Fitrajaya on 15/12/21.
//

import XCTest
import UIKit
@testable import CareerFund

class CareerFundTests: XCTestCase {
    
    func testUserRegister(){
        let promise = expectation(description: "Success Register")
        Request().register(email:  "jaka@mail.com", name:  "user in test", password:  "222", role: "LENDER") { result in
            DispatchQueue.main.async {
                
                switch result {
                case .success:
                    promise.fulfill()
                    break
                case .failure(let error):
                    XCTFail("Error: \(error.localizedDescription)")
                }
            }
        }
        
        wait(for: [promise], timeout: 5)
    }
    
    func testUserLogin(){
        let promise = expectation(description: "Success Login")
        Request().login(email: "Dwistari16@gmail.com", password: "Qwerty123!") { result in
            switch result {
            case .success:
                promise.fulfill()
                break
            case .failure(let error):
                if let responCode = Int(error.localizedDescription){
                    if responCode >= 300 && responCode < 400 {
                        XCTFail("Error: \(error.localizedDescription)")
                    } else {
                        XCTFail("Error: \(error.localizedDescription)")
                    }
                }
                
            }
        }
        
        wait(for: [promise], timeout: 5)
    }
    
    // Will success if type user is lender
    func testGetLenderLoansData(){
        let token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmaXRyYWpheWFwcmF3aXJhQGdtYWlsLmNvbSIsImV4cCI6MTY0MTE2ODAwMCwiaWF0IjoxNjQwMDE0MDAxfQ.L778kOwv7J6VVWmc1v74lhWuVAi82X3lJEusuqp_Pck"
        Session.shared.token = token
        let promise = expectation(description: "Success Get Lender Loans")
        Request().getLenderLoans { result in
            switch result {
            case .success:
                promise.fulfill()
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        wait(for: [promise], timeout: 5)
    }
    
    // Will success if type user is borrower
    func testGetListClassData(){
        let token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJEd2lzdGFyaTE2QGdtYWlsLmNvbSIsImV4cCI6MTY0MTE2ODAwMCwiaWF0IjoxNjQwMDEzNzMzfQ.lyElQ9Cv04rrCFmXBUxqT-65nE6wIU2xh9NrGky6psc"
        Session.shared.token = token
        let promise = expectation(description: "Success Get List Class")
        Request().getListClass(keyword: "", categories: [], feeMax: "0", feeMin: "0", institutions: [], sort: "asc", order: "price") { result in
            switch result {
            case .success:
                promise.fulfill()
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        wait(for: [promise], timeout: 5)
    }
    
    
    func testGetProfileData(){
        let promise = expectation(description: "Success Profile User")
        Request().getDataProfile { result in
                switch result {
                case .success:
                    promise.fulfill()
                case .failure(let error):
                    XCTFail("Error: \(error.localizedDescription)")
                }
        }
        wait(for: [promise], timeout: 5)
    }
    
    
    func testValidEmail(){
         let inputEmail = "user@gmail.com"
        XCTAssertTrue(inputEmail.isValidEmail())
     }
     
     func testNotValidEmail(){
         let inputEmail = "user.com"
        XCTAssertFalse(inputEmail.isValidEmail())
     }
    
    func testSuccessConvertDouble(){
        let input = 1000000
        let output = input.convertToCurrency()
        XCTAssertTrue(output == "Rp 1,000,000")
    }
    
}

