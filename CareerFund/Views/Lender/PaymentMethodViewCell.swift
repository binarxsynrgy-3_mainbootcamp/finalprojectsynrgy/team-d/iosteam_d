//
//  PaymentMethodViewCell.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 24/11/21.
//

import UIKit

class PaymentMethodViewCell: UICollectionViewCell {

    @IBOutlet weak var paymentMethodView: UIView!
    @IBOutlet weak var bankLogo: UIImageView!
    @IBOutlet weak var bankName: UILabel!
    @IBOutlet weak var imgChecked: UIImageView!
    var isSelectedItem = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        paymentMethodView.layer.cornerRadius = 8
        paymentMethodView.layer.masksToBounds = true
        imgChecked.isHidden = true
    }
    
    func selected (){
        self.isSelectedItem = !self.isSelectedItem
        if isSelectedItem {
            imgChecked.isHidden = false
        } else {
            imgChecked.isHidden = true
        }
    }

}
