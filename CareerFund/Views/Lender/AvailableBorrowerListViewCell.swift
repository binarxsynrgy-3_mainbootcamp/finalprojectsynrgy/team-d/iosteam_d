//
//  BorrowerListViewCell.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 22/11/21.
//

import UIKit

class AvailableBorrowerListViewCell: UICollectionViewCell {

    @IBOutlet weak var borrowerName: UILabel!
    @IBOutlet weak var bootcampName: UILabel!
    @IBOutlet weak var bootcampClass: UILabel!
    @IBOutlet weak var fundingAmount: UILabel!
    @IBOutlet weak var tenureInstallment: UILabel!
    @IBOutlet weak var loanInterest: UILabel!
    @IBOutlet weak var borrowerScore: UILabel!
    @IBOutlet weak var scoringView: UIView!
    @IBOutlet weak var borrowerCollectionView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        scoringView.layer.cornerRadius = 8
        scoringView.layer.masksToBounds = true
        scoringView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        borrowerCollectionView.layer.cornerRadius = 5
        bootcampClass.setContentHuggingPriority(.defaultHigh, for: .vertical)
    }
    
    func setupView(data : LoanData){
        let nsString = String(data.interestPercent) as NSString
        if nsString.length >= 4
        {
            loanInterest.text = nsString.substring(with: NSRange(location: 0, length: nsString.length > 4 ? 4 : nsString.length)) + "%"
        }
        
        borrowerName.text = data.borrower.name
        bootcampName.text = data.aclass.bootcamp.institutions[0].name
        bootcampClass.text = data.aclass.bootcamp.name
        fundingAmount.text = data.targetFund.convertToCurrency()
        tenureInstallment.text = "\(data.tenorMonth ?? 0) Bulan"
        borrowerScore.text = "\(data.borrower.assessmentScore ?? 0)"
    }
    
}
