//
//  WithdrawalViewCell.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 19/12/21.
//

import UIKit

class WithdrawalViewCell: UICollectionViewCell {
    
    @IBOutlet weak var withdrawalView: UIView!
    @IBOutlet weak var borrowerNameLabel: UILabel!
    @IBOutlet weak var loanAmountLabel: UILabel!
    @IBOutlet weak var loanTenureLabel: UILabel!
    @IBOutlet weak var loanInterestLabel: UILabel!
    @IBOutlet weak var checkmark: UIImageView!
    var isSelectedItem = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        withdrawalView.layer.cornerRadius = 8
        withdrawalView.layer.masksToBounds = true
        checkmark.isHidden = true
    }
    
    func selected (){
        self.isSelectedItem = !self.isSelectedItem
        if isSelectedItem {
            checkmark.isHidden = false
            withdrawalView.layer.borderWidth = 3
            withdrawalView.layer.borderColor = UIColor().hexStringToUIColor(hex: "3163AF").cgColor
        } else {
            checkmark.isHidden = true
        }
    }
    
    func setupView(data : DashboardLenderLoansResponse){
        borrowerNameLabel.text = data.loan.borrower.name
        loanAmountLabel.text = data.financialTransaction.nominal.convertToCurrency()
        loanTenureLabel.text = "\(data.loan.tenorMonth ?? 0) Bulan"
        loanInterestLabel.text = String(format: "%.2f", data.loan.interestPercent) + "%"
    }
}
