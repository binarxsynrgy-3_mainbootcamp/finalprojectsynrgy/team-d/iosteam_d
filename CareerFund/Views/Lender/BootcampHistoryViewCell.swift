//
//  BootcampHistoryViewCell.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 06/12/21.
//

import UIKit

class BootcampHistoryViewCell: UICollectionViewCell {

    @IBOutlet weak var bootcampHistoryView: UIView!
    
    @IBOutlet weak var bootcampLogo: UIImageView!
    @IBOutlet weak var bootcampTitle: UILabel!
    @IBOutlet weak var bootcampFieldStudy: UILabel!
    @IBOutlet weak var bootcampPeriod: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bootcampHistoryView.layer.cornerRadius = 8
        bootcampHistoryView.layer.masksToBounds = true
    }

}
