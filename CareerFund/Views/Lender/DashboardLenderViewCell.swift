//
//  DashboardLenderViewCell.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 17/11/21.
//

import UIKit

class DashboardLenderViewCell: UICollectionViewCell {

    @IBOutlet weak var borrowerView: UIView!
    @IBOutlet weak var borrowerName: UILabel!
    @IBOutlet weak var borrowingAmount: UILabel!
    @IBOutlet weak var installmentTenure: UILabel!
    @IBOutlet weak var interest: UILabel!
    @IBOutlet weak var InstallmentProgress: UIProgressView!
    @IBOutlet weak var borrowerCollectionView: UIView!
    
    let numberFormatter = NumberFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        borrowerView.layer.cornerRadius = 8
        borrowerView.layer.masksToBounds = true
        borrowerCollectionView.layer.cornerRadius = 5
        numberFormatter.numberStyle = .percent
    }
    
    
    func setupView(data : DashboardLenderLoansResponse){
        borrowerName.text = data.loan.borrower.name
        borrowingAmount.text = data.financialTransaction.nominal.convertToCurrency()
        installmentTenure.text = "\(data.loan.tenorMonth ?? 0) Bulan"
        InstallmentProgress.progress = data.loan.progress!
        interest.text = String(format: "%.2f", data.loan.interestPercent) + "%"
    }

}



