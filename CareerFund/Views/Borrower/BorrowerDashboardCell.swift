//
//  BorrowerDashboardCell.swift
//  CareerFund
//
//  Created by MAC on 18/11/21.
//

import UIKit

class BorrowerDashboardCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblClassName: UILabel!
    @IBOutlet weak var lblTerm: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblDurationTitle: UILabel!
    @IBOutlet weak var lblPeriodTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    
    var openAction : (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblDetail.isUserInteractionEnabled = true
        lblDetail.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openDetail)))
        mainView.layer.cornerRadius = 10
    }
    
    func setupView(data : DataLoanModel, from : String){
        if from == "Loan" {
            lblPeriodTitle.text = "Bunga"
            lblDurationTitle.text = "Lama Cicilan"
            lblTerm.text = "\(data.loan?.tenorMonth ?? 0) Bulan"
            if let interest =  data.loan?.interestPercent {
                lblDuration.text = "\( Double(round(100 * interest) / 100)) %"
            }
        } else {
            lblTerm.text = "\(data.aclass?.duration ?? 0) Bulan"
            if let startDate = data.aclass?.startDate, let endDate = data.aclass?.endDate {
                lblDuration.text = "\(startDate.date(dateFormat: "yyyy-MM-dd").string(dateFormat: "MMM")) - \(endDate.date(dateFormat: "yyyy-MM-dd").string(dateFormat: "MMM yyyy"))"
            }
        }
        
        lblCompanyName.text = data.aclass?.bootcamp?.institutions[0].name
        lblClassName.text = data.aclass?.bootcamp?.name
        lblAmount.text = data.aclass?.price.convertToCurrency()
        
        if let url = data.aclass?.bootcamp?.institutions[0].logo {
            let urlImg = URL(string:url)!
            if let data = try? Data(contentsOf: urlImg) {
                imgLogo.image = UIImage(data: data)
            }
        }
        
    }
    
    @objc func openDetail(){
        self.openAction?()
    }
}
