//
//  BorrowerClassCell.swift
//  CareerFund
//
//  Created by MAC on 23/11/21.
//

import UIKit

class BorrowerClassCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblClassName: UILabel!
    @IBOutlet weak var lblFee: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblPeriode: UILabel!
    @IBOutlet weak var btnApply: ButtonBlue!
    
    var openAction : (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainView.layer.cornerRadius = 10
        btnApply.addTarget(self, action: #selector(actionApply), for: .touchUpInside)
    }
    
    func setupView (data : Class){
        lblCompanyName.text = data.bootcamp?.institutions[0].name
        lblClassName.text = data.bootcamp?.name
        lblFee.text = data.price.convertToCurrency()
        lblDuration.text = "\(data.duration) Bulan"
        lblPeriode.text = "\(data.startDate.date(dateFormat: "yyyy-MM-dd").string(dateFormat: "MMM")) - \(data.endDate.date(dateFormat: "yyyy-MM-dd").string(dateFormat: "MMM yyyy"))"
       
        if data.registered == true {
            btnApply.isEnabled = false
        } else {
            btnApply.isEnabled = true
        }
        
        if let url = data.bootcamp?.institutions[0].logo {
            let urlImg = URL(string:url)!
            if let data = try? Data(contentsOf: urlImg) {
                imgLogo.image = UIImage(data: data)
            }
        }
    }
    
    @objc func actionApply(){
        self.openAction?()
    }

}
