//
//  FilterViewCell.swift
//  CareerFund
//
//  Created by MAC on 25/11/21.
//

import UIKit

class FilterViewCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgChecked: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    var isSelectedItem = false

    override func awakeFromNib() {
        super.awakeFromNib()
      
        mainView.layer.cornerRadius = 8
    }

    func selected (){
        self.isSelectedItem = !self.isSelectedItem
        if isSelectedItem {
            imgChecked.isHidden = false
        } else {
            imgChecked.isHidden = true
        }
    }
}
