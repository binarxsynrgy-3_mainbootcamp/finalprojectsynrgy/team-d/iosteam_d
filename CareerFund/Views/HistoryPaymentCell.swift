//
//  HistoryPaymentCell.swift
//  CareerFund
//
//  Created by MAC on 19/12/21.
//

import UIKit

class HistoryPaymentCell: UITableViewCell {

    @IBOutlet weak var lblDatePayment: UILabel!
    @IBOutlet weak var lblAmountPay: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func loadData(data : LoanPayments){
        lblAmountPay.text = data.payment?.transaction.nominal.convertToCurrency()
        lblDatePayment.text = data.payment?.transaction.date.date(dateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSz").string(dateFormat: "dd MMM yyyy")

    }
    
}
