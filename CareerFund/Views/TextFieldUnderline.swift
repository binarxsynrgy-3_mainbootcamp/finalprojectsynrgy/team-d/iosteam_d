//
//  TextFieldUnderline.swift
//  CareerFund
//
//  Created by MAC on 02/11/21.
//

import MaterialComponents

open class CustomTextField: MDCTextField {
    
    @IBInspectable var paddingLeft: CGFloat = 0
    @IBInspectable var paddingRight: CGFloat = 0
    
    var textView: MDCTextInputControllerOutlined?
    var didChangeValue: ((_ textField: UITextField) -> Void)?
    
    open override var isEnabled: Bool {
        didSet {
            textView?.borderFillColor = isEnabled ? .white: UIColor.gray
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        awakeFromNib()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        textView = MDCTextInputControllerOutlined(textInput: self)
        textView?.normalColor = UIColor.gray
        textView?.disabledColor = UIColor.white
        textView?.inlinePlaceholderColor = UIColor.gray
        
        
        defaultActiveColor()
        disableAutoFill()
        
        addTarget(self, action: #selector(onUpdatedValue), for: .editingChanged)
    }
    
    @objc private func onUpdatedValue(_ textField: UITextField) {
        didChangeValue?(textField)
    }
    
    func disableAutoFill() {
        self.textContentType = .oneTimeCode
    }
    
    private func defaultActiveColor() {
        self.textView?.activeColor = UIColor.blue
        self.textView?.floatingPlaceholderActiveColor = UIColor.blue
    }
    
    func isError(_ error: Bool) {
        if error {
            let colorError                              = UIColor.red
            textView?.inlinePlaceholderColor           = colorError
            textView?.activeColor                      = colorError
            textView?.normalColor                      = colorError
            textView?.floatingPlaceholderActiveColor   = colorError
            textView?.floatingPlaceholderNormalColor   = colorError
        } else {
            textView?.inlinePlaceholderColor           = UIColor.gray
            textView?.activeColor                      = UIColor().hexStringToUIColor(hex: "3163AF")
            textView?.normalColor                      = UIColor.gray
            textView?.floatingPlaceholderActiveColor   = UIColor().hexStringToUIColor(hex: "3163AF")
            textView?.floatingPlaceholderNormalColor   = UIColor.gray
        }
    }
    
    func setBackgroundGray(_ isGray: Bool) {
        if isGray {
            textView?.borderFillColor = UIColor(red: 0.949, green: 0.949, blue: 0.949, alpha: 1)
        } else {
            textView?.borderFillColor = .white
        }
    }
    
    func setEnable(_ value: Bool) {
        self.isEnabled = value
        if !value {
            setBackgroundGray(true)
            textColor = UIColor.gray
        } else {
            setBackgroundGray(false)
            textColor = UIColor.black
        }
    }
    
    func reset() {
        defaultActiveColor()
    }
}

open class CustomUnderlineTextField: CustomTextField {
    var underlineTextView:MDCTextInputControllerUnderline?
    
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        awakeFromNib()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override open func awakeFromNib() {
        self.underlineTextView = MDCTextInputControllerUnderline(textInput: self)
        self.defaultActiveColor()
        self.clearButtonMode = .never
        self.font = UIFont(name: "SFPRODISPLAYREGULAR", size: 16)
        self.textColor                  = UIColor.gray
        self.placeholderLabel.textColor = UIColor.gray
        self.underlineTextView?.normalColor      = UIColor.gray
        self.underlineTextView?.disabledColor    = UIColor.gray
    }
    
    private func defaultActiveColor() {
        self.underlineTextView?.activeColor = UIColor().hexStringToUIColor(hex: "3163AF")
        self.underlineTextView?.floatingPlaceholderActiveColor = UIColor().hexStringToUIColor(hex: "3163AF")
    }
    
    
    override func isError(_ error: Bool) {
        if error {
            let colorError                                           = UIColor.red
            self.textColor                                           = colorError
            self.underlineTextView?.inlinePlaceholderColor           = colorError
            self.underlineTextView?.activeColor                      = colorError
            self.underlineTextView?.normalColor                      = colorError
            self.underlineTextView?.floatingPlaceholderActiveColor   = colorError
            self.underlineTextView?.floatingPlaceholderNormalColor   = colorError
        } else {
            self.textColor                                           = UIColor.gray
            self.underlineTextView?.inlinePlaceholderColor           = UIColor.gray
            self.underlineTextView?.activeColor                      = UIColor.gray
            self.underlineTextView?.normalColor                      = UIColor.gray
            self.underlineTextView?.floatingPlaceholderActiveColor   = UIColor.gray
            self.underlineTextView?.floatingPlaceholderNormalColor   = UIColor.gray
        }
    }
    
    func disable() {
        self.textColor = UIColor.gray
        self.isUserInteractionEnabled = false
    }
}
