//
//  NotificationViewCell.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 28/11/21.
//

import UIKit

class NotificationViewCell: UICollectionViewCell {

    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var notificationTitle: UILabel!
    @IBOutlet weak var notificationContent: UITextView!
    @IBOutlet weak var notificationButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        notificationView.layer.cornerRadius = 8
        notificationView.layer.masksToBounds = true
        
        notificationView.layer.cornerRadius = 5
        
        
    }
    
    @IBAction func notifButton(_ sender: Any) {
        
    }
    
}
