//
//  MinatViewCell.swift
//  CareerFund
//
//  Created by MAC on 09/11/21.
//

import UIKit

class MinatViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblMinat: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgChecked: UIImageView!
    @IBOutlet weak var imgBg: UIImageView!
    var isSelectedItem = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.cornerRadius = 8
        mainView.layer.masksToBounds = true
    }
    
    func selected (){
        self.isSelectedItem = !self.isSelectedItem
        if isSelectedItem {
            imgChecked.isHidden = false
        } else {
            imgChecked.isHidden = true
        }
    }
    
    func setImgBg(name : String){
        if name.contains("Front"){
            imgBg.image = UIImage(named: "bg-minat-fe")
        } else  if name.contains("Back"){
            imgBg.image = UIImage(named: "bg-minat-be")
        } else  if name.contains("UI UX"){
            imgBg.image = UIImage(named: "bg-minat-uiux")
        } else  if name.contains("Science"){
            imgBg.image = UIImage(named: "bg-minat-data")
        } else  if name.contains("Digital"){
            imgBg.image = UIImage(named: "bg-minat-digital")
        } else  if name.contains("Android"){
            imgBg.image = UIImage(named: "bg-minat-android")
        } else  if name.contains("iOS"){
            imgBg.image = UIImage(named: "bg-minat-iOS")
        }
        
    }
    
}
