//
//  CheckBox.swift
//  CareerFund
//
//  Created by MAC on 06/12/21.
//

import UIKit

class Checkbox: UIView {
    
    private lazy var selectedIndicator: UIImageView = {
        let width = bounds.width
        let height = width
        let x = (bounds.width / 2) - (width / 2)
        let y = x
        let indicator = UIImageView(frame: CGRect(x: x, y: y, width: width, height: height))
        indicator.layer.masksToBounds = true
        return indicator
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configureView()
    }
    
    private func configureView() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.lightGray.cgColor
        // self.layer.cornerRadius = self.bounds.width / 2
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor.white
        self.addSubview(selectedIndicator)
    }
    
    var isSelected: Bool = false {
        didSet {
            if isSelected {
                self.selectedIndicator.image = UIImage(named: "check-box-fill")
            } else {
                self.selectedIndicator.image = nil
            }
        }
    }
    
}
