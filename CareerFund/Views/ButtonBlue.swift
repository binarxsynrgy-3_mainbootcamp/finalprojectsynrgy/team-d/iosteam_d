//
//  ButtonBlue.swift
//  CareerFund
//
//  Created by MAC on 29/10/21.
//

import UIKit

class ButtonBlue: UIButton {

    @IBInspectable
    var imageName: String = ""

    var dashedLine : CAShapeLayer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

     override func awakeFromNib() {
        super.awakeFromNib()
        configure()
    }

    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        configure()
    }

    public override var isEnabled: Bool {
        get {
            return super.isEnabled
        }
        set {
            super.isEnabled = newValue
            if isEnabled {
                self.backgroundColor = UIColor().hexStringToUIColor(hex: "3163AF")
            } else {
                self.backgroundColor = UIColor.lightGray
            }
        }
    }

    override var isSelected: Bool {
        get {
            return super.isSelected
        }
        set {
            super.isSelected = newValue
            if isSelected {
                configure()
            } else {
                self.backgroundColor = UIColor.lightGray
                self.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }

    public func configure() {
        self.setTitleColor(UIColor.white, for: .normal )
        self.setTitleColor(UIColor.white, for: .disabled)
        self.setTitleShadowColor(UIColor.systemBlue, for: .normal)
        self.setTitleShadowColor(UIColor.white, for: .disabled)
        self.titleLabel?.textAlignment = .center
        self.titleLabel?.font = UIFont(name: "SFPRODISPLAY-BOLD", size: 18)
        self.backgroundColor = UIColor().hexStringToUIColor(hex: "3163AF")
        self.layer.shadowColor = UIColor().hexStringToUIColor(hex: "3163AF").cgColor
        self.layer.shadowOpacity = 5
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 1
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.cornerRadius = 8
    }

}
