//
//  Connection.swift
//  CareerFund
//
//  Created by MAC on 28/10/21.
//

import UIKit

class Connection {
    
    private static var privateShared : Connection?
    
    static var shared = Connection()
    
    let task = URLSession.shared
    let timeoutInterval: Double = 60
    
    func connect<T:Decodable>( url: String, params: [String:Any]?, httpMethod: String = "POST", model: T.Type, completion: @escaping (Result<T, Error>) -> Void) {
        print("TOKEN::", Session.shared.token)

        guard let _url = URL(string: url) else {
            fatalError("invalid url: " + url)
        }
        var request = URLRequest(url: _url)
        request.httpMethod = params == nil ? "GET" : httpMethod
        request.httpMethod = (params == nil && httpMethod != "POST") ? httpMethod : request.httpMethod
        request.addValue("Bearer \(Session.shared.token)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if let params = params, let body = try? JSONSerialization.data(withJSONObject: params, options: [.fragmentsAllowed]) {
            request.httpBody = body
        }
        
        self.task.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                if error != nil{
                    print("error : \(String(describing: error))")
                    return
                }
                
                if let data = data, let stringResponse = String(data: data, encoding: .utf8) {
                    print("JSON from ==> "+url+" "+stringResponse)
                }
                
                if let a = response as? HTTPURLResponse {
                    print("status code: ", a.statusCode)
                    
                    if a.statusCode == 200 || a.statusCode == 201 {
                        do {
                            let responseModel = try JSONDecoder().decode(T.self, from: data!)
                            completion(.success(responseModel))
                        } catch let jsonError {
                            completion(.failure(jsonError))
                            print(String(describing: jsonError))
                        }
                    } else {
                        completion(.failure(RequestError.failed_response(e: "\(a.statusCode)")))
                    }
                }
            }
        }.resume()
    }
    
    func connectPost(url: String, params: [String:Any]?, httpMethod: String = "POST", completion: @escaping (Result<String, Error>) -> Void) {

        print("TOKEN::", Session.shared.token)

        guard let _url = URL(string: url) else {
            fatalError("invalid url: " + url)
        }

        var request = URLRequest(url: _url)
        request.httpMethod = httpMethod
        request.timeoutInterval = timeoutInterval
        request.setValue("Bearer \(Session.shared.token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if let params = params, let body = try? JSONSerialization.data(withJSONObject: params, options: [.fragmentsAllowed]) {
            request.httpBody = body
            
            print("params-body->",body)

        }
        
        self.task.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            print("JSON from ==> "+url)

            if let stringResponse = String(data: data!, encoding: .utf8) {
                if let a = response as? HTTPURLResponse {
                    print("status code: ", a.statusCode)
                    
                    if a.statusCode == 200 || a.statusCode == 201 {
                        completion(.success(stringResponse))
                        print("response_success\(stringResponse)")
                    } else {
                        completion(.failure(RequestError.failed_response(e: stringResponse)))
                        print("response_gagal\(String(describing: stringResponse))")
                    }
                }
                return
            }
        }.resume()
    }
    
    func uploadFile<T:Decodable>( url: String, fileData:Data, fileName:String , model: T.Type, completion: @escaping (Result<T, Error>) -> Void) {
        print("FILENAME: \(fileName)")

        let boundary: String = "------VohpleBoundary4QuqLuM1cE5lMwCy"
        let contentType: String = "multipart/form-data; boundary=\(boundary)"
        
        guard let _url = URL(string: url) else {
            fatalError("invalid url: " + url)
        }

        var request = URLRequest(url: _url)
        request.httpMethod = "PUT"
        request.timeoutInterval = timeoutInterval
        request.setValue("Bearer \(Session.shared.token)", forHTTPHeaderField: "Authorization")
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        let body = NSMutableData()
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("\(fileName)\r\n".data(using: String.Encoding.utf8)!)
        
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition: form-data; name=\"file\"; filename=\"img.jpg\"\r\n".data(using: String.Encoding.utf8)!)
        
        // File is an image
        if fileName.hasSuffix(".jpg") {
            body.append("Content-Type:image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
            // File is a video
        } else if fileName.hasSuffix(".mp4") {
            body.append("Content-Type:video/mp4\r\n\r\n".data(using: String.Encoding.utf8)!)
        }
        
        body.append(fileData)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        
        
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        request.httpBody = body as Data
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
            guard let _:Data = data as Data?, let _:URLResponse = response, error == nil else {
                print("response_gagal\(String(describing: error?.localizedDescription))")
                return
            }
            
            
            if let data = data, let stringResponse = String(data: data, encoding: .utf8) {
                print("JSON from ==> "+url+" "+stringResponse)
            }
            
            if let a = response as? HTTPURLResponse {
                print("status code: ", a.statusCode)
                
                if a.statusCode == 200 || a.statusCode == 201 {
                    do {
                        let responseModel = try JSONDecoder().decode(T.self, from: data!)
                        completion(.success(responseModel))
                    } catch let jsonError {
                        completion(.failure(jsonError))
                        print(String(describing: jsonError))
                    }
                } else {
                    completion(.failure(RequestError.failed_response(e: "\(a.statusCode)")))
                    print("response_gagal\(String(describing: a))")
                }
            }
        }; task.resume()
    }
}

enum RequestError: Error {
    case failed_response(e: String)
}

extension RequestError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case let .failed_response(e):
            return e
        }
    }
}
