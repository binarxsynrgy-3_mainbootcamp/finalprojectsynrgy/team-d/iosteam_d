//
//  AppDelegate.swift
//  CareerFund
//
//  Created by MAC on 27/10/21.
//

import UIKit
import CoreData

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Session.shared.initSession()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.backgroundColor = .systemBackground
        window?.overrideUserInterfaceStyle = .light
        
        let navigationController = UINavigationController(rootViewController: MainPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal  , options: nil))
        window?.rootViewController = navigationController
        
        return true
    }
}
