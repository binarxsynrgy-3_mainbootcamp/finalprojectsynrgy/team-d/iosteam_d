//
//  Session.swift
//  CareerFund
//
//  Created by MAC on 20/11/21.
//

import Foundation

class Session {
    static let shared = Session()
    
    private let userDefaults            = UserDefaults.standard
    private let key_access_token        = "token"
    private let key_refresh_token       = "refresh_token"
    private let key_account_type        = "account_type"
    private let key_minat_status        = "minat_status"
    
    private let key_user_name           = "key_user_name"
    private let key_user_email          = "key_user_email"
    private let key_user_roles          = "key_user_roles"
    private let key_user_phone          = "key_user_phone"
    private let key_user_address        = "key_user_address"
    private let key_user_balance        = "key_user_balance"
    private let key_user_profile        = "key_user_profile"
    private let key_user_ktp            = "key_user_ktp"
    private let key_user_selfie         = "key_user_selfie"
    private let key_user_score          = "key_user_score"
    private let key_user_debt           = "key_user_debt"
    private let key_user_asset          = "key_user_asset"
    private let key_user_verified       = "key_user_verified"

    var token = ""
    var refresh_token = ""
    var account_type : [String]?
    var minat_status : Bool?
    
    var name = ""
    var email = ""
    var roles = ""
    var phone = ""
    var address = ""
    var balance = 0.0
    var photoProfile = ""
    var ktp = ""
    var ktpSelfie = ""
    var assesmentScore = 0.0
    var remainingDebt = 0.0
    var assets = 0.0
    var isVerified = ""

    var loanId = 0
    
    func initSession() {
        let token = userDefaults.string(forKey: key_access_token) ?? ""
        let refresh_token = userDefaults.string(forKey: key_refresh_token) ?? ""
        let account_type = userDefaults.stringArray(forKey: key_account_type)
        let minat_status = userDefaults.bool(forKey: key_minat_status)

        let name = userDefaults.string(forKey: key_user_name) ?? ""
        let email = userDefaults.string(forKey: key_user_email) ?? ""
        let roles = userDefaults.string(forKey: key_user_roles) ?? ""
        let phone = userDefaults.string(forKey: key_user_phone) ?? ""
        let address = userDefaults.string(forKey: key_user_address) ?? ""
        let balance = userDefaults.double(forKey: key_user_balance)
        let photoProfile = userDefaults.string(forKey: key_user_profile) ?? ""
        let ktp = userDefaults.string(forKey: key_user_ktp) ?? ""
        let ktpSelfie = userDefaults.string(forKey: key_user_selfie) ?? ""
        let assesmentScore = userDefaults.double(forKey: key_user_score)
        let remainingDebt = userDefaults.double(forKey: key_user_debt)
        let assets = userDefaults.double(forKey:key_user_asset)
        let isVerified = userDefaults.string(forKey: key_user_verified) ?? ""

        self.token = token
        self.refresh_token = refresh_token
        self.account_type = account_type
        self.minat_status = minat_status
        
        self.name = name
        self.email = email
        self.roles = roles
        self.phone = phone
        self.address = address
        self.balance = balance
        self.photoProfile = photoProfile
        self.ktp = ktp
        self.ktpSelfie = ktpSelfie
        self.assesmentScore = assesmentScore
        self.remainingDebt = remainingDebt
        self.assets = assets
        self.isVerified = isVerified

    }
    
    func saveUserSession(session : SigninResponse) {
        userDefaults.set(session.accessToken, forKey: key_access_token)
        userDefaults.set(session.refreshToken, forKey: key_refresh_token)
        userDefaults.set(session.roles, forKey: key_account_type)
        
        self.token = session.accessToken
        self.refresh_token = session.refreshToken
        self.account_type = session.roles
    }
    
    func saveMinatStatus(status : Bool){
        userDefaults.set(status, forKey: key_minat_status)
        self.minat_status = status
    }
    
    func saveUserProfile(session : ProfileResponse){
        userDefaults.set(session.name, forKey: key_user_name)
        userDefaults.set(session.email, forKey: key_user_email)
        userDefaults.set(session.roles[1].authority, forKey: key_user_roles)
        userDefaults.set(session.address, forKey: key_user_address)
        userDefaults.set(session.balance?.nominal, forKey: key_user_balance)
        userDefaults.set(session.photoProfile, forKey: key_user_profile)
        userDefaults.set(session.ktpSelfie, forKey: key_user_ktp)
        userDefaults.set(session.assesmentScore, forKey: key_user_score)
        userDefaults.set(session.remainingDebt, forKey: key_user_debt)
        userDefaults.set(session.assets, forKey: key_user_asset)
        userDefaults.set(session.isVerified, forKey: key_user_verified)
        
        self.name = session.name ?? ""
        self.email = session.email ?? ""
        self.roles = session.roles[1].authority
        self.phone = session.phone ?? ""
        self.address = session.address ?? ""
        self.balance = session.balance?.nominal ?? 0.0
        self.photoProfile = session.photoProfile ?? ""
        self.ktp = session.ktp ?? ""
        self.ktpSelfie = session.ktpSelfie ?? ""
        self.assesmentScore = session.assesmentScore ?? 0.0
        self.remainingDebt = session.remainingDebt ?? 0.0
        self.assets = session.assets ?? 0.0
        self.isVerified = session.isVerified ?? ""
    }
    
    func saveLoanId(id : Int){
        self.loanId = id
    }
    
    func removeIdSession(){
        self.loanId = 0
    }
    
    func destroySession() {
        userDefaults.removeObject(forKey: key_access_token)
        userDefaults.removeObject(forKey: key_refresh_token)
        userDefaults.removeObject(forKey: key_account_type)
        userDefaults.removeObject(forKey: key_minat_status)
        userDefaults.removeObject(forKey: key_user_name)
        userDefaults.removeObject(forKey : key_user_email)
        userDefaults.removeObject(forKey: key_user_roles)
        userDefaults.removeObject(forKey: key_user_address)
        userDefaults.removeObject(forKey: key_user_balance)
        userDefaults.removeObject(forKey: key_user_profile)
        userDefaults.removeObject(forKey: key_user_ktp)
        userDefaults.removeObject(forKey: key_user_score)
        userDefaults.removeObject(forKey: key_user_debt)
        userDefaults.removeObject(forKey: key_user_asset)
        userDefaults.removeObject(forKey: key_user_verified)
        
        self.token = ""
        self.refresh_token = ""
        self.account_type = [""]
        self.minat_status = false
        
        self.name = ""
        self.email = ""
        self.roles = ""
        self.phone = ""
        self.address = ""
        self.balance = 0.0
        self.photoProfile = ""
        self.ktp = ""
        self.ktpSelfie = ""
        self.assesmentScore =  0.0
        self.remainingDebt = 0.0
        self.assets = 0.0
        self.isVerified = ""
    }
    
}

