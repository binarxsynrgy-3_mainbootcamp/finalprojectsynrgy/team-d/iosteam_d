//
//  LoanResponse.swift
//  CareerFund
//
//  Created by MAC on 19/12/21.
//

import Foundation


struct ListLoanResponse : Decodable  {
    let data : [DataLoanModel]
}

struct DataLoanModel : Decodable{
    let id : Int
    let loan : Loan?
    let score : Int?
    let aclass : MyClass?
    let dpPaid : Bool
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case loan = "loan"
        case score = "score"
        case aclass  = "aclass"
        case dpPaid  = "dp_paid"
    }

}

struct Loan : Decodable {
    let id : Int
    let interestPercent : Double
    let tenorMonth : Int
    let downPayment : Int
    let totalPayment : Int
    let monthlyPayment : Int
    let monthlyFee : Int
    let fee : Int
    let progress : Float
    let loanPayment : [LoanPayments]

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case interestPercent = "interest_percent"
        case tenorMonth  = "tenor_month"
        case downPayment  = "down_payment"
        case totalPayment  = "total_payment"
        case monthlyPayment  = "monthly_payment"
        case monthlyFee  = "monthly_fee"
        case fee  = "fee"
        case progress = "progress"
        case loanPayment  = "loan_payments"
    }
}

struct LoanPayments : Decodable {
    let id : Int
    let period : Int
    let payment : Payments?
}

struct Payments : Decodable{
    let id : Int
    let transaction : Transaction
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case transaction = "financial_transaction"
    }
}

struct Transaction : Decodable {
    let id : Int
    let nominal : Int
    let date : String
    
    enum CodingKeys: String, CodingKey {
        case date = "created_at"
        case id = "id"
        case nominal = "nominal"
    }
}
