//
//  MinatResponse.swift
//  CareerFund
//
//  Created by MAC on 29/10/21.
//

import Foundation

struct MinatResponse : Decodable  {
    let interests : [InterestData]!
}

struct InterestData : Decodable {
    let id : Int!
    let name : String!
}

struct InterestMyResponse : Decodable {
    let user : Int!
    let interests : [InterestData]!
}
