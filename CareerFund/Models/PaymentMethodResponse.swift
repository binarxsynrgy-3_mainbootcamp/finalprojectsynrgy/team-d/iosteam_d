//
//  PaymentMethodResponse.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 25/11/21.
//

import Foundation
import UIKit


struct PaymentTypeResponse : Decodable {
    let data : [DataPayment]
}

struct DataPayment : Decodable {
    let id : Int
    let name : String
    let paymentAccount : [PaymentAccount]
    
    enum CodingKeys: String, CodingKey {
        case id  = "id"
        case name  = "name"
        case paymentAccount  = "payment_accounts"
    }
}

struct PaymentAccount : Decodable {
    let id : Int
    let name : String
    let number : Int
    let bank : Bank
}

struct Bank : Decodable {
    let id : Int
    let name : String
    let logoPath : String
    
    enum CodingKeys: String, CodingKey {
        case id  = "id"
        case name  = "name"
        case logoPath  = "logo_path"
    }
}
