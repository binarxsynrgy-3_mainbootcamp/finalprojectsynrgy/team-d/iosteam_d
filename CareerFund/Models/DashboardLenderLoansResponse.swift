struct LenderLoansResponse: Decodable {
    let success: Bool!
    let data: [DashboardLenderLoansResponse]
}

struct DashboardLenderLoansResponse: Decodable  {
    let id : Int!
    let loan : LoanData!
    let financialTransaction: FinancialTransaction!
    let withdraw : String!
    
    enum CodingKeys: String, CodingKey {
        case id  = "id"
        case loan = "loan"
        case financialTransaction = "financial_transaction"
        case withdraw = "withdraw"
    }
}

struct LoanPayment: Decodable  {
    let id : Int!
    let period : Int!
    let payment : Payment!
    
}

struct Payment: Decodable  {
    let id : Int!
}


struct FinancialTransaction: Decodable  {
    let id : Int!
    let nominal : Int!
    
}


struct Borrower : Decodable {
    let name : String?
    let phone : String?
    let ktp : String?
    let address : String?
    let assesmentScore : Double?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case phone = "phone_number"
        case ktp = "photo_path"
        case address = "address"
        case assesmentScore = "assessment_score"
        
    }
}
