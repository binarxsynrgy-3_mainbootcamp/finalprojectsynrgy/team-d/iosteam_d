//
//  AvailableBorrowerListResponse.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 22/11/21.
//

import Foundation

struct AvailableBorrowerResponse: Decodable {
    let success: Bool!
    let data: [LoanData]!
}

struct LoanData: Decodable  {
    let id: Int!
    let borrower: BorrowerData!
    let interestPercent: Float!
    let tenorMonth: Int!
    let downPayment : Int!
    let totalPayment : Int!
    let targetFund: Int!
    let fundable: Bool!
    let progress: Float!
    let fundedByMe: Bool!
    let fundLeft: Int!
    let monthPaid : Int!
    let monthlyPayment : Int!
    let loanPayments : [LoanPayment]
    let aclass: ClassData!
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case borrower = "borrower"
        case interestPercent = "interest_percent"
        case tenorMonth = "tenor_month"
        case downPayment = "down_payment"
        case totalPayment  = "total_payment"
        case targetFund = "target_fund"
        case fundable = "fundable"
        case progress = "progress"
        case fundedByMe = "funded_by_me"
        case fundLeft = "fund_left"
        case monthPaid = "months_paid"
        case monthlyPayment = "monthly_payment"
        case loanPayments = "loan_payments"
        case aclass = "aclass"
    }
}

struct BorrowerData: Decodable  {
    let name: String!
    let phoneNumber: String!
    let photoPath: String!
    let address: String!
    let assessmentScore: Int!
    let userClasses: [UserClassesData]!
    let authorities: [AuthoritiesData]!
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case phoneNumber = "phone_number"
        case photoPath = "photo_path"
        case address = "address"
        case assessmentScore = "assessment_score"
        case userClasses = "user_classes"
        case authorities = "authorities"
    }
}

struct UserClassesData : Decodable{
    let id : Int!
    let score : Int!
    let aclass :ClassData!
}

struct FundingData: Decodable  {
    let id: Int!
    let financialTransaction : FinancialTransaction!
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case financialTransaction = "financial_transaction"
    }
}

struct AuthoritiesData: Decodable  {
    let name: String!
    let authority: String!
}

struct ClassData: Decodable  {
    let id: Int!
    let name: String!
    let startDate: String!
    let endDate: String!
    let durationMonth: Int!
    let quota: Int!
    let price: Int!
    let bootcamp: BotcampData!
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case startDate = "start_date"
        case endDate = "end_date"
        case durationMonth = "duration_month"
        case quota = "quota"
        case price = "price"
        case bootcamp = "bootcamp"
    }
}

struct BotcampData: Decodable {
    let id: Int!
    let name: String!
    let logoPath: String!
    let institutions: [InstitutionsData]!
    let categories: [CategoriesData]!
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case logoPath = "logo_path"
        case institutions = "institutions"
        case categories = "categories"
    }
}

struct InstitutionsData: Decodable {
    let id: Int!
    let name: String!
    let logoPath: String!
    let balance: Int!
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case logoPath = "logo_path"
        case balance = "balance"
    }
}

struct CategoriesData: Decodable {
    let id: Int!
    let name: String!
}
