//
//  ClassResponse.swift
//  CareerFund
//
//  Created by MAC on 28/11/21.
//

import Foundation


struct ClassResponse : Decodable {
    let data : [Class]?
    let page : Int?
    let totalPage: Int?
    let totalElement : Int?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case page = "page"
        case totalPage = "total_pages"
        case totalElement = "total_elements"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent([Class].self, forKey: .data)
        page = try values.decodeIfPresent(Int.self, forKey: .page)
        totalPage = try values.decodeIfPresent(Int.self, forKey: .totalPage)
        totalElement = try values.decodeIfPresent(Int.self, forKey: .totalElement)
    }
}

struct Class : Decodable {
    let id : Int
    let name : String?
    let startDate : String
    let endDate : String
    let duration : Int
    let quota : Int
    let price : Int
    let bootcamp : Bootcamp?
    let registered : Bool
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case bootcamp = "bootcamp"
        case startDate = "start_date"
        case endDate = "end_date"
        case duration = "duration_month"
        case quota = "quota"
        case price = "price"
        case registered = "registered"
    }
}

struct MyClass : Decodable {
    let id : Int
    let name : String?
    let startDate : String
    let endDate : String
    let duration : Int
    let quota : Int
    let price : Int
    let bootcamp : Bootcamp?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case bootcamp = "bootcamp"
        case startDate = "start_date"
        case endDate = "end_date"
        case duration = "duration_month"
        case quota = "quota"
        case price = "price"
    }
}

struct Bootcamp : Decodable  {
    let id : Int
    let name : String
    let icon : String?
    let institutions : [Institutions]
    let categories : [Categories]
}

struct Institutions : Decodable  {
    let id : Int
    let name : String
    let logo: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case logo = "logo_path"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
        logo = try values.decodeIfPresent(String.self, forKey: .logo) ?? ""
    }
}

struct Categories : Decodable  {
    let id : Int
    let name : String
    let logo: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case logo = "logo_path"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
        logo = try values.decodeIfPresent(String.self, forKey: .logo) ?? ""
    }
}


struct SubmitClassResponse : Decodable {
    let data : DataLoanModel
}

struct ListClassResponse : Decodable  {
    let data : [DataClassModel]
}


struct DataClassModel : Decodable{
    let id : Int
    let loan : Loan?
    let score : Int?
    let aclass : Class?
}
