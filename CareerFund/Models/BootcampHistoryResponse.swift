//
//  BootcampHistoryResponse.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 06/12/21.
//

import Foundation

struct BootcampHistoryResponse{
    let logo: String
    let name: String
    let period: String
    
    init(
        logo : String,
        name: String,
        period: String
    ) {
        self.logo = logo
        self.name = name
        self.period = period
    }
}
