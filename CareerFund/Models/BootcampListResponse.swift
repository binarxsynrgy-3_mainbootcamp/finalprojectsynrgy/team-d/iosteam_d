//
//  BootcampListResponse.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 02/12/21.
//

import Foundation

struct BootcampListResponse{
    let bootcampLogo : String
    let bootcampName : String
    
    init(
        bootcampLogo : String,
        bootcampName : String
    ) {
        self.bootcampLogo = bootcampLogo
        self.bootcampName = bootcampName
    }
}

struct FieldStudyResponse{
    let fieldStudyName : String
    
    init(
        fieldStudyName : String
    ) {
        self.fieldStudyName = fieldStudyName
    }
}
