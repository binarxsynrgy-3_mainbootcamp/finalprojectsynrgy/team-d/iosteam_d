//
//  UserResponse.swift
//  CareerFund
//
//  Created by MAC on 02/11/21.
//

import Foundation

struct UserResponse : Decodable  {
    let id : Int!
    let name : String!
    let email : String!
    let roles : [RolesData]!
    let phone_number : String!
    let address : String!
    let interests : [String]
    let balance : String!
    let photo_path : String!
    let identity_card_path : String!
    let assessment_score : String!
    let remaining_debt : Int!
    let assets : Int!
}

struct RolesData : Decodable  {
    let name: String!
    let authority : String!
}

struct ProfileResponse : Decodable  {
    let name : String?
    let email : String?
    let roles : [RolesData]
    let phone : String?
    let address : String?
    let interests : [InterestData]
    let balance : Balance?
    let photoProfile : String?
    let ktp : String?
    let ktpSelfie : String?
    let assesmentScore : Double?
    let remainingDebt : Double?
    let assets : Double?
    let isVerified : String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case email = "email"
        case roles = "roles"
        case phone = "phone_number"
        case address = "address"
        case interests = "interests"
        case balance = "balance"
        case photoProfile = "photo_path"
        case ktp = "identity_card_path"
        case ktpSelfie = "selfie_path"
        case assesmentScore = "assessment_score"
        case remainingDebt = "remaining_debt"
        case assets = "assets"
        case isVerified = "id_verification_status"
    }
}

struct Balance : Decodable {
    let id : Int
    let nominal : Double
}


struct SigninResponse : Decodable  {
    let accessToken : String
    let refreshToken : String
    let tokenType: String
    let roles : [String]?
    
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
        case tokenType = "token_type"
        case roles = "roles"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        accessToken = try values.decodeIfPresent(String.self, forKey: .accessToken) ?? ""
        refreshToken = try values.decodeIfPresent(String.self, forKey: .refreshToken)  ?? ""
        tokenType = try values.decodeIfPresent(String.self, forKey: .tokenType)  ?? ""
        roles = try values.decodeIfPresent([String].self, forKey: .roles)
    }
}

struct UploadImageResponse : Decodable {
    let data : UrlImage
}

struct UrlImage : Decodable {
    let url : String
}
