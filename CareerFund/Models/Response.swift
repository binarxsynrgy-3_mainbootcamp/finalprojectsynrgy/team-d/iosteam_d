//
//  Response.swift
//  CareerFund
//
//  Created by MAC on 28/10/21.
//

import Foundation


struct Response<T: Decodable>: Decodable {

    var status: Int!
    var data: T?
    var message: String?
    var errors: [Errors]?

    enum Keys: String, CodingKey {
        case status
        case data
        case message
        case errors
    }

    init() {
        status = 0
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        self.status   = try container.decodeIfPresent(Int.self, forKey: .status) ?? 0
        self.data     = try container.decodeIfPresent(T.self, forKey: .data)
        self.message  = try container.decodeIfPresent(String.self, forKey: .message)
        self.errors = try container.decodeIfPresent([Errors].self, forKey: .errors)
    }
}

struct Errors: Decodable {
    var userMessage: String!
    var internalMessage: String!
    var code: Int!
    var moreInfo: String!

    enum Keys: String, CodingKey {
        case userMessage
        case internalMessage
        case code
        case moreInfo
    }

    init(from decoder: Decoder) throws {
        let c = try decoder.container(keyedBy: Keys.self)
        self.userMessage = try c.decodeIfPresent(String.self, forKey: .userMessage) ?? ""
        self.internalMessage = try c.decodeIfPresent(String.self, forKey: .internalMessage) ?? ""
        self.code = try c.decodeIfPresent(Int.self, forKey: .code) ?? 0
        self.moreInfo = try c.decodeIfPresent(String.self, forKey: .moreInfo) ?? ""
    }
}
