//
//  NotificationModel.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 28/11/21.
//

import Foundation

struct NotificationResponse{
    let notifTitle : String
    let notifContent : String
    let linkContent : String
}
