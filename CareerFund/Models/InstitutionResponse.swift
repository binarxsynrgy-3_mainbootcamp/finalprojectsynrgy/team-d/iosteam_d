//
//  InstitutionResponse.swift
//  CareerFund
//
//  Created by MAC on 07/12/21.
//

import Foundation

struct InstitutionResponse : Decodable {
    let data : [Institutions]!
}
