//
//  ForgotPasswordViewController.swift
//  CareerFund
//
//  Created by MAC on 02/11/21.
//

import UIKit

class ForgotPasswordViewController: BaseViewController {
    
    @IBOutlet weak var emailTextField: CustomUnderlineTextField!
    @IBOutlet weak var btnForgot: ButtonBlue!
    @IBOutlet weak var lblWarning: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        emailTextField.placeholder = "Email"
        emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        btnForgot.addTarget(self, action: #selector(actionNext), for: .touchUpInside)
        
        checkForm()
        addKeyboardObserver(show: false)
        navigationController?.navigationBar.isHidden = false
    }
    
    @objc func actionNext(){
        checkEmail()
        self.showLoading()
        Request().forgetPassword(email: emailTextField.text!) { result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    self.dismiss(animated: false, completion: {
                        self.showSuccessSendEmail()
                    })
                case .failure:
                    self.dismiss(animated: false, completion: nil)
                    self.showErrorWarning(msg: "Email tidak ditemukan")
                }
            }
        }
    }
    
    func checkEmail(){
        let email = emailTextField.text!.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        if email.isValidEmail(){
            lblWarning.isHidden = true
        } else {
            lblWarning.isHidden = false
            lblWarning.text = "Email tidak valid"
            return
        }
    }
    
    func checkForm(){
        if emailTextField.text != "" {
            btnForgot.isEnabled = true
        } else {
            btnForgot.isEnabled = false
        }
        
    }
    
     func goToLoginPage(){
        let loginPage = LoginViewController()
        loginPage.modalPresentationStyle = .fullScreen
        loginPage.modalTransitionStyle = .flipHorizontal
        self.present(loginPage, animated: true, completion: nil)
    }
    
    func showErrorWarning(msg : String) {
        if msg != "" {
            lblWarning.isHidden = false
            lblWarning.text = "Email tidak valid"
        } else {
            lblWarning.isHidden = true
        }
    }
    
    func showSuccessSendEmail(){
        let attributedString = NSAttributedString(string: "Email Terkirim", attributes: [NSAttributedString.Key.font : UIFont.init(name: "SFProDisplay-Bold", size: 16) as Any,
           NSAttributedString.Key.foregroundColor :  UIColor().hexStringToUIColor(hex: "3163AF")])
        
        let alert = UIAlertController(title: "", message: "Silahkan cek email untuk dapat mereset password anda", preferredStyle: .alert)
        alert.setValue(attributedString, forKey: "attributedTitle")
        alert.addAction(UIAlertAction(title: "OK", style: .default,  handler: { (_) in
            self.goToLoginPage()
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension ForgotPasswordViewController : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        lblWarning.text = ""
        self.checkForm()
    }
    
}
