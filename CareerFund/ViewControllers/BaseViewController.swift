//
//  ViewController.swift
//  CareerFund
//
//  Created by MAC on 27/10/21.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
    }
    
    func showLoading() {
        let alert = UIAlertController(title: nil, message: "Mohon tunggu...", preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)

    }
    
    func addKeyboardObserver(show:Bool){
        if show {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        }
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func setDefaultToolbar(dismissTabBar: Selector?) {
        let image = UIImage(systemName: "arrow.backward")?.withRenderingMode(.alwaysOriginal)
        image?.withTintColor(UIColor().hexStringToUIColor(hex: "3163AF"))
        let leftBarItem = UIBarButtonItem(image: image, style: .plain, target: self, action: dismissTabBar)
        leftBarItem.tintColor = UIColor().hexStringToUIColor(hex: "3163AF")
        self.navigationItem.leftBarButtonItem = leftBarItem
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = UIColor.systemBlue
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.systemBlue
        ]
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func successUpdate(){
        let attributedString = NSAttributedString(string: "Data berhasil diupdate", attributes: [NSAttributedString.Key.font : UIFont.init(name: "SFProDisplay-Bold", size: 16) as Any,
           NSAttributedString.Key.foregroundColor :  UIColor().hexStringToUIColor(hex: "3163AF")])
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alert.setValue(attributedString, forKey: "attributedTitle")
        alert.addAction(UIAlertAction(title: "Kembali", style: .default, handler: { (_) in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}
