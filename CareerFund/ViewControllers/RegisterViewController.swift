//
//  RegisterViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 29/10/21.
//

import UIKit
import Toaster

class RegisterViewController: BaseViewController {
    
    let stackView = UIStackView()
    let registerLabel  = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
    let fullNameTextField = CustomUnderlineTextField()
    let emailTextField = CustomUnderlineTextField()
    let passwordTextField = CustomUnderlineTextField()
    let confirmPasswordTextField = CustomUnderlineTextField()
    let registerButton = ButtonBlue()
    let loginLabel = UILabel()
    let warningLabel = UILabel()
    var typeUser = ""
    let roleView = UIView()
    let roleLabel = UILabel()
    var roleProfile = UISegmentedControl(items: ["Pendana", "Peminjam"])
    var isValidPassword = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        addKeyboardObserver(show: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    func setupView() {
        setupTextField()
        layoutConstraint()
        style()
        checkForm()
        
        warningLabel.isHidden = true
        self.loginLabel.isUserInteractionEnabled = true
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnLabel(_ :)))
        tapgesture.numberOfTapsRequired = 1
        self.loginLabel.addGestureRecognizer(tapgesture)
        
        registerButton.addTarget(self, action: #selector(actionRegister), for: .touchUpInside)
        roleProfile.addTarget(self, action: #selector(btnTapped), for: .valueChanged)
        
    }
    
    func setupTextField() {
        fullNameTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        passwordTextField.isSecureTextEntry = true
        confirmPasswordTextField.isSecureTextEntry = true
        
        fullNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        confirmPasswordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        fullNameTextField.placeholder = "Nama Lengkap"
        emailTextField.placeholder = "Email"
        passwordTextField.placeholder = "Password"
        confirmPasswordTextField.placeholder = "Konfirmasi Password"
        
    }
    
    @objc func goToLoginPage(){
        let vc = LoginViewController()
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func actionRegister(){
        let email = emailTextField.text!.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        guard email.isValidEmail() else {
            Toast(text: "Email tidak valid", duration: Delay.short).show()
            return
        }
        
        self.showLoading()
        Request().register(email:  emailTextField.text!, name:  fullNameTextField.text!, password:  passwordTextField.text!, role: typeUser) { result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    self.dismiss(animated: false, completion: {
                        self.goToOtpPage()
                    })
                case .failure(let error):
                    self.dismiss(animated: false, completion: nil)
                    Toast(text: error.localizedDescription, duration: Delay.short).show()
                }
            }
        }
    }
    
    func goToOtpPage(){
        let vc = OTPViewController()
        vc.email = emailTextField.text!
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = self.loginLabel.text else { return }
        let secondRange = (text as NSString).range(of: "Masuk")
        if gesture.didTapAttributedTextInLabel(label: self.loginLabel, inRange: secondRange){
            goToLoginPage()
        }
    }
    
    @objc func hideKeyboard(_ gesture: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @objc func btnTapped(_ sender: UIButton){
        if roleProfile.selectedSegmentIndex == 0{
            typeUser = "LENDER"
        }else {
            typeUser = "BORROWER"
        }
        checkForm()
    }
    
    
    func checkForm(){
        if fullNameTextField.text != "" && emailTextField.text != "" && passwordTextField.text != "" && confirmPasswordTextField.text != "" && typeUser != "" && isValidPassword {
            registerButton.isEnabled = true
        } else {
            registerButton.isEnabled = false
        }
        
    }
    
    func style() {
        let textLogin = "Sudah punya akun? Masuk"
        loginLabel.textAlignment = .center
        loginLabel.textColor = .systemGray
        loginLabel.font = UIFont(name: "SF Pro Display", size: 14)
        loginLabel.text = textLogin
        let logString = NSMutableAttributedString(string: textLogin)
        let range1 = (textLogin as NSString).range(of: "Masuk")
        logString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 14), range: range1)
        logString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor().hexStringToUIColor(hex: "3163AF"), range: range1)
        loginLabel.attributedText = logString
        
        roleProfile.layer.cornerRadius = 5
        roleProfile.backgroundColor = UIColor.systemBackground
        roleProfile.selectedSegmentTintColor = UIColor.white
        roleProfile.selectedConfiguration(color: .black)
        
        
        stackView.axis = .vertical
        stackView.spacing = 16
        
        warningLabel.text = "Password tidak sesuai"
        warningLabel.textColor = UIColor.red
        registerButton.setTitle("Daftar", for: .normal)
        registerLabel.text = "Daftar"
        registerLabel.textAlignment = .center
        registerLabel.font = UIFont.boldSystemFont(ofSize: 34)
        
        roleLabel.text = "Daftar Sebagai: "
        roleLabel.textAlignment = .left
        roleLabel.font = UIFont.systemFont(ofSize: 16)
        roleLabel.textColor = .systemGray2
    }
    
    func layoutConstraint() {
        registerLabel.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        loginLabel.translatesAutoresizingMaskIntoConstraints = false
        roleView.translatesAutoresizingMaskIntoConstraints = false
        registerButton.translatesAutoresizingMaskIntoConstraints = false
        
        
        view.addSubview(registerLabel)
        view.addSubview(stackView)
        view.addSubview(registerButton)
        view.addSubview(loginLabel)
        
        stackView.addArrangedSubview(fullNameTextField)
        stackView.addArrangedSubview(emailTextField)
        stackView.addArrangedSubview(passwordTextField)
        stackView.addArrangedSubview(confirmPasswordTextField)
        stackView.addArrangedSubview(warningLabel)
        stackView.addArrangedSubview(roleLabel)
        stackView.addArrangedSubview(roleProfile)
        
        NSLayoutConstraint.activate([
            registerLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            registerLabel.centerYAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            stackView.topAnchor.constraint(equalTo: registerLabel.bottomAnchor, constant: 45),
            
            fullNameTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8),
            fullNameTextField.heightAnchor.constraint(equalToConstant: 37),
            
            emailTextField.widthAnchor.constraint(equalTo: fullNameTextField.widthAnchor),
            emailTextField.heightAnchor.constraint(equalTo: fullNameTextField.heightAnchor),
            
            passwordTextField.widthAnchor.constraint(equalTo: fullNameTextField.widthAnchor),
            passwordTextField.heightAnchor.constraint(equalTo: fullNameTextField.heightAnchor),
            
            confirmPasswordTextField.widthAnchor.constraint(equalTo: fullNameTextField.widthAnchor),
            confirmPasswordTextField.heightAnchor.constraint(equalTo: fullNameTextField.heightAnchor),
            
            warningLabel.widthAnchor.constraint(equalTo: fullNameTextField.widthAnchor),
            warningLabel.heightAnchor.constraint(equalToConstant: 30),
            warningLabel.bottomAnchor.constraint(equalTo: roleLabel.topAnchor, constant: 0),
            
            
            roleLabel.widthAnchor.constraint(equalTo: fullNameTextField.widthAnchor),
            roleLabel.heightAnchor.constraint(equalTo: fullNameTextField.heightAnchor),
            roleLabel.bottomAnchor.constraint(equalTo: roleLabel.bottomAnchor, constant: 0),
            
            roleProfile.widthAnchor.constraint(equalTo: fullNameTextField.widthAnchor),
            roleProfile.heightAnchor.constraint(equalToConstant: 45),
            roleProfile.topAnchor.constraint(equalTo: roleLabel.bottomAnchor, constant: 10),
            
            view.bottomAnchor.constraint(equalToSystemSpacingBelow: registerButton.bottomAnchor, multiplier: 12),
            view.bottomAnchor.constraint(equalToSystemSpacingBelow: loginLabel.bottomAnchor, multiplier: 5),
            
            registerButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.9),
            registerButton.heightAnchor.constraint(equalToConstant: 50),
            registerButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            loginLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.9),
            loginLabel.heightAnchor.constraint(equalToConstant: 50),
            loginLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])
    }
}

extension RegisterViewController : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == confirmPasswordTextField {
            if confirmPasswordTextField.text != passwordTextField.text {
                isValidPassword = false
                warningLabel.isHidden = false
            } else {
                isValidPassword = true
                warningLabel.isHidden = true
            }
        }
        self.checkForm()
    }
    
}
