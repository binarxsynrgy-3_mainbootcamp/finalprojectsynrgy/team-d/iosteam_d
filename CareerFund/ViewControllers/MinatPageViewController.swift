//
//  MinatPageViewController.swift
//  CareerFund
//
//  Created by MAC on 29/10/21.
//

import UIKit

class MinatPageViewController: BaseViewController {
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var minatCollectionView: UICollectionView!
    @IBOutlet weak var btnPilih: ButtonBlue!
    @IBOutlet weak var btnLewati: UIButton!
    var userType : UserType?
    var interestData = [InterestData]()
    var selectedInterest = [InterestData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        btnPilih.isEnabled = false
        minatCollectionView.register(UINib(nibName: "MinatViewCell", bundle: nil), forCellWithReuseIdentifier: "MinatCell")
        minatCollectionView.delegate = self
        minatCollectionView.dataSource = self
        minatCollectionView.showsVerticalScrollIndicator = false
        btnPilih.addTarget(self, action: #selector(actionSubmitMinat), for: .touchUpInside)
        btnLewati.addTarget(self, action: #selector(actionSkip), for: .touchUpInside)
        
        checkMinatUser()
    }
    
    func checkMinatUser(){
        self.showLoading()
        Request().getUserInterest { result in
            switch result {
            case .success(let response):
                self.dismiss(animated: false, completion: {
                    if response.interests.isEmpty {
                        self.loadDataMinat()
                    } else {
                        Session.shared.saveMinatStatus(status: true)
                        self.goToNextPage()
                    }
                })
            case .failure:
                self.dismiss(animated: false, completion: {
                    print("getUserInterest failed")
                    self.goToNextPage()
                })
            }
        }
    }
    
    func loadDataMinat(){
        self.showLoading()
        Request().getListInterest { result in
            self.dismiss(animated: false, completion: nil)
            switch result {
            case .success(let response):
                self.interestData = response.interests
                self.minatCollectionView.reloadData()
            case .failure:
                print("getListInterest failed")
            }
        }
    }
    
    func goToNextPage(){
        let tabBarController = TabBarController()
        tabBarController.user = .Borrower
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.rootViewController = tabBarController
    }
    
    func checkSelectedMinat(){
        if !selectedInterest.isEmpty {
            btnPilih.isEnabled = true
        } else {
            btnPilih.isEnabled = false
        }
    }
    
    @objc func actionSubmitMinat(){
        self.showLoading()
        var interest = [Int]()
        for data in self.selectedInterest {
            interest.append(data.id)
        }
        Request().saveUserInterest(id: interest) { result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    self.dismiss(animated: false, completion: {
                        self.goToNextPage()
                    })
                case .failure:
                    print("gagal submit minat")
                }
            }
        }
    }
    
    @objc func actionSkip(){
        let tabBarController = TabBarController()
        tabBarController.user = .Borrower
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.rootViewController = tabBarController
    }
}

extension MinatPageViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        interestData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MinatCell", for: indexPath) as! MinatViewCell
        cell.clipsToBounds = false
        cell.lblMinat.text =  interestData[indexPath.row].name
        cell.setImgBg(name: interestData[indexPath.row].name)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 60, height: 72)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = minatCollectionView.cellForItem(at: indexPath) as? MinatViewCell{
            let minat = self.interestData[indexPath.row]
            cell.selected()
            if selectedInterest.contains(where: { $0.id == minat.id}) {
                self.selectedInterest.removeAll(where: { $0.id == minat.id})
            } else {
                self.selectedInterest.append(minat)
            }
        }
        
        checkSelectedMinat()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
}
