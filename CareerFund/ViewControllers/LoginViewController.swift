//
//  LoginViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 28/10/21.
//

import UIKit
import Toaster

class LoginViewController: BaseViewController{
    
    let stackView = UIStackView()
    let loginLabel  = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
    let emailTextField = CustomUnderlineTextField()
    let passwordTextField = CustomUnderlineTextField()
    let loginButton = ButtonBlue()
    let registerLabel = UILabel()
    let forgotPasswordButton = UIButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        addKeyboardObserver(show: false)
        self.view.backgroundColor = UIColor.white
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    func setupView() {
        setupTextField()
        layoutConstraint()
        style()
        checkForm()
        
        self.registerLabel.isUserInteractionEnabled = true
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnLabel(_ :)))
        tapgesture.numberOfTapsRequired = 1
        self.registerLabel.addGestureRecognizer(tapgesture)
        
        loginButton.addTarget(self, action: #selector(actionLogin), for: .touchUpInside)
        forgotPasswordButton.addTarget(self, action: #selector(goToForgotPasswordPage), for: .touchUpInside)
        
    }
    
    @objc func actionLogin(){
        let email = emailTextField.text!.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        guard email.isValidEmail() else {
            Toast(text: "Email tidak valid", duration: Delay.short).show()
            return
        }
        self.showLoading()
        Request().login(email: emailTextField.text!, password: passwordTextField.text!) { result in
            switch result {
            case .success(let response):
                self.dismiss(animated: false, completion: {
                    Session.shared.saveUserSession(session: response)
                    self.getProfile()
                })
            case .failure(let error):
                self.dismiss(animated: false, completion: nil)
                if let responCode = Int(error.localizedDescription){
                    if responCode >= 300 && responCode < 400 {
                        self.goToOtpPage()
                    } else {
                        Toast(text: "Email atau password salah", duration: Delay.short).show()
                    }
                }
                
            }
        }
    }
    
    func getProfile() {
        Request().getDataProfile { result in
            switch result {
            case .success(let response):
                Session.shared.saveUserProfile(session: response)
                self.goToNextPage(type: response.roles[1].authority)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func goToOtpPage(){
        let vc = OTPViewController()
        vc.email = emailTextField.text!
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func goToNextPage(type : String){
        if type.contains("ROLE_BORROWER") {
            let vc = MinatPageViewController()
            navigationController?.pushViewController(vc, animated: false)
        } else {
            let tabBarController = TabBarController()
            tabBarController.user = .Lender
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.window?.rootViewController = tabBarController
        }
    }
    
    @objc func goToRegisterPage(){
        let registerPage = RegisterViewController()
        navigationController?.pushViewController(registerPage, animated: false)
    }
    
    @objc func goToForgotPasswordPage(){
        let forgotPasswordPage = ForgotPasswordViewController()
        navigationController?.pushViewController(forgotPasswordPage, animated: false)
    }
    
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = self.registerLabel.text else { return }
        let secondRange = (text as NSString).range(of: "Daftar")
        if gesture.didTapAttributedTextInLabel(label: self.registerLabel, inRange: secondRange){
            goToRegisterPage()
        }
    }
    
    func checkForm(){
        if emailTextField.text != "" && passwordTextField.text != "" {
            loginButton.isEnabled = true
        } else {
            loginButton.isEnabled = false
        }
        
    }
    
    func style() {
        let textRegister = "Belum punya akun? Daftar"
        registerLabel.textAlignment = .center
        registerLabel.textColor = .systemGray
        registerLabel.font = UIFont(name: "SF Pro Display", size: 14)
        registerLabel.text = textRegister
        
        let regString = NSMutableAttributedString(string: textRegister)
        let range1 = (textRegister as NSString).range(of: "Daftar")
        regString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 14), range: range1)
        regString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor().hexStringToUIColor(hex: "3163AF"), range: range1)
        registerLabel.attributedText = regString
        
        loginButton.setTitle("Masuk", for: .normal)
        loginLabel.text = "Masuk"
        loginLabel.textAlignment = .center
        loginLabel.font = UIFont.boldSystemFont(ofSize: 34)
        
        forgotPasswordButton.setTitle("Lupa Password ?", for: .normal)
        forgotPasswordButton.setTitleColor(UIColor().hexStringToUIColor(hex: "3163AF"), for: .normal)
        forgotPasswordButton.contentHorizontalAlignment =  .right
    }
    
    func layoutConstraint() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        loginLabel.translatesAutoresizingMaskIntoConstraints = false
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        registerLabel.translatesAutoresizingMaskIntoConstraints = false
        forgotPasswordButton.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        
        view.addSubview(loginLabel)
        view.addSubview(stackView)
        view.addSubview(loginButton)
        view.addSubview(registerLabel)
        
        stackView.addArrangedSubview(emailTextField)
        stackView.addArrangedSubview(passwordTextField)
        stackView.addArrangedSubview(forgotPasswordButton)
        
        NSLayoutConstraint.activate([
            loginLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loginLabel.centerYAnchor.constraint(equalTo: view.topAnchor, constant: 200),
            
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            
            emailTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8),
            emailTextField.heightAnchor.constraint(equalToConstant: 60),
            
            passwordTextField.widthAnchor.constraint(equalTo: emailTextField.widthAnchor),
            passwordTextField.heightAnchor.constraint(equalTo: emailTextField.heightAnchor),
            
            forgotPasswordButton.heightAnchor.constraint(equalToConstant: 65),
            forgotPasswordButton.widthAnchor.constraint(equalTo: emailTextField.widthAnchor, multiplier: 0.5),
            forgotPasswordButton.rightAnchor.constraint(equalTo: stackView.rightAnchor, constant: 0),
            
            view.bottomAnchor.constraint(equalToSystemSpacingBelow: loginButton.bottomAnchor, multiplier: 11),
            view.bottomAnchor.constraint(equalToSystemSpacingBelow: registerLabel.bottomAnchor, multiplier: 4),
            
            loginButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.9),
            loginButton.heightAnchor.constraint(equalToConstant: 50),
            loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            registerLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.9),
            registerLabel.heightAnchor.constraint(equalToConstant: 50),
            registerLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])
    }
    
    func setupTextField() {
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        
        self.emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.passwordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.emailTextField.placeholder = "Email"
        self.passwordTextField.placeholder = "Password"
        
        let eyeIcon = UIImageView(image: UIImage(systemName: "eye"))
        passwordTextField.trailingView = eyeIcon
        passwordTextField.trailingViewMode = .always
        
        passwordTextField.isSecureTextEntry = true
        passwordTextField.enablePasswordToggle()
    }
}

extension LoginViewController : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.checkForm()
    }
    
}
