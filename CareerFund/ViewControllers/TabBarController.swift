//
//  TabBarController.swift
//  CareerFund
//
//  Created by MAC on 17/11/21.
//

import UIKit

class TabBarController: UITabBarController {
    
    
    var user: UserType? {
        didSet {
            self.setupTabBar()
        }
    }
    // Borrower
    private lazy var dashboardBorrower: UIViewController = {
        let vc = DashboardBorrowerController()
        let activeImage = UIImage(named: "ic-dashboard-gray")!
        let inactiveImage = UIImage(named: "ic-dashboard-gray")!
        vc.tabBarItem = UITabBarItem(title: "Dashboard", image: inactiveImage, selectedImage: activeImage)
        return vc
    }()
    
    private lazy var kelas: UIViewController = {
        let vc = ListClassViewController()
        let activeImage = UIImage(named: "ic-class-gray")!
        let inactiveImage = UIImage(named: "ic-class-gray")!
        vc.tabBarItem = UITabBarItem(title: "List kelas", image: inactiveImage, selectedImage: activeImage)
        return vc
    }()
    
    private lazy var loan: UIViewController = {
        let vc = LoanViewController()
        let activeImage = UIImage(named: "ic-loan-gray")!
        let inactiveImage = UIImage(named: "ic-loan-gray")!
        vc.tabBarItem = UITabBarItem(title: "Pinjaman", image: inactiveImage, selectedImage: activeImage)
        return vc
    }()
    
    private lazy var profil: UIViewController = {
        let vc = ProfileViewController()
        let activeImage = UIImage(named: "ic-profile-gray")!
        let inactiveImage = UIImage(named: "ic-profile-gray")!
        vc.tabBarItem = UITabBarItem(title: "Profil", image: inactiveImage, selectedImage: activeImage)
        return vc
    }()
    
    // Lender
    
    private lazy var dashboardLender: UIViewController = {
        var vc = DashboardLenderViewController()
        let activeImage = UIImage(named: "ic-dashboard-gray")!
        let inactiveImage = UIImage(named: "ic-dashboard-gray")!
        vc.tabBarItem = UITabBarItem(title: "Dashboard", image: inactiveImage, selectedImage: activeImage)
        return vc
    }()
    
    private lazy var danai: UIViewController = {
        let vc = AvailableBorrowerListViewController()
        let activeImage = UIImage(named: "ic-search-gray")!
        let inactiveImage = UIImage(named: "ic-search-gray")!
        vc.tabBarItem = UITabBarItem(title: "Danai", image: inactiveImage, selectedImage: activeImage)
        return vc
    }()
    
    private lazy var notification: UIViewController = {
        let vc = LenderNotificationPageViewController()
        let activeImage = UIImage(named: "ic-notification-gray")!
        let inactiveImage = UIImage(named: "ic-notification-gray")!
        vc.tabBarItem = UITabBarItem(title: "Notifikasi", image: inactiveImage, selectedImage: activeImage)
        return vc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func setupTabBar() {
        self.tabBar.isOpaque = false
        self.tabBar.isTranslucent = false
        self.tabBar.tintColor = UIColor().hexStringToUIColor(hex: "3163AF")
        self.tabBar.barTintColor = UIColor.white
        self.tabBar.backgroundColor = .white
        self.tabBar.unselectedItemTintColor = UIColor.gray
        
        switch user {
        case .Borrower :
            let dashboardTab = UINavigationController(rootViewController: dashboardBorrower)
            let classTab = UINavigationController(rootViewController: kelas)
            let loanTab = UINavigationController(rootViewController: loan)
            let profileTab = UINavigationController(rootViewController: profil)
            self.viewControllers = [dashboardTab, classTab, loanTab, profileTab]
        case .Lender:
            let dashboardTab = UINavigationController(rootViewController: dashboardLender)
            let danaiTab = UINavigationController(rootViewController: danai)
            let notifTab = UINavigationController(rootViewController: notification)
            let profileTab = UINavigationController(rootViewController: profil)
            self.viewControllers = [dashboardTab, danaiTab, notifTab ,profileTab]
        case .none:
            break
        }
    }
    
    
}


enum UserType {
    case Borrower
    case Lender
}
