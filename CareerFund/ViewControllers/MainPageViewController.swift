//
//  MainPageViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 28/10/21.
//

import UIKit

class MainPageViewController: UIPageViewController {
    
    var pages = [UIViewController]()
    let pageControl = UIPageControl()
    let initialPage = 0
    let registerButton = UIButton().firstButton(withText: "Daftar")
    let loginButton = UIButton().secondButton(withText: "Masuk")
    var pageControlBottomAnchor: NSLayoutConstraint?
    var registerButtonBottomAnchor: NSLayoutConstraint?
    var loginButtonBottomAnchor: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        setupOnboarding()
        setupConstraint()
        setIndicator()

        registerButton.addTarget(self, action: #selector(goToRegisterPage), for: .touchUpInside)
        loginButton.addTarget(self, action: #selector(goToLoginPage), for: .touchUpInside)
        view.clipsToBounds = true
        
        if Session.shared.token != "" {
            goToNextPage(type: Session.shared.account_type!)
        }
    }
    
    @objc func goToNextPage(type : [String]){
        if type.contains("ROLE_BORROWER") {
            if Session.shared.minat_status != true {
                let vc = MinatPageViewController()
                navigationController?.pushViewController(vc, animated: false)
            } else {
                let tabBarController = TabBarController()
                tabBarController.user = .Borrower
                let delegate = UIApplication.shared.delegate as! AppDelegate
                delegate.window?.rootViewController = tabBarController
            }
        } else {
            let tabBarController = TabBarController()
            tabBarController.user = .Lender
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.window?.rootViewController = tabBarController
        }
    }
    
    @objc func pageControlTapped(_ sender: UIPageControl) {
        setViewControllers([pages[sender.currentPage]], direction: .forward, animated: true, completion: nil)
    }
    
    @objc func goToRegisterPage(){
        let registerPage = RegisterViewController()
        navigationController?.pushViewController(registerPage, animated: false)
    }
    
    @objc func goToLoginPage(){
        let loginPage = LoginViewController()
        navigationController?.pushViewController(loginPage, animated: false)
    }
}


extension MainPageViewController {
    func setupOnboarding() {
        dataSource = self
        delegate = self
        
        pageControl.addTarget(self, action: #selector(pageControlTapped(_:)), for: .valueChanged)
        
        let page1 = OnboardingViewController(imageName: "onboarding_page1",
                                             titleText: "Bayar Bootcamp dengan Fleksibel",
                                             subtitleText: "Kamu bisa mengatur uang muka, dan periode cicilan sesuai dengan kebutuhan kamu.")
        let page2 = OnboardingViewController(imageName: "onboarding_page2",
                                             titleText: "Pilih Peminjam dengan Mudah",
                                             subtitleText: "Kamu bisa memilih peminjam yang berpotensi menyelesaikan bootcamp dengan mudah.")
        let page3 = OnboardingViewController(imageName: "onboarding_page3",
                                             titleText: "Jaminan Pendanaan Aman",
                                             subtitleText: "Dana yang dibayarkan akan langsung disalurkan ke penyelenggara bootcamp.")
        
        pages.append(page1)
        pages.append(page2)
        pages.append(page3)
        
        setViewControllers([pages[initialPage]], direction: .forward, animated: true, completion: nil)
    }
    
    func setIndicator() {
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.currentPageIndicatorTintColor = UIColor().hexStringToUIColor(hex: "3163AF")
        pageControl.pageIndicatorTintColor = .systemGray2
        pageControl.numberOfPages = pages.count
        pageControl.currentPage = initialPage
    }
    
    func setupConstraint() {
        view.addSubview(pageControl)
        view.addSubview(registerButton)
        view.addSubview(loginButton)
        
        NSLayoutConstraint.activate([
            registerButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.9),
            registerButton.heightAnchor.constraint(equalToConstant: 50),
            registerButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            loginButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.9),
            loginButton.heightAnchor.constraint(equalToConstant: 50),
            loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            pageControl.widthAnchor.constraint(equalTo: view.widthAnchor),
        ])
        
        pageControlBottomAnchor = view.bottomAnchor.constraint(equalToSystemSpacingBelow: pageControl.bottomAnchor, multiplier: 23)
        
        registerButtonBottomAnchor = view.bottomAnchor.constraint(equalToSystemSpacingBelow: registerButton.bottomAnchor, multiplier: 13)
        
        loginButtonBottomAnchor = view.bottomAnchor.constraint(equalToSystemSpacingBelow: loginButton.bottomAnchor, multiplier: 5)
        
        pageControlBottomAnchor?.isActive = true
        registerButtonBottomAnchor?.isActive = true
        loginButtonBottomAnchor?.isActive = true
    }
}
