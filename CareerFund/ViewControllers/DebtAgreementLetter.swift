//
//  DebtAgreementLetter.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 08/12/21.
//

import Foundation
import WebKit

class DebtAgreementLetter: UIViewController, WKNavigationDelegate {
    
    var webView: WKWebView!
    var url = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let url = URL(string: self.url) {
            self.webView.load(URLRequest(url: url))
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = "Surat Perjanjian Hutang Piutang"
    }
    
}
