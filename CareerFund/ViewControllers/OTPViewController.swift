//
//  OTPViewController.swift
//  CareerFund
//
//  Created by MAC on 14/11/21.
//

import UIKit
import Toaster

class OTPViewController: BaseViewController {
    
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var lblResendOtp: UILabel!
    @IBOutlet weak var inputOtpField: CustomUnderlineTextField!
    @IBOutlet weak var btnNext: ButtonBlue!
    var email = ""
    var codeOtp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        checkForm()
    }
    
    
    func setupView(){
        lblEmail.text = email
        lblWarning.isHidden = true
        inputOtpField.delegate = self
        inputOtpField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        btnNext.addTarget(self, action: #selector(actionNext), for: .touchUpInside)
        
        let text = "Tidak meneima kode? Kirim ulang"
        lblResendOtp.textAlignment = .center
        lblResendOtp.textColor = .systemGray
        lblResendOtp.font = UIFont(name: "SF Pro Display", size: 14)
        lblResendOtp.text = text
        let logString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Kirim ulang")
        logString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 14), range: range1)
        logString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor().hexStringToUIColor(hex: "3163AF"), range: range1)
        lblResendOtp.attributedText = logString
        
        self.lblResendOtp.isUserInteractionEnabled = true
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(resendOtp(_ :)))
        tapgesture.numberOfTapsRequired = 1
        self.lblResendOtp.addGestureRecognizer(tapgesture)
    }
    
    func sendOtp(){
        self.showLoading()
        Request().requestOtp(email: self.email) { result in
            DispatchQueue.main.async {
                self.dismiss(animated: false, completion: nil)
                switch result {
                case .success:
                    Toast(text:"Mohon cek email untuk verifikasi OTP", duration: Delay.short).show()
                case .failure:
                    Toast(text:"OTP Error, mohon untuk klik resend", duration: Delay.short).show()
                }
            }
        }
    }
    
    @objc func actionNext(){
        self.showLoading()
        Request().verifyOtp(otp: self.codeOtp) { result in
            DispatchQueue.main.async {
                switch result {
                case .success (let response):
                    self.dismiss(animated: false, completion: {
                        Session.shared.saveUserSession(session: response)
                        self.goToNextPage(type: response.roles!)
                    })
                case .failure:
                    self.dismiss(animated: false, completion: nil)
                    Toast(text:"OTP yang anda masukan salah, mohon cek kembali", duration: Delay.short).show()
                }
            }
        }
    }
    
    @objc func goToNextPage(type : [String]){
        if type.contains("ROLE_BORROWER") {
            let vc = MinatPageViewController()
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let tabBarController = TabBarController()
            tabBarController.user = .Lender
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.window?.rootViewController = tabBarController
        }
    }
    
    @objc func resendOtp(_ gesture: UITapGestureRecognizer) {
        sendOtp()
    }
    
    func checkForm(){
        if inputOtpField.text != "" {
            btnNext.isEnabled = true
        } else {
            btnNext.isEnabled = false
        }
    }
    
}

extension OTPViewController : UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.codeOtp = textField.text ?? ""
        self.checkForm()
    }
}
