//
//  EditProfileViewController.swift
//  CareerFund
//
//  Created by MAC on 27/11/21.
//

import UIKit
import Toaster

class EditProfileViewController: BaseViewController {
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var btnSave: ButtonBlue!
    @IBOutlet weak var lblWarning: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        self.navigationItem.title = "Edit Profile"
        profileView.layer.cornerRadius = 10
        
        nameField.text = Session.shared.name
        phoneField.text = Session.shared.phone
        emailField.text = Session.shared.email
        addressField.text = Session.shared.address

        nameField.delegate = self
        phoneField.delegate = self
        emailField.delegate = self
        addressField.delegate = self
        
        self.nameField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.phoneField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.emailField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.addressField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        btnSave.addTarget(self, action: #selector(updateProfile), for: .touchUpInside)
        self.checkForm()

    }
    
    func checkForm(){
        if nameField.text != "" && phoneField.text != "" && emailField.text != "" && addressField.text != "" {
            btnSave.isEnabled = true
        } else {
            btnSave.isEnabled = false
        }
    }
    
    @objc func updateProfile(){
        self.showLoading()
        Request().editProfile(address: addressField.text ?? "", email: emailField.text ?? "", name: nameField.text ?? "", phone: phoneField.text ?? "") { result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    let session = Session.shared
                    session.address = self.addressField.text ?? ""
                    session.email = self.emailField.text ?? ""
                    session.name = self.nameField.text ?? ""
                    session.phone = self.phoneField.text ?? ""
                    self.dismiss(animated: false, completion: {
                        self.successUpdate()
                    })
                case .failure(let error):
                    self.dismiss(animated: false, completion : nil)
                    Toast(text: error.localizedDescription, duration: Delay.short).show()
                }
            }
        }
    }
    
}

//UITextField
extension EditProfileViewController : UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == emailField {
            let email = emailField.text!.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
            guard email.isValidEmail() else {
                lblWarning.isHidden = false
                btnSave.isEnabled = false
                return
            }
            lblWarning.isHidden = true
            self.checkForm()
        } else {
            self.checkForm()
        }
        
    }
    
}
