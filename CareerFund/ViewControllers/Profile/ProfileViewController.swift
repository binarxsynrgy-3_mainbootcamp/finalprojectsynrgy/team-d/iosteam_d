//
//  ProfileViewController.swift
//  CareerFund
//
//  Created by MAC on 26/11/21.
//

import UIKit
import Toaster
import MessageUI

class ProfileViewController: BaseViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var profileStatusView: UIView!
    @IBOutlet weak var lblProfileStatus: UILabel!
    @IBOutlet weak var imgAlertStatus: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var changeImg: UILabel!
    @IBOutlet weak var btnVerified: UIButton!
    @IBOutlet weak var lblVerified: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var editProfile: UIView!
    @IBOutlet weak var changePwd: UIView!
    @IBOutlet weak var takeKtp: UIView!
    @IBOutlet weak var takeSelfie: UIView!
    @IBOutlet weak var logout: UIView!
    
    let imagePicker = UIImagePickerController()
    var uploadImg : UIImage?
    var isVerified = ""
    var imgUrl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    func setupView(){
        self.navigationItem.title = "Profile"
        imgUrl = Session.shared.photoProfile
        if  imgUrl != "" {
            let urlImg = URL(string:(imgUrl))!
            if let data = try? Data(contentsOf: urlImg) {
                imgProfile.image  = UIImage(data: data)
            }
        }
        
        imagePicker.delegate = self
        profileView?.layer.cornerRadius = (profileView?.frame.size.width ?? 0.0) / 2
        profileView?.clipsToBounds = true
        profileView?.layer.borderWidth = 3.0
        profileView?.layer.borderColor = UIColor.white.cgColor
        
        profileStatusView.layer.cornerRadius = 8
        contentView.layer.cornerRadius = 8
        let tapEdit = UITapGestureRecognizer(target: self, action: #selector(editProfilePage(_:)))
        tapEdit.numberOfTapsRequired = 1
        self.editProfile.addGestureRecognizer(tapEdit)
        
        let tapChangePwd = UITapGestureRecognizer(target: self, action: #selector(changePasswordPage(_:)))
        tapChangePwd.numberOfTapsRequired = 1
        self.changePwd.addGestureRecognizer(tapChangePwd)
        
        let tapChangeImg = UITapGestureRecognizer(target: self, action: #selector(changeImage(_:)))
        tapChangePwd.numberOfTapsRequired = 1
        self.changeImg.addGestureRecognizer(tapChangeImg)
        
        let tapLogout = UITapGestureRecognizer(target: self, action: #selector(logout(_:)))
        tapChangePwd.numberOfTapsRequired = 1
        self.logout.addGestureRecognizer(tapLogout)
        
        let tapTakeKtp = UITapGestureRecognizer(target: self, action: #selector(fotoKtp(_:)))
        tapTakeKtp.numberOfTapsRequired = 1
        self.takeKtp.addGestureRecognizer(tapTakeKtp)
        
        let tapTakeSelfie = UITapGestureRecognizer(target: self, action: #selector(fotoSelfie(_:)))
        tapTakeKtp.numberOfTapsRequired = 1
        self.takeSelfie.addGestureRecognizer(tapTakeSelfie)
        
        getProfile()
    }
    
    func getProfile (){
        Request().getDataProfile { result in
            DispatchQueue.main.async {
                self.dismiss(animated: false, completion: nil)
                switch result {
                case .success(let response):
                    self.lblName.text = response.name
                    self.lblEmail.text = response.email
                    self.isVerified = response.isVerified ?? ""
                    self.checkVerified()
                    if let url = response.photoProfile {
                        let urlImg = URL(string:(url))!
                        if let data = try? Data(contentsOf: urlImg) {
                            self.imgProfile.image  = UIImage(data: data)
                        }
                    }
                    
                    Session.shared.saveUserProfile(session: response)
                case .failure:
                    print("getListInterest failed")
                }
            }
        }
    }
    
    func verifyProfile(){
        self.showLoading()
        Request().getVerifyProfile { result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    self.dismiss(animated: false, completion: {
                        self.verificationProfile()
                    })
                case .failure:
                    print("getListInterest failed")
                }
            }
        }
    }
    
    
    @objc func editProfilePage(_ gesture: UITapGestureRecognizer) {
        let vc = EditProfileViewController()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: false)
    }
    
    func checkProfile() -> String{
        let detailProfile = "Data Diri \n"
        let fotoKtp = "Foto KTP \n"
        let fotoSelfie = "Foto Selfie dengan KTP"
        let message = detailProfile + fotoKtp + fotoSelfie
        return message
    }
    
    @IBAction func btnVerified(_ sender: Any) {
        if isVerified != "VERIFIED" {
            let attributedString = NSAttributedString(string: "Profil Anda Belum Terverifikasi", attributes: [NSAttributedString.Key.font : UIFont.init(name: "SFProDisplay-Bold", size: 16) as Any,
               NSAttributedString.Key.foregroundColor :  UIColor().hexStringToUIColor(hex: "3163AF")])
            
            let alert = UIAlertController(title: "", message: "Silahkan Lengkapi \n \(checkProfile())", preferredStyle: .alert)
            alert.setValue(attributedString, forKey: "attributedTitle")
            alert.addAction(UIAlertAction(title: "Verifikasi Sekarang", style: .default, handler: { (_) in
                self.verifyProfile()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            openEmail()
        }
    }
    
    func openEmail(){
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["aal@careerfund.com"])
            mail.setSubject("Update Data Profile")
            present(mail, animated: true)
        } else {
            Toast(text:"Perangkat ini tidak mendukung pengiriman email", duration: Delay.short).show()
        }
    }
    
    func verificationProfile(){
        let attributedString = NSAttributedString(string: "Verifikasi Diproses", attributes: [NSAttributedString.Key.font : UIFont.init(name: "SFProDisplay-Bold", size: 16) as Any,
           NSAttributedString.Key.foregroundColor :  UIColor().hexStringToUIColor(hex: "3163AF")])
        
        let alert = UIAlertController(title: "", message: "Kami sedang melakukan verifikasi pada profile anda", preferredStyle: .alert)
        alert.setValue(attributedString, forKey: "attributedTitle")
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
            self.isVerified = "VERIFYING"
            self.checkVerified()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func checkVerified(){
        switch isVerified {
        case "VERIFIED":
            lblProfileStatus.text = "Profile Anda Terverifikasi"
            profileStatusView.backgroundColor = UIColor().hexStringToUIColor(hex: "3163AF")
            lblVerified.text = "Ingin Ubah Data Akun?"
            btnVerified.setTitle("Hubungi Kami", for: .normal)
            imgAlertStatus.isHidden = true
            imgAlertStatus.image = UIImage(named: "ic-verified")
        case "VERIFYING":
            lblProfileStatus.text = "Menunggu verifikasi"
            profileStatusView.backgroundColor = UIColor.gray
            imgAlertStatus.isHidden = true
        case "REJECTED":
            lblProfileStatus.text = "Profile Anda Belum Terverifikasi"
            profileStatusView.backgroundColor = UIColor.red
            imgAlertStatus.isHidden = false
            imgAlertStatus.image = UIImage(named: "ic-alert")
        default:
            lblProfileStatus.text = "Profile Anda Belum Terverifikasi"
            profileStatusView.backgroundColor = UIColor.red
            imgAlertStatus.isHidden = false
            imgAlertStatus.image = UIImage(named: "ic-alert")
        }
    
    }
    
    func updateProfileImg(){
        self.showLoading()
        Request().updateProfileImg(foto : uploadImg!){ result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    Session.shared.photoProfile = response.data.url
                    self.dismiss(animated: false, completion: {
                        self.successUpdate()
                    })
                case .failure(let error):
                    self.dismiss(animated: false, completion : nil)
                    Toast(text: error.localizedDescription, duration: Delay.short).show()
                }
            }
        }
    }
    
    
    @objc func changePasswordPage(_ gesture: UITapGestureRecognizer) {
        let vc = ChangePasswordViewController()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func changeImage(_ gesture: UITapGestureRecognizer) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func logout(_ gesture: UITapGestureRecognizer) {
        Session.shared.destroySession()
        let vc = LoginViewController()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func fotoKtp(_ gesture: UITapGestureRecognizer) {
        let vc = FotoKtpViewController()
        vc.type = "Ktp"
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func fotoSelfie(_ gesture: UITapGestureRecognizer) {
        let vc = FotoSelfieViewController()
        vc.type = "Selfie"
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: false)
    }
}


extension ProfileViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImg = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imgProfile.contentMode = .scaleAspectFill
            imgProfile.image = pickedImg
            self.uploadImg = pickedImg
        }
        
        dismiss(animated: true, completion: nil)
        updateProfileImg()
    }
    
}
