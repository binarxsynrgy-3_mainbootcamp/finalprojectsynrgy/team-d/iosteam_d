//
//  ChangePasswordViewController.swift
//  CareerFund
//
//  Created by MAC on 27/11/21.
//

import UIKit
import Toaster

class ChangePasswordViewController: BaseViewController {
    
    @IBOutlet weak var oldPasswordField: UITextField!
    @IBOutlet weak var newPwdField: UITextField!
    @IBOutlet weak var contentView: UIStackView!
    @IBOutlet weak var confirmPwdField: UITextField!
    @IBOutlet weak var warningLabel: UILabel!
    @IBOutlet weak var btnSave: UIButton!

    var isValidPassword = false
    var oldPwd = ""
    var newPwd = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Ganti Password"
        contentView.layer.cornerRadius = 10
        
        oldPasswordField.delegate = self
        newPwdField.delegate = self
        confirmPwdField.delegate = self
        
        oldPasswordField.isSecureTextEntry = true
        newPwdField.isSecureTextEntry = true
        confirmPwdField.isSecureTextEntry = true

        self.oldPasswordField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.newPwdField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.confirmPwdField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        btnSave.isEnabled = false
        btnSave.addTarget(self, action: #selector(updatePassword), for: .touchUpInside)
    }
    
    func checkForm(){
        if oldPasswordField.text != "" && newPwdField.text != "" && confirmPwdField.text != "" && isValidPassword {
            btnSave.isEnabled = true
        } else {
            btnSave.isEnabled = false
        }
        
    }
    
    @objc func updatePassword(){
        self.showLoading()
        Request().updatePassword(oldPwd: oldPasswordField.text ?? "", newPwd: newPwdField.text ?? "" ) { result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    self.dismiss(animated: false, completion: {
                        self.successUpdate()
                    })
                case .failure(let error):
                    self.dismiss(animated: false, completion : nil)
                    Toast(text: error.localizedDescription, duration: Delay.short).show()
                }
            }
        }
    }
    
    
}

extension ChangePasswordViewController : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == confirmPwdField {
            if confirmPwdField.text != newPwdField.text {
                isValidPassword = false
                warningLabel.text = "Konfirmasi Password tidak sesuai"
            } else {
                isValidPassword = true
                warningLabel.text = ""
            }
        }
       
        self.checkForm()
    }
    
}
