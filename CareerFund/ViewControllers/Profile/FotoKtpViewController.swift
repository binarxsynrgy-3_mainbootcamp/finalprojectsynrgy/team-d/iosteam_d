//
//  FotoKtpViewController.swift
//  CareerFund
//
//  Created by MAC on 08/12/21.
//

import UIKit
import Toaster

class FotoKtpViewController: BaseViewController {
    
    @IBOutlet weak var imgKtp: UIImageView!
    @IBOutlet weak var imgSelfieKtp: UIImageView!
    @IBOutlet weak var ktpView: UIView!
    @IBOutlet weak var selfieKtpView: UIView!
    @IBOutlet weak var btnSave: ButtonBlue!
    
    var type = ""
    var uploadImg : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        if type == "Ktp" {
            ktpView.isHidden = false
            selfieKtpView.isHidden = true
            let ktpUrl = Session.shared.ktp
            if  ktpUrl != "" {
                let urlImg = URL(string:(ktpUrl))!
                if let data = try? Data(contentsOf: urlImg) {
                    imgKtp.image = UIImage(data: data)
                }
            }
            
        } else {
            let ktpSelfieUrl = Session.shared.ktpSelfie
            if ktpSelfieUrl != "" {
                let urlImg = URL(string:(ktpSelfieUrl))!
                if let data = try? Data(contentsOf: urlImg) {
                    imgSelfieKtp.image = UIImage(data: data)
                }
            }
            
            ktpView.isHidden = true
            selfieKtpView.isHidden = false
        }
        self.navigationItem.title = "FotoKtp"
        btnSave.isEnabled = false
        ktpView.layer.cornerRadius = 8
        selfieKtpView.layer.cornerRadius = 8
        btnSave.addTarget(self, action: #selector(updateProfile), for: .touchUpInside)
    }
    
    @IBAction func takeKtp(_ sender: Any) {
        openCamera()
    }
    
    @IBAction func takeSelfieKtp(_ sender: Any) {
        openCamera()
    }
    
    func openCamera(){
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
    }
    
    @objc func updateProfile(){
        self.showLoading()
        if type == "Ktp" {
            Request().updateKtp(foto : uploadImg!){ result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let response):
                        Session.shared.ktp = response.data.url
                        self.dismiss(animated: false, completion: {
                            self.successUpdate()
                        })
                    case .failure(let error):
                        self.dismiss(animated: false, completion : nil)
                        Toast(text: error.localizedDescription, duration: Delay.short).show()
                    }
                }
            }
        } else {
            Request().updateKtpSelfie(foto : uploadImg!){ result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let response):
                        Session.shared.ktpSelfie = response.data.url
                        self.dismiss(animated: false, completion: {
                            self.successUpdate()
                        })
                    case .failure(let error):
                        self.dismiss(animated: false, completion : nil)
                        Toast(text: error.localizedDescription, duration: Delay.short).show()
                    }
                }
            }
        }
    }
    
}

extension FotoKtpViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true)
        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        
        self.uploadImg = image
        btnSave.isEnabled = true
        if type == "Ktp" {
            imgKtp.contentMode = .scaleToFill
            imgKtp.image = image
        } else {
            imgSelfieKtp.contentMode = .scaleToFill
            imgSelfieKtp.image = image
        }
    }
}


