//
//  WithdrawLenderAssetsViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 08/12/21.
//

import UIKit

class WithdrawLenderAssetsViewController: BaseViewController {
    
    
    @IBOutlet weak public var lenderLoansCollectionView: UICollectionView!
    @IBOutlet weak var totalAssetlabel: UILabel!
    @IBOutlet weak var totalWithdrawnLabel: UILabel!
    @IBOutlet weak var withdrawalButton: ButtonBlue!
    
    var lenderLoansData = [DashboardLenderLoansResponse]()
    var selectedWithdrawnAmount = 0
    var fundingId = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }
    
    func setupView(){
        let backButton = UIBarButtonItem()
        backButton.title = "Kembali"
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationItem.title = "Tarik Aset"
        navigationController?.navigationBar.backgroundColor = .white
        
        lenderLoansCollectionView.register(UINib(nibName: "WithdrawalViewCell", bundle: nil), forCellWithReuseIdentifier: "WithdrawalCell")
        
        lenderLoansCollectionView.delegate = self
        lenderLoansCollectionView.dataSource = self
        lenderLoansCollectionView.showsVerticalScrollIndicator = false
        
        totalWithdrawnLabel.text = "\(0.convertToCurrency())"
        
        withdrawalButton.isEnabled = false
        
        loadDataLenderLoans()
    }
    
    func loadDataLenderLoans (){
        self.showLoading()
        Request().getLenderLoans { result in
            self.dismiss(animated: false, completion: nil)
            switch result {
            case .success(let response):
                self.lenderLoansData = response.data
                let sumOfAsset = self.lenderLoansData.reduce(0) { $0 + ($1.financialTransaction.nominal)}
                self.totalAssetlabel.text = sumOfAsset.convertToCurrency()
                self.lenderLoansCollectionView.reloadData()
            case .failure:
                print("getLenderLoans failed")
            }
        }
        
    }
    
    @IBAction func withdrawalButton(_ sender: Any) {
        let withdrawalBankPage = WithdrawalBankViewController()
        withdrawalBankPage.withdrawnAmount = selectedWithdrawnAmount
        withdrawalBankPage.fundingId = fundingId
        navigationController?.pushViewController(withdrawalBankPage, animated: false)
        
    }
    
}

extension WithdrawLenderAssetsViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lenderLoansData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WithdrawalCell", for: indexPath) as! WithdrawalViewCell
        cell.clipsToBounds = false
        cell.setupView(data: lenderLoansData[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 40, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = lenderLoansCollectionView.cellForItem(at: indexPath) as? WithdrawalViewCell

        cell?.clipsToBounds = false
        cell?.setupView(data: lenderLoansData[indexPath.row])
        cell?.checkmark.isHidden = false
        cell?.withdrawalView.layer.borderWidth = 3
        cell?.withdrawalView.layer.borderColor = UIColor().hexStringToUIColor(hex: "3163AF").cgColor
        
        selectedWithdrawnAmount = lenderLoansData[indexPath.row].financialTransaction.nominal
        fundingId = lenderLoansData[indexPath.row].id
        totalWithdrawnLabel.text = selectedWithdrawnAmount.convertToCurrency()
        withdrawalButton.isEnabled = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = lenderLoansCollectionView.cellForItem(at: indexPath) as? WithdrawalViewCell
        cell?.checkmark.isHidden = true
        cell?.withdrawalView.layer.borderWidth = 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
}
