//
//  WithdrawalBankViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 13/12/21.
//

import UIKit

class WithdrawalBankViewController: UIViewController, PaymentMethodWidrawal{

    @IBOutlet weak var paymentButton: ButtonBlue!
    @IBOutlet weak var checkedButton: UIButton!
    @IBOutlet weak var bankAccount: UILabel!
    @IBOutlet weak var bankAccountNumberTextField: UITextField!
    
    public var withdrawnAmount = 0
    public var lenderBankAccountNumber = 0
    public var fundingId = 0
    var unchecked = true
    var paymentId = 0
    var selectedBankName: String = ""
    var noVirtualAccount = 122000000000001
    var paymentMethod : PaymentAccount?

    override func viewDidLoad() {
        super.viewDidLoad()

        
        let backButton = UIBarButtonItem()
        backButton.title = "Kembali"
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationItem.title = "Rekening Bank"
        navigationController?.navigationBar.backgroundColor = .white
       
        bankAccountNumberTextField.delegate = self
        bankAccountNumberTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        paymentButton?.isEnabled = false
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)

        if !text.isEmpty && bankAccount.text != ""{
                paymentButton?.isEnabled = true
            } else {
                paymentButton?.isEnabled = false
            }
            return true
        }
    
    @IBAction func paymentMethod(_ sender: Any) {
        let vc = PaymentMethodLenderViewController()
        vc.delegatePayWidrawal = self
        navigationController?.pushViewController(vc, animated: false)
    }
    
    func data(payment: PaymentAccount) {
        self.paymentMethod = payment
        self.paymentId = payment.id
        self.selectedBankName = payment.name
        self.noVirtualAccount = payment.number
        self.bankAccount.text = selectedBankName
    }
    
    @IBAction func checkedButton(_ sender: Any) {
        if unchecked {
            checkedButton.setImage(UIImage(named:"check-box-empty"), for: .normal)
            unchecked = false
        }

        else {
            checkedButton.setImage(UIImage(named:"check-box-fill"), for: .normal)
            unchecked = true
        }
    }
    
    
    @IBAction func withdrawButton(_ sender: Any) {
        let withdrawalConfirmationPage = WIthdrawalConfirmationViewController()
        withdrawalConfirmationPage.selectedPaymentMethod = paymentMethod
        withdrawalConfirmationPage.withdrawnAmount = withdrawnAmount
        withdrawalConfirmationPage.lenderBankAccountNumber = lenderBankAccountNumber
        withdrawalConfirmationPage.lenderBankAccountId = paymentMethod?.id ?? 0
        withdrawalConfirmationPage.fundingId = fundingId
        navigationController?.pushViewController(withdrawalConfirmationPage, animated: false)
    }
    
}

//UITextField
extension WithdrawalBankViewController : UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        let amount = Int.parse(from: textField.text?.getNumberOnly() ?? "0") ?? 0
        bankAccountNumberTextField.text = "\(amount)"
        lenderBankAccountNumber = amount
    }
    
}


protocol PaymentMethodWidrawal{
    func data (payment : PaymentAccount)
}
