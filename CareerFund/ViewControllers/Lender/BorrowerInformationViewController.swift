//
//  BorrowerInformationViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 23/11/21.
//

import UIKit

class BorrowerInformationViewController: BaseViewController {
    
    @IBOutlet weak var fundingButton: ButtonBlue!
    @IBOutlet weak var borrowerScrollView: UIScrollView!
    @IBOutlet weak var borrowerInformationScrollView: NSLayoutConstraint!
    @IBOutlet weak var borrowerNameLabel: UILabel!
    @IBOutlet weak var bootcampLabel: UILabel!
    @IBOutlet weak var institutionLabel: UILabel!
    @IBOutlet weak var assesmentLabel: UILabel!
    @IBOutlet weak var loanAmountLabel: UILabel!
    @IBOutlet weak var tenureLabel: UILabel!
    @IBOutlet weak var interestLabel: UILabel!
    @IBOutlet weak var returnAmountLabel: UILabel!
    @IBOutlet weak var inputAmountField: UITextField!
    @IBOutlet weak var lblFundingAlert: UILabel!
    @IBOutlet weak var customFundingView: UIView!
    @IBOutlet weak var loanNeededLabel: UILabel!
    
    public var senderPageToHideFunding: Bool = false
    public var borrowerName = ""
    public var bootcampName = ""
    public var bootcampStartPeriod = ""
    public var bootcampEndPeriod = ""
    public var bootcampPrice = 0
    public var bootcampDuration = 0
    public var institutionName = ""
    public var institutionLogo = ""
    public var assesment = ""
    public var loanAmount = 0
    public var loanTenure = 0
    public var loanInterest = 0.00
    public var returnAmount = 0.00
    public var borrowerAddress = ""
    public var borrowerPhoneNumber = ""
    public var borrowerDownPayment = 0
    var fundingAmount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }
    
    func setupView(){
        let backButton = UIBarButtonItem()
        backButton.title = "Kembali"
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationItem.title = "Informasi Borrower"
        navigationController?.navigationBar.backgroundColor = .white
        addKeyboardObserver(show: true)
        
        if senderPageToHideFunding == true {
            fundingButton.isHidden = true
            customFundingView.isHidden = true
            loanNeededLabel.text = "Besar Pinjaman yang Diberikan"
            NSLayoutConstraint.activate([
                borrowerScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
            ])
        } else {
            fundingButton.isHidden = false
        }
        
        
        let nsString = String(loanInterest) as NSString
        if nsString.length >= 4
        {
            interestLabel.text = nsString.substring(with: NSRange(location: 0, length: nsString.length > 4 ? 4 : nsString.length)) + "%"
        }
        
        borrowerNameLabel.text = borrowerName
        bootcampLabel.text = bootcampName
        institutionLabel.text = institutionName
        assesmentLabel.text = assesment
        loanAmountLabel.text = loanAmount.convertToCurrency()
        tenureLabel.text = "\(loanTenure) bulan"
        returnAmountLabel.text = returnAmount.convertToCurrency()
        
        inputAmountField.delegate = self
        inputAmountField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        checkForm()
    }
    
    @IBAction func fundingbutton(_ sender: Any) {
        let vc = LoanConfirmationViewController()
        vc.borrowerName = borrowerName
        vc.fundingAmount = fundingAmount
        vc.loanTenure = loanTenure
        vc.loanInterest = loanInterest
        vc.institutionName = institutionName
        vc.institutionLogo = institutionLogo
        vc.bootcampName = bootcampName
        vc.borrowerAddress = borrowerAddress
        vc.borrowerPhoneNumber = borrowerPhoneNumber
        vc.bootcampStartPeriod = bootcampStartPeriod
        vc.bootcampEndPeriod = bootcampEndPeriod
        vc.bootcampDuration = bootcampDuration
        vc.bootcampPrice = bootcampPrice
        vc.borrowerDownPayment = borrowerDownPayment
        print(borrowerAddress)
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func bootcampHistoryButton(_ sender: Any) {
        let bootcampHistoryPage = BootcampHistoryViewController()
        navigationController?.pushViewController(bootcampHistoryPage, animated: false)
    }
    
    func checkForm(){
        fundingButton.isEnabled = false
        guard self.checkValidAmount() else {return}
        fundingButton.isEnabled = true
    }
    
    func checkValidAmount() -> Bool {
        let minFunding = 100000
        let maxFunding = loanAmount
        guard self.fundingAmount > 0 else {return false}
        if self.fundingAmount < minFunding {
            self.lblFundingAlert.isHidden = false
            self.lblFundingAlert.text = "Minimal mendanai \(minFunding.convertToCurrency())"
        } else if self.fundingAmount > maxFunding {
            self.lblFundingAlert.isHidden = false
            self.lblFundingAlert.text = "Maksimal mendanai \(maxFunding.convertToCurrency())"
        } else {
            self.lblFundingAlert.isHidden = true
        }
        return self.fundingAmount >= minFunding && self.fundingAmount <= maxFunding
    }
    
}

//UITextField
extension BorrowerInformationViewController : UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        let amount = Int.parse(from: textField.text?.getNumberOnly() ?? "0") ?? 0
        inputAmountField.text = amount.convertToCurrency()
        fundingAmount = amount
        checkForm()
    }
    
}
