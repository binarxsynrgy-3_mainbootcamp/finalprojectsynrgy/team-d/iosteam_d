//
//  FilterBootcampViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 25/11/21.
//

import UIKit

class FilterFundingViewController: BaseViewController {
    
    
    @IBOutlet weak var bootcampHeight: NSLayoutConstraint!
    @IBOutlet weak var fieldStudyHeight: NSLayoutConstraint!
    @IBOutlet weak var scoreTextField: UITextField!
    @IBOutlet weak var bootcampCollectionView: UICollectionView!
    @IBOutlet weak var fieldStudyCollectionView: UICollectionView!
    
    var bootcampData = [Institutions]()
    var fieldStudyData = [InterestData]()
    var cellHeight : CGFloat = 80
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        let backButton = UIBarButtonItem()
        let applyButton = UIBarButtonItem(title: "Terapkan", style: .plain, target: self, action: #selector(applyingFilter))
        
        navigationItem.title = "Filter"
        backButton.title = "Kembali"
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.topItem?.rightBarButtonItem = applyButton
        navigationController?.navigationBar.backgroundColor = .white
        
        bootcampCollectionView.register(UINib(nibName: "FilterViewCell", bundle: nil), forCellWithReuseIdentifier: "BootcampCell")
        bootcampCollectionView.delegate = self
        bootcampCollectionView.dataSource = self
        bootcampCollectionView.showsVerticalScrollIndicator = false
        
        fieldStudyCollectionView.register(UINib(nibName: "FilterViewCell", bundle: nil), forCellWithReuseIdentifier: "FieldStudyCell")
        fieldStudyCollectionView.delegate = self
        fieldStudyCollectionView.dataSource = self
        fieldStudyCollectionView.showsVerticalScrollIndicator = false
        
        bootcampCollectionView.allowsMultipleSelection = true
        fieldStudyCollectionView.allowsMultipleSelection = true
        
        loadDataInterest()
        loadDataInstitution()
    }
    
    func updateHeight(){
        self.bootcampHeight.constant = CGFloat(bootcampData.count) * self.cellHeight
        self.fieldStudyHeight.constant = CGFloat(fieldStudyData.count) * self.cellHeight
    }
    
    func loadDataInstitution(){
        self.showLoading()
        Request().getListInstition { result in
            switch result {
            case .success(let response):
                self.bootcampData = response.data
                self.bootcampCollectionView.reloadData()
                self.loadDataInterest()
            case .failure(let error):
                print("getListInstition failed->\(error)")
            }
        }
    }
    
    func loadDataInterest(){
        self.showLoading()
        Request().getListInterest { result in
            self.dismiss(animated: false, completion: nil)
            switch result {
            case .success(let response):
                self.fieldStudyData = response.interests
                self.fieldStudyCollectionView.reloadData()
                self.updateHeight()
            case .failure(let error):
                print("getListInterest failed->\(error)")
            }
        }
    }
    
    @objc func applyingFilter(){
        let appliedFilterPage = AvailableBorrowerListViewController()
        appliedFilterPage.hidesBottomBarWhenPushed = false
        navigationController?.pushViewController(appliedFilterPage, animated: false)
    }
}

extension FilterFundingViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var countCell: Int = 0
        
        if collectionView == bootcampCollectionView {
            countCell = bootcampData.count
            
        } else {
            countCell = fieldStudyData.count
        }
        
        return countCell
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == bootcampCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BootcampCell", for: indexPath) as! FilterViewCell
            let urlImg = URL(string:bootcampData[indexPath.row].logo)!
            cell.clipsToBounds = false
            cell.lblName.text = bootcampData[indexPath.row].name
            if let data = try? Data(contentsOf: urlImg) {
                cell.imgLogo.image = UIImage(data: data)
            }
            return cell
            
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FieldStudyCell", for: indexPath) as! FilterViewCell
            cell.clipsToBounds = false
            cell.lblName.text = fieldStudyData[indexPath.row].name
            cell.imgLogo.isHidden = true
            NSLayoutConstraint.activate([
                cell.lblName.leftAnchor.constraint(equalTo: cell.mainView.leftAnchor, constant: 20)
            ])
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 65)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let bootcampCell = bootcampCollectionView.cellForItem(at: indexPath) as? FilterViewCell
        let fieldStudyCell = fieldStudyCollectionView.cellForItem(at: indexPath) as? FilterViewCell
        
        if collectionView == bootcampCollectionView {
            bootcampCell?.imgChecked.isHidden = false
        } else{
            fieldStudyCell?.imgChecked.isHidden = false
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        let bootcampCell = bootcampCollectionView.cellForItem(at: indexPath) as? FilterViewCell
        let fieldStudyCell = fieldStudyCollectionView.cellForItem(at: indexPath) as? FilterViewCell
        
        if collectionView == bootcampCollectionView {
            bootcampCell?.imgChecked.isHidden = true
        } else{
            fieldStudyCell?.imgChecked.isHidden = true
        }
    }
}
