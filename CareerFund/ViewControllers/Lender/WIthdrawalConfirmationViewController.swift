//
//  WIthdrawalConfirmationViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 14/12/21.
//

import UIKit
import Toaster

class WIthdrawalConfirmationViewController: BaseViewController {

    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblNumberVA: UILabel!
    @IBOutlet weak var withdrawnAmountLabel: UILabel!
    
    public var withdrawnAmount = 0
    public var lenderBankAccountNumber = 0
    public var lenderBankAccountId = 0
    public var fundingId = 0
    var selectedPaymentMethod : PaymentAccount?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backButton = UIBarButtonItem()
        backButton.title = "Kembali"
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationItem.title = "Konfirmasi"
        navigationController?.navigationBar.backgroundColor = .white
        
        withdrawnAmountLabel.text = withdrawnAmount.convertToCurrency()
        lblBankName.text = selectedPaymentMethod?.name
        lblNumberVA.text = "\(selectedPaymentMethod?.number ?? 0)"

    }

    @IBAction func withdrawalButton(_ sender: Any) {
        let fundingId = fundingId
        self.showLoading()
        Request().submitWithdraw(fundingId: fundingId, bankAccountNumber: lenderBankAccountNumber, bankId: lenderBankAccountId){ result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    self.dismiss(animated: false, completion: {
                        self.successAlert(type: "Success")
                    })
                case .failure:
                    self.dismiss(animated: false, completion: {
                        self.successAlert(type: "Gagal")
                    })
                }
            }
        }
    }
    
    func successAlert(type : String){
        if type == "Success" {
            let alert = UIAlertController(title: "Penarikan sedang diproses", message: "Penarikanmu segera kami proses dalam waktu 2 x 24 jam", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Kembali", style: .default, handler: { _ in
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
            }))

            self.present(alert, animated: true)
        } else {
            let alert = UIAlertController(title: "Penarikan gagal", message: "Pinjaman belum terlunasi oleh Pengguna, silahkan kembali dan pilih pinjaman yang sudah lunas", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Kembali", style: .default, handler: { _ in
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
            }))

            self.present(alert, animated: true)
        }
    }
    
}
