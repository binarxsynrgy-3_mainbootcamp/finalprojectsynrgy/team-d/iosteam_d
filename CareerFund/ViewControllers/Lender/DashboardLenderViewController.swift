//
//  DashboardLenderViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 16/11/21.
//

import UIKit
import Toaster

class DashboardLenderViewController: BaseViewController{
    
    
    @IBOutlet weak var totalAsset: UILabel!
    @IBOutlet weak var lenderLoansCollectionView: UICollectionView!
    @IBOutlet weak var totalAssetTitle: UILabel!
    @IBOutlet weak var titleNotificationEmpty: UILabel!
    @IBOutlet weak var logoNotificationEmpty: UIImageView!
    @IBOutlet weak var subtitleNotificationEmpty: UILabel!
    @IBOutlet weak var fundingBottonView: ButtonBlue!
    
    var lenderLoansData = [DashboardLenderLoansResponse]()
    var financialTransaction = [FundingData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    func setupView(){
        
        navigationItem.title = "Dashboard"
        navigationController?.navigationBar.backgroundColor = .white
        
        let withdrawAssetsButton = UIBarButtonItem(title: "Tarik Aset", style: .plain, target: self, action: #selector(goToWithdrawAssets))
        navigationController?.navigationBar.topItem?.rightBarButtonItem = withdrawAssetsButton
        
        lenderLoansCollectionView.register(UINib(nibName: "DashboardLenderViewCell", bundle: nil), forCellWithReuseIdentifier: "DashboardLenderCell")
        
        lenderLoansCollectionView.delegate = self
        lenderLoansCollectionView.dataSource = self
        lenderLoansCollectionView.showsVerticalScrollIndicator = false

        titleNotificationEmpty.isHidden = true
        logoNotificationEmpty.isHidden = true
        subtitleNotificationEmpty.isHidden = true
        fundingBottonView.isHidden = true
        
        loadDataLenderLoans()
        
    }
    
    func goToNextPage(){
        let tabBarController = TabBarController()
        tabBarController.user = .Lender
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.rootViewController = tabBarController
    }
    
    func loadDataLenderLoans (){
        self.showLoading()
        Request().getLenderLoans { result in
            self.dismiss(animated: false, completion: nil)
            switch result {
            case .success(let response):
                self.lenderLoansData = response.data
                let sumOfAsset = self.lenderLoansData.reduce(0) { $0 + ($1.financialTransaction.nominal)}
                self.totalAsset.text = sumOfAsset.convertToCurrency()
                self.lenderLoansCollectionView.reloadData()
            case .failure(let error):
                Toast(text: error.localizedDescription, duration: Delay.short).show()
            }
        }
        
    }
    
    @objc func goToWithdrawAssets(){
        let vc = WithdrawLenderAssetsViewController()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func fundingButton(_ sender: Any) {
        let availableBorrowerPage = AvailableBorrowerListViewController()
        navigationController?.pushViewController(availableBorrowerPage, animated: false)
    }
    
}


extension DashboardLenderViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (lenderLoansData.count == 0) {
            totalAssetTitle.isHidden = true
            totalAsset.isHidden = true
            titleNotificationEmpty.isHidden = false
            logoNotificationEmpty.isHidden = false
            subtitleNotificationEmpty.isHidden = false
            fundingBottonView.isHidden = false
        } else {
            totalAssetTitle.isHidden = false
            totalAsset.isHidden = false
            titleNotificationEmpty.isHidden = true
            logoNotificationEmpty.isHidden = true
            subtitleNotificationEmpty.isHidden = true
            fundingBottonView.isHidden = true
            let withdrawAssetsButton = UIBarButtonItem(title: "Tarik Aset", style: .plain, target: self, action: #selector(goToWithdrawAssets))
            navigationController?.navigationBar.topItem?.rightBarButtonItem = withdrawAssetsButton
        }
        print(lenderLoansData.count)
        return lenderLoansData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashboardLenderCell", for: indexPath) as! DashboardLenderViewCell
        cell.clipsToBounds = false
        cell.setupView(data: lenderLoansData[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 40, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let assetDetailsPage = AssetDetailsViewController()
        assetDetailsPage.hidesBottomBarWhenPushed = true
        assetDetailsPage.loanAmount = lenderLoansData[indexPath.row].financialTransaction.nominal!
        assetDetailsPage.tenureInstallment = lenderLoansData[indexPath.row].loan.tenorMonth!
        assetDetailsPage.totalTenure = lenderLoansData[indexPath.row].loan.tenorMonth!
        assetDetailsPage.loanInterest = Double(lenderLoansData[indexPath.row].loan.interestPercent!)
        assetDetailsPage.loanTenurePaid = lenderLoansData[indexPath.row].loan.monthPaid
        assetDetailsPage.loanPaidAmount = lenderLoansData[indexPath.row].loan.progress
        assetDetailsPage.borrowerName = lenderLoansData[indexPath.row].loan.borrower.name ?? ""
        assetDetailsPage.institutionName = lenderLoansData[indexPath.row].loan.aclass.bootcamp.institutions[0].name
        assetDetailsPage.bootcampName = lenderLoansData[indexPath.row].loan.aclass.bootcamp.name
        assetDetailsPage.assesment = lenderLoansData[indexPath.row].loan.borrower.assessmentScore ?? 0
        navigationController?.pushViewController(assetDetailsPage, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
}

