//
//  BorrowerListViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 22/11/21.
//

import UIKit
import Toaster

class AvailableBorrowerListViewController: BaseViewController {
    
    @IBOutlet weak var availableBorrowerCollectionView: UICollectionView!
    @IBOutlet weak var emptyView: UIView!
    
    var availableBorrowerResponse = [AvailableBorrowerResponse]()
    var availableBorrowerData = [LoanData]()
    var borrowerProfileData = [BorrowerData]()
    var institutionData = [InstitutionsData]()
    var bootcampData = [BotcampData]()
    public var backToDashboard: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        navigationItem.title = "Danai"
        navigationController?.navigationBar.backgroundColor = .white
        navigationItem.backButtonTitle = "Kembali"
        navigationController?.navigationBar.topItem?.hidesBackButton = true
        
        if backToDashboard == true {
            navigationController?.navigationBar.topItem?.hidesBackButton = false
        }
        
        availableBorrowerCollectionView.register(UINib(nibName: "AvailableBorrowerListViewCell", bundle: nil), forCellWithReuseIdentifier: "AvailableBorrowerListCell")
        
        availableBorrowerCollectionView.delegate = self
        availableBorrowerCollectionView.dataSource = self
        availableBorrowerCollectionView.showsVerticalScrollIndicator = false

        loadDataAvailableBorrower()
    }
    
    func loadDataAvailableBorrower (){
        self.showLoading()
        Request().getAvailableBorrower { result in
            self.dismiss(animated: false, completion: nil)
            switch result {
            case .success(let response):
                if response.data.count > 0 {
                    self.availableBorrowerData = response.data
                    self.emptyView.isHidden = true
                    self.availableBorrowerCollectionView.reloadData()
                } else {
                    self.emptyView.isHidden = false
                }
            case .failure(let error):
                Toast(text: error.localizedDescription, duration: Delay.long).show()
            }
        }
        
    }
    
    @objc func goToFilterPage(){
        let filterPage = FilterFundingViewController()
        navigationController?.pushViewController(filterPage, animated: false)
    }
    
}

extension AvailableBorrowerListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return availableBorrowerData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvailableBorrowerListCell", for: indexPath) as! AvailableBorrowerListViewCell
        cell.clipsToBounds = false
        cell.setupView(data: availableBorrowerData[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let borrowerInformationPage = BorrowerInformationViewController()
        let interesetPercentage = availableBorrowerData[indexPath.row].interestPercent / 100
        let returnAmount = Float(availableBorrowerData[indexPath.row].targetFund) * interesetPercentage
        
        borrowerInformationPage.senderPageToHideFunding = false
        borrowerInformationPage.hidesBottomBarWhenPushed = true
        
        borrowerInformationPage.borrowerName = availableBorrowerData[indexPath.row].borrower.name
        borrowerInformationPage.borrowerAddress = availableBorrowerData[indexPath.row].borrower.address ?? ""
        borrowerInformationPage.borrowerPhoneNumber = availableBorrowerData[indexPath.row].borrower.phoneNumber ?? ""
        borrowerInformationPage.institutionName = availableBorrowerData[indexPath.row].aclass.bootcamp.institutions[0].name
        borrowerInformationPage.institutionLogo = availableBorrowerData[indexPath.row].aclass.bootcamp.institutions[0].logoPath ?? ""
        borrowerInformationPage.bootcampName = availableBorrowerData[indexPath.row].aclass.bootcamp.name
        borrowerInformationPage.bootcampStartPeriod = availableBorrowerData[indexPath.row].aclass.startDate
        borrowerInformationPage.bootcampEndPeriod = availableBorrowerData[indexPath.row].aclass.endDate
        borrowerInformationPage.bootcampPrice = availableBorrowerData[indexPath.row].aclass.price
        borrowerInformationPage.bootcampDuration = availableBorrowerData[indexPath.row].aclass.durationMonth ?? 0
        borrowerInformationPage.assesment = "\(availableBorrowerData[indexPath.row].borrower.assessmentScore ?? 0)"
        borrowerInformationPage.loanAmount = availableBorrowerData[indexPath.row].targetFund ?? 0
        borrowerInformationPage.loanTenure = availableBorrowerData[indexPath.row].tenorMonth ?? 0
        borrowerInformationPage.loanInterest = Double(availableBorrowerData[indexPath.row].interestPercent ?? 0)
        borrowerInformationPage.returnAmount = Double(returnAmount)
        borrowerInformationPage.borrowerDownPayment = availableBorrowerData[indexPath.row].downPayment
        
        navigationController?.pushViewController(borrowerInformationPage, animated: false)
        
        Session.shared.saveLoanId(id: availableBorrowerData[indexPath.row].id)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 40, height: 160)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
}
