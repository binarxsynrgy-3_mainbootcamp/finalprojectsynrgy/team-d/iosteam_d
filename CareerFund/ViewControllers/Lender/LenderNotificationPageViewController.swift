//
//  LenderNotificationPageViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 25/11/21.
//

import UIKit

class LenderNotificationPageViewController: UIViewController {
    
    @IBOutlet weak var notificationCollectionView: UICollectionView!
    @IBOutlet weak var notificationEmptyImage: UIImageView!
    @IBOutlet weak var notificationEmptyTitle: UILabel!
    @IBOutlet weak var notificationEmptySubtitle: UILabel!
    @IBOutlet weak var fundingButton: ButtonBlue!
    
    var notificationData = [NotificationResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        navigationItem.title = "Notifikasi"
        navigationController?.navigationBar.backgroundColor = .white
        navigationItem.hidesBackButton = true
        
        notificationEmptyImage.isHidden = true
        notificationEmptyTitle.isHidden = true
        notificationEmptySubtitle.isHidden = true
        fundingButton.isHidden = true
        
        notificationCollectionView.register(UINib(nibName: "NotificationViewCell", bundle: nil), forCellWithReuseIdentifier: "NotificationCell")
        notificationCollectionView.delegate = self
        notificationCollectionView.dataSource = self
        notificationCollectionView.showsVerticalScrollIndicator = false
        
        notificationData.append(NotificationResponse.init(notifTitle: "Dana Pinjamanmu telah diterima", notifContent: "Dana yang kamu pinjamkan telah diterima oleh bootcamp dan akan segera diproses untuk digunakan oleh peminjam digunakann.", linkContent: "Lihat sekarang >"))
        notificationData.append(NotificationResponse.init(notifTitle: "Dana Pinjamanmu sedang diproses", notifContent: "Dana yang kamu pinjamkan telah diterima oleh bootcamp dan akan segera diproses untuk digunakan oleh peminjam digunakann.", linkContent: "Lihat sekarang >"))
        notificationData.append(NotificationResponse.init(notifTitle: "Ada Peminjam Baru yang bisa didanai", notifContent: "Dana yang kamu pinjamkan telah diterima oleh bootcamp dan akan segera diproses untuk digunakan oleh peminjam digunakann.", linkContent: "Lihat sekarang >"))
        notificationData.append(NotificationResponse.init(notifTitle: "Ada Peminjam Baru yang bisa didanai", notifContent: "Dana yang kamu pinjamkan telah diterima oleh bootcamp dan akan segera diproses untuk digunakan oleh peminjam digunakann.", linkContent: "Lihat sekarang >"))
        notificationData.append(NotificationResponse.init(notifTitle: "Ada Peminjam Baru yang bisa didanai", notifContent: "Dana yang kamu pinjamkan telah diterima oleh bootcamp dan akan segera diproses untuk digunakan oleh peminjam digunakann.", linkContent: "Lihat sekarang >"))
        notificationData.append(NotificationResponse.init(notifTitle: "Ada Peminjam Baru yang bisa didanai", notifContent: "Dana yang kamu pinjamkan telah diterima oleh bootcamp dan akan segera diproses untuk digunakan oleh peminjam digunakann.", linkContent: "Lihat sekarang >"))
        notificationData.append(NotificationResponse.init(notifTitle: "Ada Peminjam Baru yang bisa didanai", notifContent: "Dana yang kamu pinjamkan telah diterima oleh bootcamp dan akan segera diproses untuk digunakan oleh peminjam digunakann.", linkContent: "Lihat sekarang >"))
    }
    
    @objc func notifbuttonClicked(){
        let availableBorrowerListViewControllerPage = AvailableBorrowerListViewController()
        navigationController?.pushViewController(availableBorrowerListViewControllerPage, animated: false)
    }
    
    @IBAction func fundingButtonAction(_ sender: Any) {
        let availableBorrowerListViewControllerPage = AvailableBorrowerListViewController()
        navigationController?.pushViewController(availableBorrowerListViewControllerPage, animated: false)
    }
}

extension LenderNotificationPageViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (notificationData.count == 0) {
            notificationEmptyImage.isHidden = false
            notificationEmptyTitle.isHidden = false
            notificationEmptySubtitle.isHidden = false
            fundingButton.isHidden = false
            }
        
        return notificationData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NotificationCell", for: indexPath) as! NotificationViewCell
        
        cell.clipsToBounds = false
        
        cell.notificationTitle.text = notificationData[indexPath.row].notifTitle
        cell.notificationContent.text = notificationData[indexPath.row].notifContent
        
        cell.notificationButton.addTarget(self, action: #selector(notifbuttonClicked), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 40, height: 175)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let assetDetailsPage = AssetDetailsViewController()
        navigationController?.pushViewController(assetDetailsPage, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
}
