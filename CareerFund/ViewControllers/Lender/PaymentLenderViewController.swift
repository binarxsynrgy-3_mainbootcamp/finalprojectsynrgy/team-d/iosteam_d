//
//  PaymentLenderViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 24/11/21.
//

import UIKit
import Toaster

class PaymentLenderViewController: BaseViewController, PaymentMethodLender {
    
    @IBOutlet weak var paymentButton: ButtonBlue!
    @IBOutlet weak var bankAccount: UILabel!
    @IBOutlet weak var checkedButton: UIButton!
    @IBOutlet weak var termsAndConditionText: UILabel!
    @IBOutlet weak var lblTotalFunding: UILabel!
    
    var totalFunding = 0
    var unchecked = true
    var paymentId = 0
    var selectedBankName: String = "BCA Virtual Account"
    var noVirtualAccount = 122000000000001
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        let backButton = UIBarButtonItem()
        backButton.title = "Kembali"
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationItem.title = "Pembayaran"
        navigationController?.navigationBar.backgroundColor = .white
        
        bankAccount.text = selectedBankName
        paymentButton.isEnabled = false
        checkedButton.isHidden = true
        termsAndConditionText.isHidden = true
        lblTotalFunding.text = totalFunding.convertToCurrency()
        
        if selectedBankName != "  " {
            checkedButton.isHidden = false
            termsAndConditionText.isHidden = false
        }
    }
    
    
    @IBAction func paymentMethodButton(_ sender: Any) {
        let vc = PaymentMethodLenderViewController()
        vc.delegatePayLender = self
        navigationController?.pushViewController(vc, animated: false)
    }
    
    func data(payment: PaymentAccount) {
        self.paymentId = payment.id
        self.selectedBankName = payment.name
        self.noVirtualAccount = payment.number
        self.bankAccount.text = selectedBankName
    }
    
    @IBAction func debtAgreementButton(_ sender: Any) {
        let vc = DebtAgreementLetter()
        vc.url = "https://careerfund.s3.ap-southeast-1.amazonaws.com/documents/bonds/bond.pdf"
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func checkedButton(_ sender: Any) {
        if unchecked {
            checkedButton.setImage(UIImage(named:"check-box-empty"), for: .normal)
            paymentButton.isEnabled = false
            unchecked = false
        }
        else {
            checkedButton.setImage(UIImage(named:"check-box-fill"), for: .normal)
            paymentButton.isEnabled = true
            unchecked = true
        }
    }
    
    @IBAction func requestPaymentCodeButton(_ sender: Any) {
        let loanId = Session.shared.loanId
        self.showLoading()
        Request().submitFunding(amount: totalFunding, loanId: loanId ){ result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    self.dismiss(animated: false, completion: {
                        self.goToDetailPayment()
                    })
                case .failure:
                    self.dismiss(animated: false, completion: {
                        self.failAlert()
                    })
                }
            }
        }
        
    }
    
    func failAlert(){
        let alert = UIAlertController(title: "Pendanaan gagal", message: "Pendanaan sudah mencapai batas maksimum, silahkan kembali dan pilih pendanaan yang masih tersedia", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Kembali", style: .default, handler: { _ in
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
        }))

        self.present(alert, animated: true)
    }
    
    func goToDetailPayment(){
        let vc = RequestPaymentCodeViewController()
        vc.totalPayment = totalFunding
        vc.numberVA = noVirtualAccount
        navigationController?.pushViewController(vc, animated: false)
    }
}

protocol PaymentMethodLender{
    func data (payment : PaymentAccount)
}
