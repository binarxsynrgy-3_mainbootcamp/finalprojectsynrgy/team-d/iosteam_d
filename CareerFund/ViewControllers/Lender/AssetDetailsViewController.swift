//
//  ReportDetailsViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 25/11/21.
//

import UIKit

class AssetDetailsViewController: UIViewController {
    
    @IBOutlet weak var loanAmountLabel: UILabel!
    @IBOutlet weak var tenureInstallmentLabel: UILabel!
    @IBOutlet weak var loanInterestLabel: UILabel!
    @IBOutlet weak var loanTenurePaidLabel: UILabel!
    @IBOutlet weak var totalTenureLabel: UILabel!
    @IBOutlet weak var loanPaidAmountLabel: UIProgressView!
    @IBOutlet weak var borrowerNameLabel: UILabel!
    @IBOutlet weak var institutionLabel: UILabel!
    @IBOutlet weak var bootcampLabel: UILabel!
    
    public var loanAmount = 0
    public var tenureInstallment = 0
    public var loanInterest = 0.00
    public var loanTenurePaid = 0
    public var totalTenure = 0
    public var loanPaidAmount : Float = 0.00
    public var borrowerName = ""
    public var institutionName = ""
    public var bootcampName = ""
    public var assesment = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        
        let backButton = UIBarButtonItem()
        backButton.title = "Kembali"
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationItem.title = "Detail Asset"
        navigationController?.navigationBar.backgroundColor = .white
        
        let nsString = String(loanInterest) as NSString
        if nsString.length >= 4
        {
            loanInterestLabel.text = nsString.substring(with: NSRange(location: 0, length: nsString.length > 4 ? 4 : nsString.length)) + "%"
        }
        
        loanAmountLabel.text = loanAmount.convertToCurrency()
        tenureInstallmentLabel.text = "\(tenureInstallment) Bulan"
        loanTenurePaidLabel.text = "\(loanTenurePaid)"
        totalTenureLabel.text = "\(totalTenure)"
        loanPaidAmountLabel.progress = loanPaidAmount
        borrowerNameLabel.text = borrowerName
        institutionLabel.text = institutionName
        bootcampLabel.text = bootcampName
    }
    
    @IBAction func borrowerInformationButton(_ sender: Any) {
        let borrowerInformationPage = BorrowerInformationViewController()
        let fundedReturnAmount = Double(loanAmount) * (loanInterest/100)
        borrowerInformationPage.senderPageToHideFunding = true
        borrowerInformationPage.borrowerName = borrowerName
        borrowerInformationPage.institutionName = institutionName
        borrowerInformationPage.bootcampName = bootcampName
        borrowerInformationPage.assesment = "\(assesment)"
        borrowerInformationPage.loanAmount = loanAmount
        borrowerInformationPage.loanTenure = tenureInstallment
        borrowerInformationPage.loanInterest = loanInterest
        borrowerInformationPage.returnAmount = fundedReturnAmount
        navigationController?.pushViewController(borrowerInformationPage, animated: false)
    }
}

