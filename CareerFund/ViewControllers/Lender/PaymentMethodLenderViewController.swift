//
//  PaymentMethodLenderViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 24/11/21.
//

import UIKit

class PaymentMethodLenderViewController: BaseViewController {
    
    @IBOutlet weak var bankTransferCollectionView: UICollectionView!
    @IBOutlet weak var selectButton: ButtonBlue!
    
    var senderPage = ""
    var selectedPaymentMethod = ""
    var selectedNoAccount = 0
    var bankTransferMethod = [PaymentAccount]()
    var delegatePayBorrower : PaymentMethodBorrower?
    var delegatePayLender : PaymentMethodLender?
    var delegatePayWidrawal : PaymentMethodWidrawal?
    var selectedPaymentData : PaymentAccount?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        let backButton = UIBarButtonItem()
        backButton.title = "Kembali"
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationItem.title = "Metode Pembayaran"
        navigationController?.navigationBar.backgroundColor = .white
        
        selectButton.isEnabled = false
        
        bankTransferCollectionView.register(UINib(nibName: "PaymentMethodViewCell", bundle: nil), forCellWithReuseIdentifier: "BankTransferCell")
        bankTransferCollectionView.delegate = self
        bankTransferCollectionView.dataSource = self
        bankTransferCollectionView.showsVerticalScrollIndicator = false
        getPaymentType()
    }
    
    func getPaymentType (){
        self.showLoading()
        Request().getPaymentType { result in
            DispatchQueue.main.async {
                self.dismiss(animated: false, completion: nil)
                switch result {
                case .success(let response):
                    self.bankTransferMethod = response.data[0].paymentAccount
                    self.bankTransferCollectionView.reloadData()
                case .failure:
                    print("getPaymentType failed")
                }
            }
        }
    }
    
    
    @IBAction func selectPaymentButton() {
        delegatePayBorrower?.data(payment: selectedPaymentData!)
        delegatePayLender?.data(payment: selectedPaymentData!)
        delegatePayWidrawal?.data(payment: selectedPaymentData!)
        self.navigationController?.popViewController(animated: true)        
    }
}

extension PaymentMethodLenderViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        bankTransferMethod.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BankTransferCell", for: indexPath) as! PaymentMethodViewCell
        cell.clipsToBounds = false
        let bankName = bankTransferMethod[indexPath.row].bank.name
        let bankLogo = bankTransferMethod[indexPath.row].bank.logoPath
        
        let urlImg = URL(string:bankLogo)!
        if let data = try? Data(contentsOf: urlImg) {
            cell.bankLogo.image = UIImage(data: data)
        }
        cell.bankName.text = bankName

        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 65)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let bankTransferCell = bankTransferCollectionView.cellForItem(at: indexPath) as? PaymentMethodViewCell
        selectButton.isEnabled = true
        bankTransferCell?.imgChecked.isHidden = false
        selectedPaymentMethod = bankTransferMethod[indexPath.row].name
        selectedNoAccount = bankTransferMethod[indexPath.row].number
        
        selectedPaymentData = bankTransferMethod[indexPath.row]
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let bankTransferCell = bankTransferCollectionView.cellForItem(at: indexPath) as? PaymentMethodViewCell
        
        bankTransferCell?.imgChecked.isHidden = true
        
    }
    
}
