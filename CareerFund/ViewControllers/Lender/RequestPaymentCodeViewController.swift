//
//  RequestPaymentCodeViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 13/12/21.
//

import UIKit

class RequestPaymentCodeViewController: UIViewController {
    
    @IBOutlet weak var lblTotalPayment: UILabel!
    @IBOutlet weak var lblNoVA: UILabel!
    @IBOutlet weak var lblPaymentDate: UILabel!

    var totalPayment = 0
    var numberVA = 0
    var paymentDate = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()

    }

    func setupView(){
        self.navigationItem.title = "Detail Pembayaran"
        self.navigationItem.hidesBackButton = true
        self.navigationController?.interactivePopGestureRecognizer!.isEnabled = false
        
        lblTotalPayment.text = totalPayment.convertToCurrency()
        lblNoVA.text = "\(numberVA)"
        lblPaymentDate.text = "\(paymentDate)"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        lblPaymentDate.text = "\(dateFormatter.string(from: Date.tomorrow))"
    }
    
    @IBAction func btnBack(_ sender: Any) {
        Session.shared.removeIdSession()
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 5], animated: true)
        let vc = WithdrawLenderAssetsViewController()
        vc.loadDataLenderLoans()

    }
    

}
