//
//  DebtAgreementLetter.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 08/12/21.
//

import Foundation
import WebKit

class DebtAgreementLetter: UIViewController, WKNavigationDelegate {
    
    var webView: WKWebView!
    
    override func loadView() {

        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: "https://law.uii.ac.id/wp-content/uploads/2012/12/Contoh-surat-perjanjian-utang-piutang-dengan-kuasa-hipotek-FH-UII.pdf")!
        webView.load(URLRequest(url: url))
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            title = "Surat Perjanjian Hutang Piutang"
        }
    
}
