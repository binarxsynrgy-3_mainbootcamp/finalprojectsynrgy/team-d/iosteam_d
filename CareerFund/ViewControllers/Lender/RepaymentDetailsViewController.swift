//
//  PaymentDetailsViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 06/12/21.
//

import UIKit

class RepaymentDetailsViewController: UIViewController {
    
    @IBOutlet weak var totalLoanLabel: UILabel!
    @IBOutlet weak var monthlyAmountToPayLabel: UILabel!
    @IBOutlet weak var returnAmountLabel: UILabel!
    
    public var totalLoan = 0
    public var monthlyAmountToPay = 0
    public var returnAmount = 0.00
    var numberFormatter = NumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
    }
    
    func setupView(){
        navigationItem.title = "Detail Pelunasan"
        navigationController?.navigationBar.backgroundColor = .white
        
        numberFormatter.numberStyle = .currency
        numberFormatter.currencySymbol = "Rp"
        numberFormatter.maximumFractionDigits = 0
        
        totalLoanLabel.text = numberFormatter.string(from: NSNumber(value: totalLoan))
        monthlyAmountToPayLabel.text = numberFormatter.string(from: NSNumber(value: monthlyAmountToPay))
        returnAmountLabel.text = numberFormatter.string(from: NSNumber(value: returnAmount))
        
    }
    
}
