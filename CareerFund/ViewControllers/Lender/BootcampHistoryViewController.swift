//
//  BootcampHistoryViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 06/12/21.
//

import UIKit

class BootcampHistoryViewController: UIViewController {

    @IBOutlet weak var bootcampHistoryCollectionView: UICollectionView!
    
    var bootcampHistoryData = [BootcampHistoryResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        }

    func setupView(){
        navigationItem.title = "Dashboard"
        navigationController?.navigationBar.backgroundColor = .white


        bootcampHistoryCollectionView.register(UINib(nibName: "BootcampHistoryViewCell", bundle: nil), forCellWithReuseIdentifier: "BootcampHistoryCell")
        
        bootcampHistoryCollectionView.delegate = self
        bootcampHistoryCollectionView.dataSource = self
        bootcampHistoryCollectionView.showsVerticalScrollIndicator = false
    
        bootcampHistoryData.append(BootcampHistoryResponse.init(logo: "binar", name: "Binar Academy", period: "sedang berlangsung"))
        bootcampHistoryData.append(BootcampHistoryResponse.init(logo: "hacktiv8", name: "Hacktiv8", period: "11 Januari 2022 - 11 Maret 2022"))
        bootcampHistoryData.append(BootcampHistoryResponse.init(logo: "purwadhika", name: "Purwadhika", period: "11 April 2022 - 11 Juni 2022"))
    }
}

extension BootcampHistoryViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        bootcampHistoryData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BootcampHistoryCell", for: indexPath) as! BootcampHistoryViewCell

        cell.clipsToBounds = false
        
        cell.bootcampLogo.image = UIImage(named: bootcampHistoryData[indexPath.row].logo)
        cell.bootcampTitle.text = bootcampHistoryData[indexPath.row].name
        cell.bootcampPeriod.text = bootcampHistoryData[indexPath.row].period
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 40, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
}
