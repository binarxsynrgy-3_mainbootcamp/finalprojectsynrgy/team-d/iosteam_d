//
//  BootcampDetailsViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 06/12/21.
//

import UIKit

class BootcampDetailsViewController: UIViewController {
    
    @IBOutlet weak var institutionsNameLabel: UILabel!
    @IBOutlet weak var institutionsLogoImage: UIImageView!
    @IBOutlet weak var bootcampNameLabel: UILabel!
    @IBOutlet weak var bootcampPriceLabel: UILabel!
    @IBOutlet weak var bootcampDurationLabel: UILabel!
    @IBOutlet weak var bootcampPeriodLabel: UILabel!
    @IBOutlet weak var interestLabel: UILabel!
    @IBOutlet weak var interestAmountLabel: UILabel!
    @IBOutlet weak var downPaymentLabel: UILabel!
    
    public var institutionsName = ""
    public var institutionsLogo = ""
    public var bootcampName = ""
    public var bootcampPrice = 0
    public var bootcampDuration = 0
    public var bootcampStartPeriod = ""
    public var bootcampEndPeriod = ""
    public var loanInterest = 0.00
    public var borrowerDownPayment = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    func setupView(){
        navigationItem.title = "Detail Bootcamp"
        navigationController?.navigationBar.backgroundColor = .white
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let startDateString = bootcampStartPeriod
        let endDateString = bootcampEndPeriod
        let startDate = formatter.date(from: startDateString)!
        let endDate = formatter.date(from: endDateString)!
        formatter.dateFormat = "MMM yyy"
        let bootcampStartDate = formatter.string(from: startDate)
        let bootcampEndDate = formatter.string(from: endDate)
        
        let urlImg = URL(string:institutionsLogo)!
        if let data = try? Data(contentsOf: urlImg) {
            institutionsLogoImage.image = UIImage(data: data)
        }
        let interestAmount = Double(bootcampPrice) * (loanInterest/100)
        
        let nsString = String(loanInterest) as NSString
        if nsString.length >= 4
        {
            interestLabel.text = nsString.substring(with: NSRange(location: 0, length: nsString.length > 4 ? 4 : nsString.length)) + "%"
        }
        
        institutionsNameLabel.text = institutionsName
        bootcampNameLabel.text = bootcampName
        bootcampPriceLabel.text = bootcampPrice.convertToCurrency()
        bootcampDurationLabel.text = "\(bootcampDuration) Bulan"
        bootcampPeriodLabel.text = bootcampStartDate + " - " + bootcampEndDate
        interestAmountLabel.text = interestAmount.convertToCurrency()
        downPaymentLabel.text = borrowerDownPayment.convertToCurrency()
    }
}
