//
//  LoanConfirmationViewController.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 24/11/21.
//

import UIKit

class LoanConfirmationViewController: BaseViewController {
    
    @IBOutlet weak var borrowerNameLabel: UILabel!
    @IBOutlet weak var borrowerEmploymentLabel: UILabel!
    @IBOutlet weak var borrowerAddressLabel: UILabel!
    @IBOutlet weak var borrowerPhoneNumberLabel: UILabel!
    @IBOutlet weak var lenderNameLabel: UILabel!
    @IBOutlet weak var lenderEmploymentLabel: UILabel!
    @IBOutlet weak var lenderAddressLabel: UILabel!
    @IBOutlet weak var lenderPhoneNumberLabel: UILabel!
    @IBOutlet weak var fundingAmountLabel: UILabel!
    @IBOutlet weak var loanTenureLabel: UILabel!
    @IBOutlet weak var loanInterestLabel: UILabel!
    @IBOutlet weak var institutionLabel: UILabel!
    @IBOutlet weak var bootcampNameLabel: UILabel!
    @IBOutlet weak var bootcampBatchLabel: UILabel!
    
    public var borrowerName = ""
    public var borrowerEmployment = ""
    public var borrowerAddress = ""
    public var borrowerPhoneNumber = ""
    public var borrowerDownPayment = 0
    public var fundingAmount = 0
    public var loanTenure = 0
    public var loanInterest = 0.00
    public var institutionName = ""
    public var institutionLogo = ""
    public var bootcampName = ""
    public var bootcampBatch = ""
    public var bootcampStartPeriod = ""
    public var bootcampEndPeriod = ""
    public var bootcampPrice = 0
    public var bootcampDuration = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        let backButton = UIBarButtonItem()
        backButton.title = "Kembali"
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationItem.title = "Konfirmasi"
        navigationController?.navigationBar.backgroundColor = .white
        
        getProfile()
        
        let dateString = bootcampStartPeriod
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: dateString)!
        
        formatter.dateFormat = "yyyy"
        let year = formatter.string(from: date)
        formatter.dateFormat = "MMMM"
        let month = formatter.string(from: date)
        
        let nsString = String(loanInterest) as NSString
        if nsString.length >= 4
        {
            loanInterestLabel.text = nsString.substring(with: NSRange(location: 0, length: nsString.length > 4 ? 4 : nsString.length)) + "%"
        }
        
        borrowerNameLabel.text = borrowerName
        borrowerEmploymentLabel.text = "Karyawan Swasta"
        borrowerAddressLabel.text = borrowerAddress
        borrowerPhoneNumberLabel.text = borrowerPhoneNumber
        fundingAmountLabel.text = fundingAmount.convertToCurrency()
        loanTenureLabel.text = "\(loanTenure)" + " bulan"
        institutionLabel.text = institutionName
        bootcampNameLabel.text = bootcampName
        bootcampBatchLabel.text = month + " " + year
    }
    
    func getProfile (){
        self.showLoading()
        Request().getDataProfile { result in
            self.dismiss(animated: false, completion: nil)
            switch result {
            case .success(let response):
                self.lenderNameLabel.text = response.name
                self.lenderEmploymentLabel.text = "Wiraswasta"
                self.lenderAddressLabel.text = response.address
                self.lenderPhoneNumberLabel.text = response.phone
            case .failure:
                print("getListInterest failed")
            }
        }
    }
    
    @IBAction func loanConfirmationButton(_ sender: Any) {
        let vc = PaymentLenderViewController()
        vc.totalFunding = fundingAmount
        navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func repaymentDetails(_ sender: Any) {
        let repaymentDetails = RepaymentDetailsViewController()
        let fundedReturnAmount = (Double(fundingAmount) * loanInterest)/100
        let fundedmonthlyAmountToPay = (fundingAmount + Int(fundedReturnAmount)) / loanTenure
        repaymentDetails.totalLoan = fundingAmount
        repaymentDetails.monthlyAmountToPay = fundedmonthlyAmountToPay
        repaymentDetails.returnAmount = fundedReturnAmount
        navigationController?.pushViewController(repaymentDetails, animated: false)
        
    }
    
    @IBAction func bootcampDetails(_ sender: Any) {
        let bootcampDetailsPage = BootcampDetailsViewController()
        bootcampDetailsPage.institutionsName = institutionName
        bootcampDetailsPage.institutionsLogo = institutionLogo
        bootcampDetailsPage.bootcampName = bootcampName
        bootcampDetailsPage.bootcampPrice = bootcampPrice
        bootcampDetailsPage.bootcampDuration = bootcampDuration
        bootcampDetailsPage.bootcampStartPeriod = bootcampStartPeriod
        bootcampDetailsPage.bootcampEndPeriod = bootcampEndPeriod
        bootcampDetailsPage.loanInterest = loanInterest
        bootcampDetailsPage.borrowerDownPayment = borrowerDownPayment
        navigationController?.pushViewController(bootcampDetailsPage, animated: false)
        
    }
    
}
