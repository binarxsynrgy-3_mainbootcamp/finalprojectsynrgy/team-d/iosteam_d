//
//  LoanViewController.swift
//  CareerFund
//
//  Created by MAC on 24/11/21.
//

import UIKit

class LoanViewController: BaseViewController {
    
    @IBOutlet weak var oldLoanCollectionView: UICollectionView!
    @IBOutlet weak var newLoanCollectionView: UICollectionView!
    @IBOutlet weak var oldCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var newCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var lblEmptyHistory: UILabel!
    @IBOutlet weak var chooseClassView: UIView!
    @IBOutlet weak var chooseAssesmentView: UIView!
    @IBOutlet weak var lblAccessWeb: UILabel!
    @IBOutlet weak var lblChooseClass: UILabel!

    var cellHeight : CGFloat = 200
    var oldLoan =  [DataLoanModel]()
    var newLoan =  [DataLoanModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        self.navigationItem.title = "Pinjaman"
        chooseClassView.layer.cornerRadius = 8
        chooseAssesmentView.layer.cornerRadius = 8

        oldLoanCollectionView.register(UINib(nibName: "BorrowerDashboardCell", bundle: nil), forCellWithReuseIdentifier: "OldLoanCell")
        oldLoanCollectionView.delegate = self
        oldLoanCollectionView.dataSource = self
        oldLoanCollectionView.showsVerticalScrollIndicator = false
        
        newLoanCollectionView.register(UINib(nibName: "BorrowerDashboardCell", bundle: nil), forCellWithReuseIdentifier: "NewLoanCell")
        newLoanCollectionView.delegate = self
        newLoanCollectionView.dataSource = self
        newLoanCollectionView.showsVerticalScrollIndicator = false
    
        lblAccessWeb.isUserInteractionEnabled = true
        lblAccessWeb.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToClassPage)))
        lblChooseClass.isUserInteractionEnabled = true
        lblChooseClass.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToClassPage)))
        
        loadData()
    }
    
    func updateHeight() {
        self.oldCollectionViewHeight.constant = CGFloat(oldLoan.count) * self.cellHeight
        self.newCollectionViewHeight.constant = CGFloat(newLoan.count) * self.cellHeight

    }
    
    func loadData(){
        self.showLoading()
        Request().getLoanList(completion: { result in
            self.dismiss(animated: true, completion: nil)
            switch result {
            case .success(let response):
                if response.data.count > 0 {
                    self.setLoan(data: response.data)
                    self.emptyView.isHidden = true
                } else {
                    self.emptyView.isHidden = false
                }
            case .failure(let error):
                print("loadDataClass failed->\(error)")
            }
        })
    }
    
    func setLoan(data : [DataLoanModel]){
        for loan in data {
            let currentDate = Date()
            let startDate = loan.aclass?.startDate
            if  (startDate?.date())! > currentDate {
                newLoan.append(loan)
            } else {
                oldLoan.append(loan)
            }
        }
        
        if newLoan.isEmpty {
            lblEmptyHistory.isHidden = true
            chooseClassView.isHidden = false
        } else {
            lblEmptyHistory.isHidden = false
            chooseClassView.isHidden = true
        }
        
        if oldLoan.isEmpty {
            lblEmptyHistory.isHidden = false
        } else {
            lblEmptyHistory.isHidden = true
        }
        
        
        newLoanCollectionView.reloadData()
        oldLoanCollectionView.reloadData()
        self.updateHeight()
    }
    
    @objc func goToDetailLoan(_ gesture: UITapGestureRecognizer){
        let vc = LoanDetailViewController()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func goToClassPage(){
        let vc = ListClassViewController()
        let nv = UINavigationController(rootViewController: vc)
        self.present(nv, animated: true, completion: nil)
    }
    
}

extension LoanViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == oldLoanCollectionView {
            return oldLoan.count
        } else {
            return newLoan.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == oldLoanCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OldLoanCell", for: indexPath) as! BorrowerDashboardCell
            cell.clipsToBounds = false
            cell.setupView(data: oldLoan[indexPath.row], from: "Loan")
            cell.openAction = {
                let vc = LoanDetailViewController()
                vc.data = self.oldLoan[indexPath.row]
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: false)
            }
            return cell
            
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewLoanCell", for: indexPath) as! BorrowerDashboardCell
            cell.clipsToBounds = false
            cell.setupView(data: newLoan[indexPath.row], from: "Loan")
            cell.openAction = {
                let vc = LoanDetailViewController()
                vc.data = self.newLoan[indexPath.row]
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: false)
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 200)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }    
}
