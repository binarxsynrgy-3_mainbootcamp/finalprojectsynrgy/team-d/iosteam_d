//
//  ListClassViewController.swift
//  CareerFund
//
//  Created by MAC on 23/11/21.
//

import UIKit
import DropDown

class ListClassViewController: BaseViewController, FilterInsert {

    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchInput: UITextField!
    @IBOutlet weak var lblSort: UILabel!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var classCollectioView: UICollectionView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var emptyView: UIView!
    
    let dropDown = DropDown()
    var classData = [Class]()
    var keyword = ""
    var categories : [Int] = []
    var institutions : [Int] = []
    var feeMax = "0"
    var feeMin = "0"
    var sort = "price"
    var order = "asc"
    var profileVerified = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
    }
    

    func setupView(){
        self.navigationItem.title = "List Kelas"
        let filter = UIBarButtonItem(title : "Filter", style: .plain, target: self, action:  #selector(goToFilterPage))
        self.navigationItem.rightBarButtonItem  = filter
        
        profileVerified = Session.shared.isVerified
        
        searchInput.delegate = self
        searchInput.returnKeyType = .search
        searchInput.addTarget(self, action: #selector(textFieldShouldReturn(_:)), for: .editingChanged)

        searchView.layer.cornerRadius = 8
        sortView.layer.cornerRadius = 8

        classCollectioView.register(UINib(nibName: "BorrowerClassCell", bundle: nil), forCellWithReuseIdentifier: "BorrowerClassCell")
        classCollectioView.delegate = self
        classCollectioView.dataSource = self
        classCollectioView.showsVerticalScrollIndicator = false
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tapSortMenuItem(_ :)))
        tapgesture.numberOfTapsRequired = 1
        self.sortView.addGestureRecognizer(tapgesture)
     
        loadDataClass()
    }
    
    @objc func tapSortMenuItem(_ sender: UITapGestureRecognizer) {
        dropDown.dataSource = ["Termurah", "Periode Terdekat", "Durasi Tersingkat"]
        dropDown.anchorView = sortView
        dropDown.bottomOffset = CGPoint(x: 0, y: sortView.frame.size.height)
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
          guard let _ = self else { return }
            self?.lblSort.text = item
            switch item {
            case "Termurah":
                self?.sort = "price"
                self?.order = "asc"
            case "Periode Terdekat":
                self?.sort = "startDate"
                self?.order = "asc"
            case "Durasi Tersingkat":
                self?.sort = "durationMonth"
                self?.order = "asc"
            default:
                self?.sort = ""
                self?.order = ""
            }
            self?.loadDataClass()
        }
    }
        
    
    @objc func goToFilterPage(){
        let vc = FilterClassViewController()
        vc.delegateFilter = self
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: false)
    }
    
    func loadDataClass(){
        self.loadingView.isHidden = false
        Request().getListClass(keyword: keyword, categories: categories, feeMax: feeMax, feeMin: feeMin, institutions: institutions, sort: sort, order: order) { result in
            self.loadingView.isHidden = true
            self.setEmptyFilter()
            switch result {
            case .success(let response):
                if let data = response.data {
                    if data.count > 0 {
                        self.classData = response.data!
                        self.classCollectioView.reloadData()
                        self.emptyView.isHidden = true
                    } else {
                        self.emptyView.isHidden = false
                    }
                }
               
            case .failure(let error):
                print("loadDataClass failed->\(error)")
            }
        }
    }
    
    @objc func beginSearch() {
        guard let keyword = self.searchInput.text else { return }
        self.keyword = keyword
        loadDataClass()
    }
    
    func data(institutions: [Int], interest: [Int],feeMax : Int, feeMin : Int) {
        self.categories = interest
        self.institutions = institutions
        self.feeMax = "\(feeMax)"
        self.feeMin = "\(feeMin)"
        loadDataClass()
    }
    
    func setEmptyFilter(){
        self.categories = []
        self.institutions = []
        self.feeMax = "0"
        self.feeMin = "0"
        self.sort = ""
    }
    
    func checkProfile() -> String{
        let detailProfile = "Data Diri \n"
        let fotoKtp = "Foto KTP \n"
        let fotoSelfie = "Foto Selfie dengan KTP"
        let message = detailProfile + fotoKtp + fotoSelfie
        return message
    }
    
    func showAlert(){
        let attributedString = NSAttributedString(string: "Profil Anda Belum Terverifikasi", attributes: [NSAttributedString.Key.font : UIFont.init(name: "SFProDisplay-Bold", size: 16) as Any,
           NSAttributedString.Key.foregroundColor :  UIColor().hexStringToUIColor(hex: "3163AF")])
        
        let alert = UIAlertController(title: "", message: "Silahkan Lengkapi \n \(checkProfile())", preferredStyle: .alert)
        alert.setValue(attributedString, forKey: "attributedTitle")
        alert.addAction(UIAlertAction(title: "Verifikasi Sekarang", style: .default, handler: { (_) in
            self.goToProfile()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func goToProfile(){
        let vc = ProfileViewController()
        self.navigationController?.pushViewController(vc, animated: false)
    }
}


extension ListClassViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.beginSearch()
        return true
    }
}

extension ListClassViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return classData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BorrowerClassCell", for: indexPath) as! BorrowerClassCell
        cell.clipsToBounds = false
        cell.setupView(data: classData[indexPath.row])
        cell.openAction = { [self] in
            if profileVerified != "VERIFIED" {
                showAlert()
            } else {
                let vc = ApplyLoanViewController()
                vc.dataClass = self.classData[indexPath.row]
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 220)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
}

protocol FilterInsert {
    func data (institutions :[Int], interest : [Int], feeMax : Int, feeMin : Int)
}
