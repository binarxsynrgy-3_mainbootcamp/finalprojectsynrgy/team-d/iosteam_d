//
//  DashboardBorrowerController.swift
//  CareerFund
//
//  Created by MAC on 17/11/21.
//

import UIKit

class DashboardBorrowerController: BaseViewController {
    
    
    @IBOutlet weak var lblInstalment: UILabel!
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var installmentView: UIView!
    @IBOutlet weak var assesmentView: UIView!
    @IBOutlet weak var newClassCollectionView: UICollectionView!
    @IBOutlet weak var newCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var oldClassCollectionView: UICollectionView!
    @IBOutlet weak var oldCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var lblEmptyHistory: UILabel!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var chooseAssesmentView: UIView!
    @IBOutlet weak var chooseClassView: UIView!
    @IBOutlet weak var lblAccessWeb: UILabel!
    @IBOutlet weak var lblChooseClass: UILabel!
    
    var cellHeight : CGFloat = 200
    var oldClass =  [DataLoanModel]()
    var newClass =  [DataLoanModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        self.navigationItem.title = "Dashboard"
        installmentView.layer.cornerRadius = 8
        assesmentView.layer.cornerRadius = 8
        chooseAssesmentView.layer.cornerRadius = 8
        chooseClassView.layer.cornerRadius = 8
        
        let installmeent = Session.shared.remainingDebt
        let assesment = Session.shared.assesmentScore
        
        lblInstalment.text =  installmeent.convertToCurrency()
        lblScore.text = "\(assesment)"

        oldClassCollectionView.register(UINib(nibName: "BorrowerDashboardCell", bundle: nil), forCellWithReuseIdentifier: "OldClassCell")
        oldClassCollectionView.delegate = self
        oldClassCollectionView.dataSource = self
        oldClassCollectionView.showsVerticalScrollIndicator = false
        
        newClassCollectionView.register(UINib(nibName: "BorrowerDashboardCell", bundle: nil), forCellWithReuseIdentifier: "NewClassCell")
        newClassCollectionView.delegate = self
        newClassCollectionView.dataSource = self
        newClassCollectionView.showsVerticalScrollIndicator = false
        
        lblAccessWeb.isUserInteractionEnabled = true
        lblAccessWeb.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openAssessment)))
        lblChooseClass.isUserInteractionEnabled = true
        lblChooseClass.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToClassPage)))
        loadDataClass()
    }
    
    func updateHeight() {
        self.oldCollectionHeight.constant = CGFloat(oldClass.count) * self.cellHeight
        self.newCollectionHeight.constant = CGFloat(newClass.count) * self.cellHeight
    }
    
    func loadDataClass(){
        self.showLoading()
        Request().getLoanList(completion: { result in
            self.oldClass.removeAll()
            self.newClass.removeAll()
            self.dismiss(animated: true, completion: nil)
            switch result {
            case .success(let response):
                if response.data.count > 0 {
                    self.setLoan(data: response.data)
                    self.emptyView.isHidden = true
                } else {
                    self.setLoan(data: response.data)
                    if Session.shared.assesmentScore == 0.0 {
                        self.chooseAssesmentView.isHidden = false
                        self.chooseClassView.isHidden = true
                        self.emptyView.isHidden = true
                    } else {
                        self.chooseAssesmentView.isHidden = true
                        self.chooseClassView.isHidden = false
                        self.emptyView.isHidden = false
                    }
                }
                self.updateHeight()
            case .failure(let error):
                print("loadDataClass failed->\(error)")
            }
        })
    }
    
    func setLoan(data : [DataLoanModel]){
        lblScore.text = "0"
        for loan in data {
            let currentDate = Date()
            let startDate = loan.aclass?.startDate
            if (startDate?.date())! > currentDate {
                newClass.append(loan)
            } else {
                oldClass.append(loan)
            }
        }
        
        
        if newClass.isEmpty {
            chooseClassView.isHidden = false
        } else {
            chooseClassView.isHidden = true
        }
        
        if oldClass.isEmpty {
            lblEmptyHistory.isHidden = false
        } else {
            lblEmptyHistory.isHidden = true
        }
        
        newClassCollectionView.reloadData()
        oldClassCollectionView.reloadData()
        self.updateHeight()
    }
    
    @objc func goToClassPage(){
        let vc = ListClassViewController()
        let nv = UINavigationController(rootViewController: vc)
        self.present(nv, animated: true, completion: nil)
    }
    
    @objc func openAssessment(){
        guard let url = URL(string: "https://career-fund.vercel.app/assessment") else { return }
        UIApplication.shared.open(url)
    }
}

extension DashboardBorrowerController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == oldClassCollectionView {
            return oldClass.count
        } else {
            return newClass.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == oldClassCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OldClassCell", for: indexPath) as! BorrowerDashboardCell
            cell.clipsToBounds = false
            cell.setupView(data: oldClass[indexPath.row],from: "Dashboard")
            cell.openAction = {
                let vc = LoanDetailViewController()
                vc.data = self.oldClass[indexPath.row]
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: false)
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewClassCell", for: indexPath) as! BorrowerDashboardCell
            cell.clipsToBounds = false
            cell.setupView(data: newClass[indexPath.row],from: "Dashboard")
            cell.openAction = {
                let vc = LoanDetailViewController()
                vc.data = self.newClass[indexPath.row]
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: false)
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == oldClassCollectionView {
            let vc = LoanDetailViewController()
            vc.data = self.oldClass[indexPath.row]
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: false)
        } else {
            let vc = LoanDetailViewController()
            vc.data = self.newClass[indexPath.row]
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 200)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}
