//
//  ConfirmLoanViewController.swift
//  CareerFund
//
//  Created by MAC on 29/11/21.
//

import UIKit

class ConfirmLoanViewController: BaseViewController {
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblPersonalName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblClassName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblInterestFee: UILabel!
    @IBOutlet weak var lblAmountDp: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblPeriode: UILabel!
    @IBOutlet weak var statusLoanView: UIView!
    @IBOutlet weak var contractView: UIView!
    @IBOutlet weak var checkBox: Checkbox!
    @IBOutlet weak var btnConfirm: ButtonBlue!
    @IBOutlet weak var lblWordingContract: UILabel!
    
    var periode = ""
    var dataClass : Class?
    var dataLoan : DataLoanModel?
    var loanAmount = 0
    var tenure = 0
    var fee = 0
    var interest = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    func setupView(){
        self.navigationItem.title = "Konfirmasi"

        lblCompany.text = dataClass?.bootcamp?.name
        lblClassName.text = dataClass?.name
        lblAmount.text = dataClass?.price.convertToCurrency()
        lblAmountDp.text = loanAmount.convertToCurrency()

        let digit = Double(round(100 * interest) / 100)
        lblRate.text = "\(digit) %"
        lblInterestFee.text = fee.convertToCurrency()
        lblDuration.text = "\(dataClass?.duration ?? 0) Bulan"

        lblPeriode.text = "\(dataClass!.startDate.date(dateFormat: "yyyy-MM-dd").string(dateFormat: "MMM")) - \(dataClass!.endDate.date(dateFormat: "yyyy-MM-dd").string(dateFormat: "MMM yyyy"))"
    
        if let url = dataClass?.bootcamp?.institutions[0].logo {
            let urlImg = URL(string:(url))!
            if let data = try? Data(contentsOf: urlImg) {
                self.imgLogo.image  = UIImage(data: data)
            }
        }
        
        checkBox.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(checkedAgreeContract)))
        contractView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openContract)))
        btnConfirm.addTarget(self, action: #selector(submitLoan), for: .touchUpInside)
        
        statusLoanView.isHidden = true
        contractView.isHidden = true
        checkBox.isHidden = true
        lblWordingContract.isHidden = true
        
        let name = Session.shared.name
        let phone = Session.shared.phone
        let address = Session.shared.address

        lblPersonalName.text = name
        lblPhone.text = phone
        lblAddress.text = address
        
        let url =  Session.shared.photoProfile
        if  url != "" {
            let urlImg = URL(string:(url))!
            if let data = try? Data(contentsOf: urlImg) {
                self.imgProfile.image  = UIImage(data: data)
            }
        }

    }

    @objc func checkedAgreeContract() {
        self.checkBox.isSelected = !self.checkBox.isSelected
        if checkBox.isSelected {
            self.btnConfirm.isEnabled = true
        } else {
            self.btnConfirm.isEnabled = false
        }
    }
    
    @objc func submitLoan(_ sender: UIButton){
        self.showLoading()
        Request().submitLoanClass(id: dataClass?.id ?? 0, dp: loanAmount, tenor: tenure){ result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    self.dataLoan = response.data
                    self.dismiss(animated: false, completion: {
                        self.successAlert()
                    })
                case .failure:
                    self.dismiss(animated: false, completion : nil)
                    print("gagal submit pinjaman")
                }
            }
        }
    }
    
    func successAlert(){
        let attributedString = NSAttributedString(string: "Pinjaman Anda Berhasil Dibuat", attributes: [NSAttributedString.Key.font : UIFont.init(name: "SFProDisplay-Bold", size: 16) as Any,
           NSAttributedString.Key.foregroundColor :  UIColor().hexStringToUIColor(hex: "3163AF")])
        
        let alert = UIAlertController(title: "", message: "Silahkan Lakukan Pembayaran Agar Pinjaman Anda Dapat Diteruskan Ke Pendana", preferredStyle: .alert)
        alert.setValue(attributedString, forKey: "attributedTitle")
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
            self.goToDetailPayment()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func openContract(){
        let vc = DebtAgreementLetter()
        vc.url = "https://careerfund.s3.ap-southeast-1.amazonaws.com/documents/bonds/bond.pdf"
        navigationController?.pushViewController(vc, animated: false)
    }
    
    func goToDetailPayment(){
        let vc = DetailPaymentViewController()
        vc.detailClass = dataLoan
        vc.loanAmount = loanAmount
        vc.loanTerm = tenure
        vc.rate = interest
        navigationController?.pushViewController(vc, animated: false)
    }
}
