//
//  ConfirmPaymentViewController.swift
//  CareerFund
//
//  Created by MAC on 05/12/21.
//

import UIKit
import Toaster

class DetailPaymentViewController: UIViewController, PaymentMethodBorrower {

    @IBOutlet weak var contractView: UIView!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblClassName: UILabel!
    @IBOutlet weak var lblTerm: UILabel!
    @IBOutlet weak var lblPrecentage: UILabel!
    @IBOutlet weak var lblPaymentName: UILabel!
    @IBOutlet weak var lblVirtualAccount: UILabel!
    @IBOutlet weak var lblInstallment: UILabel!
    @IBOutlet weak var lblPaymentDate: UILabel!
    @IBOutlet weak var imgCopy: UIImageView!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    @IBOutlet weak var lblHowToTransfer: UILabel!
    
    var detailClass : DataLoanModel?
    var loanAmount = 0
    var loanTerm = 0
    var rate = 0.0
    var paymentId = 1
    var bankName = "BCA Virtual Account"
    var numberVA = 122000000000001
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        self.navigationItem.title = "Konfirmasi"
        self.navigationItem.hidesBackButton = true
        self.navigationController?.interactivePopGestureRecognizer!.isEnabled = false
        
        contractView.layer.cornerRadius = 8
        lblCompanyName.text = detailClass?.aclass?.bootcamp?.institutions[0].name
        lblClassName.text = detailClass?.aclass?.bootcamp?.categories[0].name
        lblAmount.text = detailClass?.aclass?.price.convertToCurrency()
        lblInstallment.text = loanAmount.convertToCurrency()
        lblTerm.text = "\(loanTerm) Bulan"
        lblVirtualAccount.text = "\(numberVA)"
        lblPaymentName.text = bankName
        
        let digit = Double(round(100 * rate) / 100)
        lblPrecentage.text = "\(digit) %"
        
        let step = "Pilih menu “Bayar/Beli” \n 1.Selanjutnya akan muncul tipe pembayaran seperti Multi Payment dan Bill Payment (PLN, PDAM, dll), untuk pembayaran VA pilih “Multi Payment” \n 2. Selanjutnya akan muncul berbagai tipe pembayaran, untuk pembayaran VA pilih “Virtual Account” \n 3.Masukkan nomor Virtual Account Anda (contoh: 8890802001287578) dan klik “Benar” \n 4. Akan muncul nominal yang harus dibayarkan. Pastikan semua informasi dan total tagihan yang ditampilkan sudah benar. Jika benar, tekan angka 1 dan pilih “Ya” \n 5.Bukti pembayaran dalam bentuk struk agar disimpan sebagai bukti pembayaran yang sah \n 7. Transaksi Anda telah selesai"
        lblHowToTransfer.text = step
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        lblPaymentDate.text = "\(dateFormatter.string(from: Date.tomorrow))"
                
        lblPaymentMethod.isUserInteractionEnabled = true
        lblPaymentMethod.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openPaymentMethod)))
        imgCopy.isUserInteractionEnabled = true
        imgCopy.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(copyNoVirtual)))
    }
    
    func data(payment: PaymentAccount) {
        self.paymentId = payment.id
        self.bankName = payment.name
        self.numberVA = payment.number
        
        lblVirtualAccount.text = "\(numberVA)"
        lblPaymentName.text = bankName
    }
    
    @objc func openPaymentMethod(){
        let vc = PaymentMethodLenderViewController()
        vc.delegatePayBorrower = self
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func copyNoVirtual(){
        UIPasteboard.general.string = "\(numberVA)"
        Toast(text: "Berhasil disalin", duration: Delay.short).show()
        Request().payLoanClass(classId: detailClass?.id ?? 0, paymentId: paymentId, amount: loanAmount){ result in
            DispatchQueue.main.async {
                self.dismiss(animated: false, completion: nil)
                switch result {
                case .success:
                    print("success bayar")
                case .failure:
                    print("gagal bayar")
                }
            }
        }
    }
    
    @IBAction func backDashboard(_ sender: Any) {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
    }
    
}

protocol PaymentMethodBorrower {
    func data (payment : PaymentAccount)
}
