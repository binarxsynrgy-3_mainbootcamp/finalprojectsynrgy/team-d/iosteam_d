//
//  FilterClassViewController.swift
//  CareerFund
//
//  Created by MAC on 25/11/21.
//

import UIKit

class FilterClassViewController: BaseViewController {
    
    @IBOutlet weak var lembagaCollectionView: UICollectionView!
    @IBOutlet weak var lembagaViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bidangCollectionView: UICollectionView!
    @IBOutlet weak var bidangViewHeight: NSLayoutConstraint!
    @IBOutlet weak var minInputField: UITextField!
    @IBOutlet weak var maxInputField: UITextField!
    
    var cellHeight : CGFloat = 80
    var minAmount = 0
    var maxAmount = 0
    var lembaga = [Institutions]()
    var bidang = [InterestData]()
    
    var selectedInstitutions = [Int]()
    var selectedInterest = [Int]()
    var delegateFilter : FilterInsert?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        loadDataInstitution()
    }
    
    func setupView(){
        self.navigationItem.title = "Filter"
        let filter = UIBarButtonItem(title : "Terapkan", style: .plain, target: self, action:  #selector(filterClass))
        self.navigationItem.rightBarButtonItem  = filter
        
        lembagaCollectionView.register(UINib(nibName: "FilterViewCell", bundle: nil), forCellWithReuseIdentifier: "LembagaCell")
        lembagaCollectionView.delegate = self
        lembagaCollectionView.dataSource = self
        lembagaCollectionView.showsVerticalScrollIndicator = false
        
        bidangCollectionView.register(UINib(nibName: "FilterViewCell", bundle: nil), forCellWithReuseIdentifier: "BidangCell")
        bidangCollectionView.delegate = self
        bidangCollectionView.dataSource = self
        bidangCollectionView.showsVerticalScrollIndicator = false
        
        minInputField.delegate = self
        maxInputField.delegate = self
        minInputField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        maxInputField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        addKeyboardObserver(show: true)
        
    }
    
    func loadDataInstitution(){
        self.showLoading()
        Request().getListInstition { result in
            switch result {
            case .success(let response):
                self.lembaga = response.data
                self.lembagaCollectionView.reloadData()
                self.loadDataInterest()
            case .failure(let error):
                print("getListInstition failed->\(error)")
            }
        }
    }
    
    func loadDataInterest(){
        Request().getListInterest { result in
            self.dismiss(animated: false, completion: nil)
            switch result {
            case .success(let response):
                self.bidang = response.interests
                self.bidangCollectionView.reloadData()
                self.updateHeight()
            case .failure(let error):
                print("getListInterest failed->\(error)")
            }
        }
    }
    
    
    @objc func filterClass(){
       delegateFilter?.data(institutions: selectedInstitutions, interest: selectedInterest, feeMax: maxAmount, feeMin: minAmount)
       self.navigationController?.popViewController(animated: true)

    }
    
    func updateHeight(){
        self.lembagaViewHeight.constant = CGFloat(lembaga.count) * self.cellHeight
        self.bidangViewHeight.constant = CGFloat(bidang.count) * self.cellHeight
    }
}

extension FilterClassViewController : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if textField == self.minInputField {
            let amount = Int.parse(from: textField.text?.getNumberOnly() ?? "0") ?? 0
            minInputField.text = amount.convertToCurrency()
            minAmount = amount
        } else {
            let amount = Int.parse(from: textField.text?.getNumberOnly() ?? "0") ?? 0
            maxInputField.text = amount.convertToCurrency()
            maxAmount = amount
        }
    }
    
}

extension FilterClassViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == lembagaCollectionView {
            return lembaga.count
        } else {
            return bidang.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == lembagaCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LembagaCell", for: indexPath) as! FilterViewCell
            let urlImg = URL(string:lembaga[indexPath.row].logo)!
            cell.clipsToBounds = false
            cell.lblName.text = lembaga[indexPath.row].name
            if let data = try? Data(contentsOf: urlImg) {
                cell.imgLogo.image = UIImage(data: data)
            }
            return cell
            
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BidangCell", for: indexPath) as! FilterViewCell
            cell.clipsToBounds = false
            cell.lblName.text = bidang[indexPath.row].name
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == lembagaCollectionView {
            if let cell = lembagaCollectionView.cellForItem(at: indexPath) as? FilterViewCell{
                cell.selected()
                let institution = lembaga[indexPath.row]
                if !selectedInstitutions.contains(where: { $0 == institution.id}) {
                    self.selectedInstitutions.append(institution.id)
                } else {
                    self.selectedInstitutions.removeAll(where: { $0 == institution.id})
                }
                
                
            }
        } else {
            if let cell = bidangCollectionView.cellForItem(at: indexPath) as? FilterViewCell{
                cell.selected()
                
                let interest = bidang[indexPath.row]
                if !selectedInterest.contains(where: { $0 == interest.id}) {
                    self.selectedInterest.append(interest.id)
                } else {
                    self.selectedInterest.removeAll(where: { $0 == interest.id})
                }
            }
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 60)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
}

