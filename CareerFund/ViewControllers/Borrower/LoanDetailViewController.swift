//
//  LoanDetailViewController.swift
//  CareerFund
//
//  Created by MAC on 24/11/21.
//

import UIKit
import Toaster

class LoanDetailViewController: UIViewController, PaymentMethodBorrower {
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblTenure: UILabel!
    @IBOutlet weak var lblInterest: UILabel!
    @IBOutlet weak var lblNoVA: UILabel!
    @IBOutlet weak var lblHistoryPayment: UILabel!
    @IBOutlet weak var lblInstalment: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblClassName: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblHowToTransfer: UILabel!
    @IBOutlet weak var imgCopy: UIImageView!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var historyTableView: UITableView!
    @IBOutlet weak var tableCellHeight: NSLayoutConstraint!
    
    var data : DataLoanModel?
    var paymentId = 1
    var numberVA = 122000000000001
    var totalPayment = 0
    var cellHeight : CGFloat = 60
    var historyData : [LoanPayments]?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        self.navigationItem.title = "Detail"
        contentView.layer.cornerRadius = 8
        self.navigationItem.backButtonTitle = "Kembali"
      
        if let tenure = data?.loan?.tenorMonth, let interest = data?.loan?.interestPercent {
            lblTenure.text = "\(tenure) Bulan"
            let digit = Double(round(100 * interest) / 100)
            lblInterest.text = "\(digit) %"
        }
        
        lblCompanyName.text =  data?.aclass?.bootcamp?.institutions[0].name
        lblClassName.text =  data?.aclass?.bootcamp?.name
        lblAmount.text = data?.aclass?.price.convertToCurrency()
        lblInstalment.text =  data?.loan?.monthlyPayment.convertToCurrency()
        lblNoVA.text = "\(numberVA)"
        self.historyData = data?.loan?.loanPayment
        progressView.progress = data?.loan?.progress ?? 0

        if data?.dpPaid != true {
            totalPayment = data?.loan?.downPayment ?? 0
        } else {
            totalPayment = data?.loan?.monthlyPayment ?? 0
        }
        
        if historyData?.count == 0 {
            lblHistoryPayment.isHidden = true
        } else {
            lblHistoryPayment.isHidden = false
        }
        
        if let url = data?.aclass?.bootcamp?.institutions[0].logo {
            let urlImg = URL(string:url)!
            if let data = try? Data(contentsOf: urlImg) {
                imgLogo.image = UIImage(data: data)
            }
        }
        let step = "Pilih menu “Bayar/Beli” \n 1.Selanjutnya akan muncul tipe pembayaran seperti Multi Payment dan Bill Payment (PLN, PDAM, dll), untuk pembayaran VA pilih “Multi Payment” \n 2. Selanjutnya akan muncul berbagai tipe pembayaran, untuk pembayaran VA pilih “Virtual Account” \n 3.Masukkan nomor Virtual Account Anda (contoh: 8890802001287578) dan klik “Benar” \n 4. Akan muncul nominal yang harus dibayarkan. Pastikan semua informasi dan total tagihan yang ditampilkan sudah benar. Jika benar, tekan angka 1 dan pilih “Ya” \n 5.Bukti pembayaran dalam bentuk struk agar disimpan sebagai bukti pembayaran yang sah \n 7. Transaksi Anda telah selesai"
        
        lblHowToTransfer.text = step
        imgCopy.isUserInteractionEnabled = true
        imgCopy.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(copyNoVirtual)))
        lblPaymentMethod.isUserInteractionEnabled = true
        lblPaymentMethod.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openPaymentMethod)))
        
        setupTableView()
        updateHeight()
    }
    
    func setupTableView(){
        self.historyTableView.delegate = self
        self.historyTableView.dataSource = self
        self.historyTableView.register(UINib(nibName: "HistoryPaymentCell", bundle: nil), forCellReuseIdentifier: "HistoryPaymentCell")
    }
    
    func updateHeight() {
        self.tableCellHeight.constant = CGFloat(self.historyData?.count ?? 0) * self.cellHeight
    }
    
    @objc func copyNoVirtual(){
        UIPasteboard.general.string = "\(numberVA)"
        Toast(text: "Berhasil disalin", duration: Delay.short).show()
        Request().payLoanClass(classId: data?.id ?? 0, paymentId: paymentId, amount: totalPayment){ result in
            DispatchQueue.main.async {
                self.dismiss(animated: false, completion: nil)
                switch result {
                case .success:
                    print("success bayar")
                case .failure:
                    print("gagal bayar")
                }
            }
        }
    }
    
    func data(payment: PaymentAccount) {
        self.paymentId = payment.id
        self.numberVA = payment.number
        lblNoVA.text = "\(numberVA)"
    }
    
    @objc func openPaymentMethod(){
        let vc = PaymentMethodLenderViewController()
        vc.delegatePayBorrower = self
        navigationController?.pushViewController(vc, animated: false)
    }
}

extension LoanDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyData?.count ?? 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryPaymentCell", for: indexPath) as! HistoryPaymentCell
        cell.selectionStyle = .none
        cell.loadData(data: historyData![indexPath.row])
        return cell
    }
}

