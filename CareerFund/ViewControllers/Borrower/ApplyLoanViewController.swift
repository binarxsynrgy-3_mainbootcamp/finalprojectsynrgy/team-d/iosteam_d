//
//  ApplyLoanViewController.swift
//  CareerFund
//
//  Created by MAC on 26/11/21.
//

import UIKit

class ApplyLoanViewController: BaseViewController {
    
    @IBOutlet weak var classVIew: UIView!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblClassName: UILabel!
    @IBOutlet weak var lblFee: UILabel!
    @IBOutlet weak var lblPeriode: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblPrecentageFee: UILabel!
    @IBOutlet weak var lblNominalFee: UILabel!
    @IBOutlet weak var lblLoanAmountAlert: UILabel!
    @IBOutlet weak var lbelTermAlert: UILabel!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var durationField: UITextField!
    @IBOutlet weak var installmentView: UIView!
    @IBOutlet weak var btnFinish: ButtonBlue!
    
    var dataClass : Class?
    var loanAmount = 0
    var loanTerm = 0
    var nominalFee = 0
    var interest = 0.0
    var minimumLoan = 0
    var maximalLoan = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        self.navigationItem.title = "Daftar"
        
        btnFinish.isEnabled = false
        durationField.delegate = self
        amountField.delegate = self
        durationField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        amountField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        btnFinish.addTarget(self, action: #selector(submit), for: .touchUpInside)
        addKeyboardObserver(show: true)
        
        classVIew.layer.cornerRadius = 8
        installmentView.layer.cornerRadius = 8
        
        lblCompanyName.text = dataClass?.bootcamp?.institutions[0].name
        lblClassName.text = dataClass?.name
        lblFee.text = dataClass?.price.convertToCurrency()
        lblDuration.text = "\(dataClass?.duration ?? 0) Bulan"
        
        if let url = dataClass?.bootcamp?.institutions[0].logo {
            let urlImg = URL(string:(url))!
            if let data = try? Data(contentsOf: urlImg) {
                self.imgLogo.image  = UIImage(data: data)
            }
        }

        if let startDate = dataClass?.startDate, let endDate = dataClass?.endDate {
            lblPeriode.text = "\(startDate.date(dateFormat: "yyyy-MM-dd").string(dateFormat: "MMM")) - \(endDate.date(dateFormat: "yyyy-MM-dd").string(dateFormat: "MMM yyyy"))"
        }
    }
    
    
    func checkForm(){
        btnFinish.isEnabled = false
        guard self.checkValidAmount() else {return}
        guard self.checkValidTerm() else {return}

        guard !self.amountField.text!.isEmpty else {return}
        guard !self.durationField.text!.isEmpty else {return}
        btnFinish.isEnabled = true
    }
    
    func checkValidAmount() -> Bool {
        minimumLoan = dataClass!.price * 10/100
        maximalLoan = dataClass!.price * 30/100

        guard self.loanAmount > 0 else {return false}
         if self.loanAmount < self.minimumLoan {
            self.lblLoanAmountAlert.isHidden = false
            self.lblLoanAmountAlert.text = "Minimal \(minimumLoan.convertToCurrency()) "
         } else if self.loanAmount > maximalLoan {
            self.lblLoanAmountAlert.isHidden = false
            self.lblLoanAmountAlert.text = "Maksimal \(maximalLoan.convertToCurrency()) "
        } else {
            self.lblLoanAmountAlert.isHidden = true
        }
        return self.loanAmount >= self.minimumLoan && self.loanAmount <= maximalLoan
    }
    
    func checkValidTerm() -> Bool {
        guard self.loanTerm > 0 else {return false}
         if self.loanTerm <= 2 {
            self.lbelTermAlert.isHidden = false
            self.lbelTermAlert.text = "Minimal 3 Bulan "
         } else if self.loanTerm > 12 {
            self.lbelTermAlert.isHidden = false
            self.lbelTermAlert.text = "Maksimal 12 Bulan  "
        } else {
            self.lbelTermAlert.isHidden = true
        }
        return self.loanTerm >= 3 && self.loanTerm <= 12
    }
    
    func calculateFee(){
        interest = Double(loanTerm) / 12.0 * 5.0
        nominalFee = loanAmount  * Int(interest) / 100
        let digit = Double(round(100 * interest) / 100)
        lblPrecentageFee.text = "\(digit) % "
        lblNominalFee.text =  nominalFee.convertToCurrency()
    }
    
    @objc func submit(){
        let vc = ConfirmLoanViewController()
        vc.dataClass = dataClass
        vc.loanAmount = loanAmount
        vc.tenure = loanTerm
        vc.fee = nominalFee
        vc.interest = interest
        navigationController?.pushViewController(vc, animated: false)
    }
}

extension ApplyLoanViewController : UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == self.amountField {
            let amount = Int.parse(from: textField.text?.getNumberOnly() ?? "0") ?? 0
            amountField.text = amount.convertToCurrency()
            loanAmount = amount
        } else {
            let term = Int.parse(from: textField.text?.getNumberOnly() ?? "0") ?? 0
            durationField.text = "\(term)"
            loanTerm = term
            lblMonth.isHidden = false
        }
        
        calculateFee()
        checkForm()
    }
    
}
