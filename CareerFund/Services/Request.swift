//
//  Request.swift
//  CareerFund
//
//  Created by MAC on 27/10/21.
//

import UIKit

class Request  {
    
    func getDataUsers(completion : @escaping(Result<[UserResponse],Error>) -> Void) {
        let url = UrlConstant.get_list_user
        Connection.shared.connect(url: url, params: nil,httpMethod: "GET", model: [UserResponse].self,completion: { result in
            completion(result)
        })
    }
    
    func getDataProfile(completion : @escaping(Result<ProfileResponse,Error>) -> Void) {
        let url = UrlConstant.get_profile
        Connection.shared.connect(url: url, params: nil,httpMethod: "GET", model: ProfileResponse.self,completion: { result in
            completion(result)
        })
    }
    
    func register (email : String, name : String, password: String, role : String, completion : @escaping(Result<String,Error>) -> Void) {
        let url = UrlConstant.register
        var params = [String:Any]()
        params["email"] = email
        params["name"] = name
        params["password"] = password
        params["role"] = role
        Connection.shared.connectPost(url: url, params: params, httpMethod: "POST",completion: { result in
            completion(result)
        })
    }
    
    func requestOtp (email : String, completion : @escaping(Result<String,Error>) -> Void) {
        let url = UrlConstant.requestOtp
        var params = [String:Any]()
        params["email"] = email
        Connection.shared.connectPost(url: url, params: params, httpMethod: "POST",completion: { result in
            completion(result)
        })
    }
    
    func verifyOtp (otp : String, completion : @escaping(Result<SigninResponse,Error>) -> Void) {
        let url = UrlConstant.verifyOtp
        var params = [String:Any]()
        params["code"] = otp
        Connection.shared.connect(url: url, params: params, httpMethod: "POST",model : SigninResponse.self,completion: { result in
            completion(result)
        })
    }
    
    func login (email : String, password: String, completion : @escaping(Result<SigninResponse,Error>) -> Void) {
        let url = UrlConstant.login
        var params = [String:Any]()
        params["email"] = email
        params["password"] = password
        Connection.shared.connect(url: url, params: params, httpMethod: "POST",model : SigninResponse.self,completion: { result in
            completion(result)
        })
    }
    
    
    func forgetPassword (email : String, completion : @escaping(Result<String,Error>) -> Void) {
        let url = UrlConstant.forget_password
        var params = [String:Any]()
        params["email"] = email
        Connection.shared.connectPost(url: url, params: params, httpMethod: "POST",completion: { result in
            completion(result)
        })
    }
    
    func getListInterest (completion : @escaping(Result<MinatResponse,Error>) -> Void) {
        let url = UrlConstant.interest
        Connection.shared.connect(url: url, params: nil, httpMethod: "POST",model : MinatResponse.self,completion: { result in
            completion(result)
        })
    }
    
    func getListInstition (completion : @escaping(Result<InstitutionResponse,Error>) -> Void) {
        let url = UrlConstant.get_institutions
        Connection.shared.connect(url: url, params: nil, httpMethod: "GET",model : InstitutionResponse.self,completion: { result in
            completion(result)
        })
    }
    
    func getUserInterest (completion : @escaping(Result<InterestMyResponse,Error>) -> Void) {
        let url = UrlConstant.interest_user
        Connection.shared.connect(url: url, params: nil, httpMethod: "GET",model : InterestMyResponse.self,completion: { result in
            completion(result)
        })
    }
    
    func saveUserInterest( id : [Int], completion : @escaping(Result<String,Error>) -> Void) {
        let url = UrlConstant.interest_user
        var params = [String:Any]()
        params["interest_ids"] = id
        Connection.shared.connectPost(url: url, params: params, httpMethod: "POST", completion: { result in
            completion(result)
        })
    }
    
    func getLenderLoans (completion : @escaping(Result<LenderLoansResponse,Error>) -> Void) {
        let url = UrlConstant.lenderLoans + "?withdrawable=1"
        Connection.shared.connect(url: url, params: nil, httpMethod: "GET",model : LenderLoansResponse.self,completion: { result in
            completion(result)
        })
    }
    
    func submitFunding(amount : Int, loanId : Int, completion : @escaping(Result<String,Error>) -> Void) {
        let url =  UrlConstant.availableBorrower + "/\(loanId)/fund"
        var params = [String:Any]()
        params["fund"] = amount
        Connection.shared.connectPost(url: url, params: params, httpMethod: "POST", completion: { result in
            completion(result)
        })
    }
    
    func submitWithdraw(fundingId : Int, bankAccountNumber : Int, bankId : Int, completion : @escaping(Result<String,Error>) -> Void) {
        let url = UrlConstant.lenderLoans + "/\(fundingId)/withdraw"
        var params = [String:Any]()
        params["account_number"] = bankAccountNumber
        params["bank_id"] = bankId
        Connection.shared.connectPost(url: url, params: params, httpMethod: "POST", completion: { result in
            completion(result)
        })
    }
    
    func sumAsset (completion : @escaping(Result<DashboardLenderLoansResponse,Error>) -> Void) {
        let url = UrlConstant.lenderLoans
        Connection.shared.connect(url: url, params: nil, httpMethod: "GET",model : DashboardLenderLoansResponse.self,completion: { result in
            completion(result)
        })
    }
    
    func getAvailableBorrower (completion : @escaping(Result<AvailableBorrowerResponse,Error>) -> Void) {
        let url = UrlConstant.availableBorrower
        Connection.shared.connect(url: url, params: nil, httpMethod: "GET",model : AvailableBorrowerResponse.self,completion: { result in
            completion(result)
        })
    }
    
    func getListClass (keyword : String?, categories : [Int], feeMax : String?,feeMin : String?, institutions : [Int],sort: String?,order : String?, completion : @escaping(Result<ClassResponse,Error>) -> Void) {
        var urlComponents = URLComponents()
        urlComponents.scheme = UrlConstant.scheme
        urlComponents.host   = UrlConstant.host
        urlComponents.path   = UrlConstant.get_class
        
        var cat = "\(categories)"
        var ins = "\(institutions)"
        
        cat = cat.replacingOccurrences(of: "[", with: "", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: "]", with: "", options: NSString.CompareOptions.literal, range: nil)
        ins = ins.replacingOccurrences(of: "[", with: "", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: "]", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        var queryItem = [URLQueryItem]()
        
        if keyword != "" {
            queryItem.append(URLQueryItem(name: "search", value: keyword))
        }
        if categories.count > 0 {
            queryItem.append(URLQueryItem(name: "category", value: cat))
        }
        if institutions.count > 0 {
            queryItem.append(URLQueryItem(name: "institution", value: ins))
        }
        if feeMax != "0" {
            queryItem.append(URLQueryItem(name: "pmax", value: feeMax))
        }
        if feeMin != "0" {
            queryItem.append(URLQueryItem(name: "pmin", value: feeMin))
        }
        if sort != "" {
            queryItem.append(URLQueryItem(name: "sort", value: sort))
        }
        
        if order != "" {
            queryItem.append(URLQueryItem(name: "order", value: order))
        }
        
        urlComponents.queryItems = queryItem
        let url = urlComponents.url!.absoluteString
        Connection.shared.connect(url: url, params: nil, httpMethod: "GET",model : ClassResponse.self,completion: { result in
            completion(result)
        })
    }
    
    func getLoanList (completion : @escaping(Result<ListLoanResponse,Error>) -> Void) {
        let url = UrlConstant.get_borrower_class
        Connection.shared.connect(url: url, params: nil, httpMethod: "GET",model : ListLoanResponse.self,completion: { result in
            completion(result)
        })
    }
    
    func submitLoanClass(id : Int, dp : Int, tenor : Int, completion : @escaping(Result<SubmitClassResponse,Error>) -> Void) {
        let url = UrlConstant.submit_loan_class
        var params = [String:Any]()
        params["class_id"] = id
        params["down_payment"] = dp
        params["tenor_month"] = tenor
        Connection.shared.connect(url: url, params: params, httpMethod: "POST",model : SubmitClassResponse.self,completion: { result in
            completion(result)
        })
    }
    
    func payLoanClass(classId : Int,paymentId : Int, amount : Int, completion : @escaping(Result<String,Error>) -> Void) {
        let url = UrlConstant.submit_loan_class + "/\(classId)/pay"
        var params = [String:Any]()
        params["payment_account_id"] = paymentId
        params["payment_amount"] = amount
        Connection.shared.connectPost(url: url, params: params, httpMethod: "POST", completion: { result in
            completion(result)
        })
 }

    func editProfile(address : String, email : String, name : String, phone : String, completion : @escaping(Result<String,Error>) -> Void) {
        let url = UrlConstant.edit_profile
        var params = [String:Any]()
        params["address"] = address
        params["email"] = email
        params["name"] = name
        params["phone_number"] = phone
        Connection.shared.connectPost(url: url, params: params, httpMethod: "PUT",completion: { result in
            completion(result)
        })
    }
    
    func updatePassword(oldPwd : String, newPwd : String, completion : @escaping(Result<String,Error>) -> Void) {
        let url = UrlConstant.update_password
        var params = [String:Any]()
        params["new_password"] = newPwd
        params["old_password"] = oldPwd
        Connection.shared.connectPost(url: url, params: params, httpMethod: "PUT",completion: { result in
            completion(result)
        })
    }
    
    
    func updateProfileImg(foto : UIImage, completion : @escaping(Result<UploadImageResponse,Error>) -> Void) {
        let url = UrlConstant.update_profile_photo
        let imageData = foto.jpegData(compressionQuality: 1)
        Connection.shared.uploadFile(url: url, fileData: imageData!, fileName: "image.jpg",model: UploadImageResponse.self ,completion: { result in
            completion(result)
        })
        
    }
    
    func updateKtp(foto : UIImage, completion : @escaping(Result<UploadImageResponse,Error>) -> Void) {
        let url = UrlConstant.update_ktp
        let imageData = foto.jpegData(compressionQuality: 1)        
        Connection.shared.uploadFile(url: url, fileData: imageData!, fileName: "image.jpg",model: UploadImageResponse.self ,completion:{ result in
            completion(result)
        })
        
    }
    
    func updateKtpSelfie(foto : UIImage, completion : @escaping(Result<UploadImageResponse,Error>) -> Void) {
        let url = UrlConstant.update_ktp_selfie
        let imageData = foto.jpegData(compressionQuality: 1)
        Connection.shared.uploadFile(url: url, fileData: imageData!, fileName: "image.jpg",model: UploadImageResponse.self ,completion:{ result in
            completion(result)
        })
    }
    
    func getPaymentType (completion : @escaping(Result<PaymentTypeResponse,Error>) -> Void) {
        let url = UrlConstant.payment_type
        Connection.shared.connect(url: url, params: nil, httpMethod: "GET",model : PaymentTypeResponse.self,completion: { result in
            completion(result)
        })
    }
    
    func getVerifyProfile (completion : @escaping(Result<String,Error>) -> Void) {
        let url = UrlConstant.verify_profile
        Connection.shared.connectPost(url: url, params: nil, httpMethod: "PUT",completion: { result in
            completion(result)
        })
    }
}
