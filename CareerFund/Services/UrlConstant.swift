//
//  UrlConstant.swift
//  CareerFund
//
//  Created by MAC on 27/10/21.
//

import Foundation
import UIKit

struct UrlConstant {
    
    
    static let staging    = "api-careerfund-staging.herokuapp.com"
    static let production = "api-careerfund.herokuapp.com"
    
    static let scheme     = "https"
    static let host       = "api-careerfund-staging.herokuapp.com"
    
    static let server = "https://\(production)/v1"
    static let get_list_user = server + "/users"
    static let register = server + "/register"
    static let requestOtp = server + "/signup/otp/request-email"
    static let verifyOtp = server + "/signup/otp/verify"
    static let login = server + "/signin"
    static let forget_password = server + "/password/forgot"
    static let interest = server + "/interests"
    static let interest_user = server + "/my/interests"
    static let lenderLoans = server + "/lender/my/loans"
    static let availableBorrower = server + "/lender/loans"
    static let submit_funding = server + "/lender/loans/fund"
    static let get_class = "/v1/classes"
    static let get_institutions = server + "/institutions"
    static let get_borrower_class = server + "/my/classes"
    static let submit_loan_class = server + "/my/classes"
    static let payment_type = server + "/payment-types"
    
    //Profile
    static let get_profile = server + "/profile"
    static let edit_profile = server + "/profile/edit"
    static let update_ktp = server + "/profile/identity-card"
    static let update_ktp_selfie = server + "/profile/selfie"
    static let get_profile_data = server + "/profile/edit"
    static let update_profile_photo = server + "/profile/photo"
    static let verify_profile = server + "/profile/verify"
    static let update_password = server + "/password"

}
