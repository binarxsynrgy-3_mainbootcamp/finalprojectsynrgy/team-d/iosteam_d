//
//  String+Ext.swift
//  CareerFund
//
//  Created by MAC on 12/11/21.
//

import Foundation

extension String {
    func isValidEmail() -> Bool {
        let regex = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" +
            "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" +
            "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" +
            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
            "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
            "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: self)
    }
    
    func getNumberOnly() -> String {
        let str = self
        if !str.isEmpty {
            return str.replacingOccurrences( of:"[^0-9]", with: "", options: .regularExpression)
        } else {
            return ""
        }
    }
    
    
    func date(dateFormat: String = "yyyy-MM-dd'T'HH:mm:ss") -> Date {
        let knowingFormats = [
            "yyyy-MM-dd'T'HH:mm:ss",
            "yyyy-MM-dd' 'HH:mm:ss",
            "yyyy-MM-dd'T'HH:mm:ss.SSS",
            "yyyy-MM-dd'T'HH:mm:ssZ",
            "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
            "yyyy-MM-dd'T'HH:mm:ssZZZZZ",
            "yyyy-MM-dd'T'HH:mm:ssXXX",
            "yyyy-MM-dd'T'HH:mm:ss'Z'",
            "yyyy-MM-dd",
            "yyyy-MM-dd'T'HH:mm.ss'Z'",
            "dd MM yyyy",
            "dd/MM/yyyy"
        ]
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.dateFormat = dateFormat
        if let date = dateFormatter.date(from: self) {
            return date
        } else {
            for format in knowingFormats {
                dateFormatter.dateFormat = format
                if let _date = dateFormatter.date(from: self) {
                    return _date
                }
            }
        }
        return Date()
    }
    
    func getMonth() -> String {
        let str = self
        var formatDate = ""
        if !str.isEmpty {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            formatter.dateFormat = "MMM"
            
            if let dateFormate = formatter.date(from: str){
                formatDate = formatter.string(from: dateFormate)
            }
            return formatDate
        } else {
            return ""
        }
    }
    
    func getMonthYear() -> String {
        let str = self
        var formatDate = ""
        
        if !str.isEmpty {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            formatter.dateFormat = "MMM yyyy"
            if let dateFormate = formatter.date(from: str){
                formatDate = formatter.string(from: dateFormate )
            }
            return formatDate
        } else {
            return ""
        }
    }
}

extension Double {
    func convertToCurrency() -> String {
        let formatter                   = NumberFormatter()
        formatter.locale                = Locale(identifier: "en_US")
        formatter.numberStyle           = .currency
        formatter.currencySymbol        = ""
        formatter.maximumFractionDigits = 0
        let newSelf = floor(self)
        return "Rp" + " \((formatter.string(from: newSelf as NSNumber) ?? "0").replacingOccurrences(of: ".", with: ","))"
    }
}

extension Int {
    func convertToCurrency() -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        formatter.maximumFractionDigits = 0
        
        let text = "Rp" + " \((formatter.string(from: self as NSNumber) ?? "").replacingOccurrences(of: ".", with: ","))"
        if text.contains("-") {
            return "-\(text.replacingOccurrences(of: "-", with: ""))"
        } else {
            return text
        }
    }
    static func parse(from string: String) -> Int? {
        let decimals = string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        return Int(decimals)
    }
    
}

extension Date {
    func string(dateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: self)
    }
    
    func daysBetween(date: Date) -> Int {
         return Date.monthBetween(start: self, end: date)
     }
     
     static func monthBetween(start: Date, end: Date) -> Int {
         let calendar = Calendar.current
         
         // Replace the hour (time) of both dates with 00:00
         let date1 = calendar.startOfDay(for: start)
         let date2 = calendar.startOfDay(for: end)
         
         let a = calendar.dateComponents([.month], from: date1, to: date2)
         return a.value(for: .month)!
     }
    
    static var yesterday: Date { return Date().dayBefore }
        static var tomorrow:  Date { return Date().dayAfter }
        var dayBefore: Date {
            return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
        }
        var dayAfter: Date {
            return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
        }
        var noon: Date {
            return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
        }
        var month: Int {
            return Calendar.current.component(.month,  from: self)
        }
        var isLastDayOfMonth: Bool {
            return dayAfter.month != month
        }
}
