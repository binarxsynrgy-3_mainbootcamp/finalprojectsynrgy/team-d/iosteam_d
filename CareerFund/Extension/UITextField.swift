//
//  UITextField.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 10/11/21.
//

import Foundation
import UIKit

extension UITextField {
    // function to show eye image for show/hidden password text
    
    fileprivate func setPasswordToggleImage(_ button: UIButton) {
        if(isSecureTextEntry){
            button.setImage(UIImage(systemName: "eye"), for: .normal)
            button.tintColor  =  UIColor.systemGray
        }else{
            button.setImage(UIImage(systemName: "eye.slash"), for: .normal)
            button.tintColor  =  UIColor.systemGray
            
        }
    }
    
    func enablePasswordToggle(){
        let button = UIButton(type: .custom)
        setPasswordToggleImage(button)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(self.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        button.addTarget(self, action: #selector(togglePasswordView), for: .touchUpInside)
        self.rightView = button
        self.rightViewMode = .always
    }
    
    @objc func togglePasswordView(_ sender: Any) {
        self.isSecureTextEntry = !self.isSecureTextEntry
        setPasswordToggleImage(sender as! UIButton)
    }
}


