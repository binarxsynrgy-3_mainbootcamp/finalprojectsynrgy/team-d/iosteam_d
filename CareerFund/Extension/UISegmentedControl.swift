//
//  UISegmentedControl.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 10/11/21.
//

import Foundation
import UIKit

extension UISegmentedControl {
    //function to change color of selected segment
    
    func selectedConfiguration(font: UIFont = UIFont.boldSystemFont(ofSize: 15), color: UIColor)
    {
        let selectedAttributes = [
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.foregroundColor: color
        ]
        setTitleTextAttributes(selectedAttributes, for: .selected)
    }
}
