//
//  UIButton.swift
//  CareerFund
//
//  Created by Prawira Hadi Fitrajaya on 10/11/21.
//

import Foundation
import UIKit

// Custom Button View

extension UIButton{
    
    func firstButton(withText text: String) -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(text, for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.contentEdgeInsets = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        button.backgroundColor = UIColor().hexStringToUIColor(hex: "3163AF")
        button.layer.cornerRadius = 8
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        
        return button
    }
    
    func secondButton(withText text: String) -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(text, for: .normal)
        button.setTitleColor(UIColor().hexStringToUIColor(hex: "3163AF"), for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.contentEdgeInsets = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        button.backgroundColor = .systemBackground
        button.layer.borderColor = UIColor().hexStringToUIColor(hex: "3163AF").cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 8
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)

        return button
    }
}
